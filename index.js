require('rootpath')();
var http = require('http');
var url = require('url');
var express = require('express');
var expressJwt = require('express-jwt');
var bodyParser = require('body-parser');
var config = require('./config');
var logger = require('./lib/logger');
var connection = require('./models/connection');
var User = require('./models/user/user');
var cors = require('cors');

// Enterprise Connector Routes
const ecConnectorTypeRoute = require('./routes/ec/connectortype.route.js');
const ecConnectorRoute = require('./routes/ec/connector.route.js');
const ecDataSetRoute = require('./routes/ec/dataset.route.js');
const ecDataRoute = require('./routes/ec/data.route.js');

const { DomainModelGraphRoute, IntentFulfillmentGraphRoute,
     IntentFulfillmentRoute, NLPIntentFulfillmentMappingRoute } = require('./routes/intentfulfillment/');

const intentRoute = require('./routes/intent/intent.route.js');
const nlpRoute = require('./routes/nlp/nlp.route.js');
const modelRoute = require('./routes/model/model.route.js');
const projectRoute = require('./routes/project/project.route.js');
const commonRoute = require('./routes/common/common.route.js');
const e2eRoute = require('./routes/e2e/e2e.route.js');
const conversationRoute = require('./routes/client/conversation.route');

var app = express();
app.set('superSecret', config.token.secret); // secret variable
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(cors());

// use JWT auth to secure the api, the token can be passed in the authorization header or querystring
app.use(expressJwt({
    secret: config.token.secret
}).unless({ path: ['/api/users/authenticate', '/api/process/', '/api/users/register', '/api/users/resetpassword', '/',
    /\/api\/client\/conversation*/,
    /\/api\/ec\/data*/,
    /\/api\/intentfulfillment*/, 
//    /\/api\/domainmodel*/, 
//    /\/api\/nlpintentfulfillmentmapping*/,
//    /\/api\/domainmodelgraph*/,
// /\/api\/intentfulfillmentgraph*/
] }));

app.get('/', function (req, res) {
    res.json({ message: 'Welcome to AI/ML Realm!!' });
});

// routes
app.use('/api/users', require('./controllers/user/users.controller'));
app.use('/api/models', modelRoute);
app.use('/api/ec/connectortype', ecConnectorTypeRoute);
app.use('/api/ec/connector', ecConnectorRoute);
app.use('/api/ec/dataset', ecDataSetRoute);
app.use('/api/ec/data', ecDataRoute);
app.use('/api/domainmodelgraph', DomainModelGraphRoute);
app.use('/api/intentfulfillmentgraph', IntentFulfillmentGraphRoute);
app.use('/api/intentfulfillment', IntentFulfillmentRoute);
app.use('/api/nlpintentfulfillmentmapping',NLPIntentFulfillmentMappingRoute);
app.use('/api/intent', intentRoute);
app.use('/api/nlp', nlpRoute);
app.use('/api/project', projectRoute);
app.use('/api/common', commonRoute);
app.use('/api/process', e2eRoute);
app.use('/api/client/conversation', conversationRoute);

// error handler
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401).send('Invalid Token');
    }
    else {
        throw err;
    }
});

// start server
//var port = process.env.NODE_ENV === 'production' ? 80 : 4000;
var port = config.port;
var server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
