"use strict";

const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const del = require('del');
const runSequence = require('run-sequence');
const replace = require('gulp-replace');
const jsonConcat = require('gulp-json-concat');
//const path = require('path');

const plugins = gulpLoadPlugins();

const coveragePaths = [
    './**/*.js',
  '!node_modules/**',
  '!coverage/**',
  '!./**/*spec.js',
  '!gulpfile.js'];

const options = {
  codeCoverage: {
    reporters: ['lcov', 'text-summary'],
    thresholds: {
      global: { statements: 80, branches: 80, functions: 80, lines: 80 }
    }
  }
};

// Clean up coverage directory
gulp.task('clean', () =>
  del(['coverage/**', '!coverage'])
);

// Set env variables
/*gulp.task('set-test-env', () => {
  plugins.env({
    file: './config/env/.env.test.json'
  });
});
*/
/*gulp.task('set-dev-env', () => {
  plugins.env({
    file: './config/env/.env.dev.json'
  });
});
*/
// Start server with restart on file changes
gulp.task('nodemon', () =>
  plugins.nodemon({
    script: 'index.js',
    ext: 'js',
    ignore: ['node_modules/**/*.js']
  })
);

// covers files for code coverage
gulp.task('pre-test', () =>
  gulp.src(['./**/*.js', '!node_modules/**', '!coverage/**', '!./**/*spec.js', '!gulpfile.js', '!config/**', '!logs/**'])
  // Covering files
    .pipe(plugins.istanbul({
      includeUntested: true
    }))
    // Force `require` to return covered files
    .pipe(plugins.istanbul.hookRequire())
);

// triggers jasmine test with code coverage
gulp.task('test', ['pre-test'], () => {
  let reporters;
  let exitCode = 0;

  if (plugins.util.env['code-coverage-reporter']) {
    reporters = [options.codeCoverage.reporters, plugins.util.env['code-coverage-reporter']];
  } else {
    reporters = options.codeCoverage.reporters;
  }

  return gulp.src('./spec/*Spec.js')
    //.pipe(plugins.plumber())
    .pipe(plugins.jasmine())
    .once('error', (err) => {
      plugins.util.log(err);
      exitCode = 1;
    })
    // Creating the reports after execution of test cases
    .pipe(plugins.istanbul.writeReports({
      dir: './coverage',
      reporters
    }))
    // Enforce test coverage
    //.pipe(plugins.istanbul.enforceThresholds({
    //  thresholds: options.codeCoverage.thresholds
    //}))
    .once('end', () => {
      plugins.util.log('completed !!');
      process.exit(exitCode);
    });
});

// execute tests
gulp.task('jasmine', ['clean'], () => {
  runSequence(
    'eventDataCopy',
    'test'
  );
});

// gulp serve for development
gulp.task('serve', ['clean'], () => runSequence('nodemon'));

gulp.task('lcov', () => {
  gulp.src(['coverage/lcov.info'])
    .pipe(
      replace(`${__dirname}/`, '')
      )
    .pipe(gulp.dest('coverage'));
});

// event csv file copy
gulp.task('eventDataCopy', () => {
  gulp.src('spec/User_Events.csv')
  .pipe(gulp.dest('datasource/uploaded'));
});
