const { DomainModels } = require('./domainmodels/');
const { AggregateFunctions } = require('./functions/');
const { ConnectorTypes } = require('./connectortypes/');
const { ClientApplications } = require('./clientapplications/');

const { DomainModelController } = require('../controllers/intentfulfillment');
const { DomainModelGraphService, FunctionsService } = require('../services/intentfulfillment')
const connectorTypeService = require('../services/ec/connectortype.service');
const { ClientApplicationService } = require('../services/client');


const Users = require('./master/users.json');
const Categories = require('./master/categories.json');
const ProjOptions = require('./master/projectoptions.json');
const Cultures = require('./master/cultures.json');

const UserService = require('../services/user/user.service');
const ProjectService = require('../services/project/project.service');
const CommonService = require('../services/common/common.service');

const utils = require('../lib/utility');

console.log('**** Initial Data Load: Start ***********');

var connectorTypeToAdd = (ConnectorTypes != null && ConnectorTypes != 'undefined') ? ConnectorTypes : [];
console.log('***** Connector Types Loading... total Connector Types =>', ConnectorTypes.length);
for (var i = 0; i < ConnectorTypes.length; i++) {
    loadConnectorType(ConnectorTypes[i]);
}

var functionsToAdd = (AggregateFunctions != null && AggregateFunctions != 'undefined') ? AggregateFunctions : [];
console.log('***** Functions Loading... total functions =>', functionsToAdd.length);
for (var i = 0; i < functionsToAdd.length; i++) {
    loadFunction(functionsToAdd[i]);
}

var domainmodelsToAdd = (DomainModels != null && DomainModels != 'undefined') ? DomainModels : [];
console.log('***** Domain Models Loading... total models =>', domainmodelsToAdd.length);
for (var i = 0; i < domainmodelsToAdd.length; i++) {
    loadDomainModels(domainmodelsToAdd[i]);
}

var clientApplicationsToAdd = (ClientApplications != null && ClientApplications != 'undefined') ? ClientApplications : [];
console.log('***** Client Applications Loading... total client applications =>', clientApplicationsToAdd.length);
for (var i = 0; i < clientApplicationsToAdd.length; i++) {
    loadClientApplications(clientApplicationsToAdd[i]);
}

console.log('***** Users Loading... total Users =>', Users.length);
if (Users && Users.length > 0) {
    Users.forEach(x => {
        const payload = {
            username: x.username,
            password: utils.encryptData(x.password),
            firstname: x.firstname,
            lastname: x.lastname,
            email: x.email,
            admin: x.admin
        };

        UserService.create(payload)
            .then(success => console.info('User "' + x.username + '" created succefully'))
            .catch(error => console.error('INFO ==>', error));
    });
}

UserService.getByUserName('admin').then(user => {
    console.log('***** Categories Loading... total Categories =>', Categories.length);

    if (Categories && Categories.length > 0) {
        Categories.forEach(x => {
            ProjectService.createCategory({ name: x.name, updatedBy: user._id })
                .then(success => console.info('Category "' + x.name + '" added successfully'))
                .catch(error => console.error('INFO ==>', error));
        });
    }
});

UserService.getByUserName('admin').then(user => {
    console.log('***** Cultures Loading... total Cultures =>', Cultures.length);

    if (Cultures && Cultures.length > 0) {
        Cultures.forEach(x => {
            CommonService.createCulture({ name: x.name, code: x.code, updatedBy: user._id })
                .then(success => console.info('Culture "' + x.code + '" added successfully'))
                // .catch(error => console.log(error));
                .catch(error => console.error('INFO ==>', error));
        });
    }
});

ProjectService.getProjectOptions().then(options => {
    console.log('***** Project Options Loading... total Project Options =>', ProjOptions.length);

    if (ProjOptions && ProjOptions.length > 0) {
        ProjOptions.forEach(x => {
            const option = options.filter(opt => { return x.name == opt.name });

            if (option == null) {
                ProjectService.createProjectOption(x)
                    .then(success => console.info('Project Option "' + x.name + '" added successfully'))
                    .catch(error => console.error('INFO ==>', error));
            }
            else {
                // console.log('option', option)
                ProjectService.updateProjectOption(x)
                    .then(success => console.info('Project Option "' + x.name + '" updated successfully'))
                    .catch(error => console.error('INFO ==>', error));
            }
        });
    }
});

/****** functions *********************/
function loadConnectorType(connectortype) {
    connectorTypeService.Create(connectortype, function (err, data) {
        if (err) {
            console.log('Error occured while adding connector type =>', connectortype.name, err.message);
        }
        else {
            console.log('connector type added successfully =>', data.name);
        }
    });
}

function loadFunction(newfunction) {
    FunctionsService.GetByCode(newfunction.code).then(
        existingfunction => {
            if (existingfunction != null && existingfunction != 'undefined') {
                newfunction["_id"] = existingfunction._id;
                newfunction["lastmodifiedby"] = 'initial-data';
                FunctionsService.Update(newfunction).then(
                    success => { console.log('function updated successfully =>', existingfunction.name); }
                    , err => { console.log('Error occured while updating function =>', existingfunction.name); }
                )
            }
            else {

                newfunction["createdby"] = 'initial-data';
                FunctionsService.Add(newfunction).then(
                    success => { console.log('function added successfully =>', newfunction.name); }
                    , err => { console.log('Error occured while adding function =>', newfunction.name); }
                )
            }
        },
        err => { console.log('Error on verifying function existing...', err) }
    );
}

function loadDomainModels(newdomainmodel) {
    var req = { "body": newdomainmodel };
    var res = { "status": function (val) { console.log('status: ', val); }, "send": function (val) { console.log('response: ', val); } };

    DomainModelGraphService.GetByName(newdomainmodel.name).then(
        existingmodel => {

            if (existingmodel != null && existingmodel != 'undefined') {
                newdomainmodel["_id"] = existingmodel._id;
                newdomainmodel["lastmodifiedby"] = 'initial-data';
                DomainModelController.Update(req, res).then(
                    success => { console.log('Domain Model updated successfully =>', existingmodel.name); }
                    , err => { console.log('Error occured while updating function =>', existingmodel.name); }
                )
            }
            else {
                newdomainmodel["createdby"] = 'initial-data';
                DomainModelController.Create(req, res).then(
                    success => { console.log('Domain Model added successfully =>', newdomainmodel.name); }
                    , err => { console.log('Error occured while adding Domain Model =>', newdomainmodel.name); }
                )
            }
        }
    );
}

function loadClientApplications(clientapplication) { 

    ClientApplicationService.GetByApplicationName(clientapplication.applicationname).then(
            existingClientApplication => {
                if (existingClientApplication != null && existingClientApplication != 'undefined') {
                    clientapplication["_id"] = existingClientApplication._id;
                    clientapplication["lastmodifiedby"] = 'initial-data';
                    ClientApplicationService.Update(clientapplication).then(
                        success => { console.log('Client application details updated successfully =>', existingClientApplication.applicationname); }
                        , err => { console.log('Error occured while updating client application details =>', existingClientApplication.applicationname); }
                    )
                }
                else {
    
                    clientapplication["createdby"] = 'initial-data';
                    ClientApplicationService.Add(clientapplication).then(
                        success => { console.log('Client application details added successfully =>', newfunction.applicationname); }
                        , err => { console.log('Error occured while adding Client application details =>', newfunction.applicationname); }
                    )
                }
            },
            err => { console.log('Error on verifying existing client application...', err) }
        );
}
