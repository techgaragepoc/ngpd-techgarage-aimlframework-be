function GetListfromDir(startPath, filter, filteredfiles) {
    var path = require('path'), fs = require('fs');
    //console.log('Starting from dir '+startPath+'/');

    if (!fs.existsSync(startPath)) {
        console.log("no dir ", startPath);
        return;
    }

    var files = fs.readdirSync(startPath);
    for (var i = 0; i < files.length; i++) {
        var filename = path.join(startPath, files[i]);
        var stat = fs.lstatSync(filename);
        if (stat.isDirectory()) {
            GetListfromDir(filename, filter, filteredfiles); //recurse
        }
        else if (filename.indexOf(filter) >= 0) {
            // console.log('-- found: ',filename);
            filteredfiles.push(filename);
        };
    }
};

const ClientApplications = [];
const jsonfiles = [];

GetListfromDir(__dirname, '.json', jsonfiles);

for (var i = 0; i < jsonfiles.length; i++) {
    var jsondata = require(jsonfiles[i]);
    if (jsondata) {  
        if(IsArray(jsondata)) {
            for(var j=0; j<jsondata.length;j++) {
                ClientApplications.push(jsondata[j]);
            }
        }
        else {
            ClientApplications.push(jsondata);
        }
    } 
}

function IsArray(value) {
    return value && value !== 'undefined' && typeof value === 'object' && value.constructor === Array;
}

module.exports = {
    ClientApplications
}