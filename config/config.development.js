var config = require('./config.global');

config.env = 'development';
config.logger.level = 'DEBUG';

// config.modules.adminbe.baseurl = 'http://localhost:5001/api';
// config.modules.enterpriseconnector.baseurl = 'http://localhost:83/econnector';

module.exports = config;