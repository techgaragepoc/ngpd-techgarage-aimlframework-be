var config = module.exports = {};

config.env = process.env.NODE_ENV || 'development';
config.hostname = 'localhost';
config.port = process.env.PORT || '5001';
config.timeout = process.env.APP_TIMEOUT || 5000000;

// Environment values
config.environment = {}
config.environment.development = 'development';
config.environment.test = 'test';
config.environment.production = 'production';

// Logger options
config.logger = {}
config.logger.appender = 'console';
config.logger.filepath = 'logs';
config.logger.filename = 'ml-model-be.log';
config.logger.label = 'ML Admin';
config.logger.level = 'ALL'; // OFF/FATAL/ERROR/WARN/INFO/DEBUG/TRACE/ALL
config.logger.dateformat = "MM-DD-YYYY";
config.logger.datetimeformat = "YYYY-MM-DD HH:mm:ss"

// Log Levels
config.logger.levels = {}

config.logger.levels.off = 'OFF';
config.logger.levels.fatal = 'FATAL';
config.logger.levels.error = 'ERROR';
config.logger.levels.warn = 'WARN';
config.logger.levels.info = 'INFO';
config.logger.levels.debug = 'DEBUG';
config.logger.levels.trace = 'TRACE';
config.logger.levels.all = 'ALL';

// mongo database
config.mongo = {}
config.mongo.uri = process.env.MONGO_URI || 'mongodb://localhost:27017/mladmindb';
config.mongo.hashsalt = '$2a$10$nRESwH7rxob00LPvaHHaKu';// '$2a$20$82JOTSUlcMpPUCiTPPlhvO';

// component urls
config.modules = {};
config.modules.adminbe = {};
config.modules.adminbe.baseurl = process.env.ADMIN_BE_URL || 'http://localhost:5001/api';
config.modules.enterpriseconnector = {};
config.modules.enterpriseconnector.baseurl = process.env.EC_URL || 'http://localhost:83/econnector'

// Model Admin 
config.tracker = {}
config.tracker.activitynamespace = 'com.mercer.ent.ml.admin.model';
config.tracker.version = '1.0.0';
config.tracker.lang = 'en';

// token secret
config.token = {}
config.token.secret = 'ilovescotchyscotch'
config.token.expiryinmins = 60

// security related 
config.security = {}
config.security.randompassword = {}
config.security.secretkey = '@lph@b3t@g@mm@'
config.security.randompassword.length = 10
config.security.randompassword.numbers = true

// mail related
config.mail = {}
config.mail.auth = {}
config.mail.allowsend = false
config.mail.protocol = "SMTP"
config.mail.secure = false // true for 465, false for other ports

// config.mail.service = "gmail"
// config.mail.server = "smtp.gmail.com"
// config.mail.port = 465
// config.mail.senderemail = "aimlapp@mercer.com"
// config.mail.auth.username = "user@gmail.com"
// config.mail.auth.password = "mailpassword"

config.mail.server = "smtp.ethereal.email"
config.mail.port = 587
config.mail.senderemail = "aimlapp@mercer.com"
config.mail.auth.username = "gkkmu5twcgmwfw37@ethereal.email"
config.mail.auth.password = "Z8TgJAEYPp6w7xddVc"

// External endpoints
config.externalendpoints = {}
config.externalendpoints.nlp = process.env.NLP_BE_URL || 'http://10.23.56.15:5003/api/';
config.externalendpoints.nlpVersionId = '0.1';

// nlp use cases related configurations
config.nlp = {};
config.nlp.defaults = {};
config.nlp.defaults.utterance = '-999999';
