
var tracker 	= require('../lib/event'),
	trackerMaster = require('../lib/eventmaster'),
	logger 		  = require('../lib/logger'),
    EventMaster = require('../models/eventmastermodel'),
	eventModel      = require('../models/eventmodel'),
	utility 	= require('../lib/utility'),
	fs 			= require('fs');

module.exports = {
    trackEvent: function (Event) {
		var t = tracker(Event);
	    t.track();
	},
	trackEventMaster: function(EventMaster){
		var t = trackerMaster(EventMaster)
		t.track();
	},
	checkParams: function(Event, req, res){
		/* istanbul ignore else */
		var output = true;
		if(!Event.AppKey){
			res.status(500).json({success: "false", message: "App Key is either blank or null"});
			output = false;
		}
		/* istanbul ignore else */
		if(!(Event.Source && utility.isJson(Event.Source))){
			res.status(500).json({success: "false", message: "Invalid JSON for Source"});
			output = false;
		}
		
		/* istanbul ignore else */
		else if(!Event.EventType &&  !Event.AppEventType){
			res.status(500).json({success: "false", message: "EventType/ AppEventType data is not provided"});
			output = false; 
		}
		/* istanbul ignore else */
		else if(Event.EventBody && !utility.isJson(Event.EventBody)){
			res.status(500).json({success: "false", message: "Invalid json for EventBody"});
			output = false;
		}
		if(output === false)
			return false;
		if(!Event.DateTime){
			var now = new Date();
			Event.DateTime = now.getTime() + (now.getTimezoneOffset() * 60000);
		}
		else if(Event.DateTime && !utility.isDateTime(Event.DateTime)){
				res.status(500).json({success: "false", message: "Invalid format for DateTime"});
				return false;
		}
		else{
				Event.DateTime = utility.getDateTime(Event.DateTime); 
		}
		
		Event.IP = Event.IP || req.ip ;
		return true;
	},
	checkGetParams: function(params, res){
		/* istanbul ignore else */
		if(params.startdt && !utility.isDate(params.startdt)){
			res.status(500).json({success: "false", message: "Invalid date format"});
			return false;
		}
		/* istanbul ignore else */
		if(params.enddt && !utility.isDate(params.enddt)){
			res.status(500).json({success: "false", message: "Invalid date format"});
			return false;
		}

		return true;

		
		
	},
	getDate: function(item){
		return utility.getDate(item);
	},
	isFileSync: function (aPath) 
	{  try	{    
			return fs.statSync(aPath).isFile();  
		} 
	   catch (e) {    
		   		throw e;      
		}
	},
	checkEventType: function( Event, res, callback ){
		/* istanbul ignore else */
		
			EventMaster.findOne({EventName: Event.EventType}, {EventStr: 1, EventName: 1},
			function (err, eventmaster) {
				/* istanbul ignore else */
				if (err) {
					callback("MongoDB Error: " + err); 
				}
				else if(!Event.EventType){
					callback(true);
				}
				else if(eventmaster){
					if(!Event.EventBody){
						callback("EventBody missing");
					}
					else{
					var EventTypeJson =JSON.parse(eventmaster.EventStr);
					if(Event.EventBody && !utility.isMatchingJsonStr(EventTypeJson, Event.EventBody)){
						callback("Invalid eventBody structure");
					}else
						callback(true);
					}
				}
				else{
					callback("Invalid EventType data");
				}
			});
		
	},
	checkEventMasterParams: function(EventMaster, req, res){
		/* istanbul ignore else */
		if(!EventMaster.EventName){
			res.status(500).json({success: "false", message: "EventName is either blank or null"});
			return false;
		}
		/* istanbul ignore else */
		if(!EventMaster.EventDesc){
			res.status(500).json({success: "false", message: "EventDesc is either blank or null"});
			return false;
		}
		/* istanbul ignore else */
		if(!(EventMaster.EventStr && utility.isJson(EventMaster.EventStr))){
			res.status(500).json({success: "false", message: "Invalid JSON for EventStr"});
			return false;
		}
		/* istanbul ignore else */
		if(!EventMaster.Updatedts){
			var now = new Date();
			EventMaster.Updatedts = now.getTime() + (now.getTimezoneOffset() * 60000);
		}
		return true;
	},
	returnObject: function(err, object, res){
		if (err){
			logger.error(err.message);
			res.status(500).json({errorMessage : err.message});
		}
		else
			res.status(200).json(object);
	},
	GetEventsByDate: function(req, res){
		var startDate = this.getDate(req.params.startdt);
		var endDate;
		if(req.params.enddt)
			endDate = this.getDate(req.params.enddt);
		else
			endDate = this.getDate(utility.getDateString(new Date()));
		eventModel.find({
			GlobalProfileId: req.params.gpid
			,DateTime: {'$gte': startDate,'$lte': endDate} 
		})
		.select({EventName: 1, EventDesc: 1, EventStr: 1, UpdatedBy: 1})
		.exec(function(err, Event){
			if (err){
				logger.error(err.message);
				res.status(500).json({errorMessage : err.message});
			}
			else{
				res.status(200).json(Event);
			}
		});
	},
	GetEventByAppKey: function(req, res){
		var startDate = this.getDate(req.params.startdt);
		var endDate;
		if(req.params.enddt)
			endDate = this.getDate(req.params.enddt);
		else
			endDate = this.getDate(utility.getDateString(new Date()));

		const query = {
			AppKey: req.params.appkey,
			DateTime: { '$gte': startDate,'$lte': endDate },
		};
		const eventType = req.query.EventType;
		if (eventType) {
			query.EventType = eventType;
		}
		eventModel.find(query)
		.exec(function(err, Event){
				res.status(200).json(Event);
		});
	},
	GetEvents_Date: function (validParams, req, res){
		if(validParams)
        {
            this.GetEventsByDate(req, res);
        }
	},
	GetEvents_AppKey: function(validParams, req, res){
		if(validParams)
			{
				this.GetEventByAppKey(req, res);
			}
	}

};