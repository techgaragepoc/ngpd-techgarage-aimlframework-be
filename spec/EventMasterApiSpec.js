var     chai        =   require('chai'),
        request     =   require('supertest-as-promised');
        httpStatus  =   require('http-status'),
        app         =   require('../index');

const expect = chai.expect;

var EventName = "Button_Click",
    EventDesc = "Button Click",
    EventStr = [{
                    "EventStatus": 
                    {   "applicationname":"Harmonise",
                        "servicename":"UpdateUserEventList",
                        "serviceproviderid":12345,
                        "activities": [
                        {
                            "category": "Events",
                            "subcategory": "LifeAndCic",
                            "name": "Sandbox",
                            "value": "Completed",
                            "date": "2014-05-06T00:00:00.000"
                        }]
                    },
                    "EventLocationReference" : "Welcome_Page",
                    "EventNote" : "",
                    "EventResponseText" : "",
                    "RelatedAuditId" : ""
                }],
    UpdatedBy = "manish-kumar5"

describe('## Tracker Event Master API', function(){
    describe('# Post /api/tracker/eventMaster', function(){
        it('Should save Event Master record', function(done){
            request(app)
            .post('/api/tracker/eventMaster')
            .send({
                    "EventMaster" : 
                    {
                        "EventName" : EventName,
                        "EventDesc" : EventDesc,
                        "EventStr" : EventStr,
                        "UpdatedBy" : UpdatedBy
                    }
                })
            .expect(httpStatus.OK)
            .then(function(res){
                expect(res.text).to.equal('{"success":"true","message":"Event Master data saved successfully"}');
                done();
            });
        });
        it('Should throw exception if Event Master Payload is not provided', function(done){
            request(app)
            .post('/api/tracker/eventMaster')
            .send({
                    "EventMaster1" : 
                    {
                        "EventName" : EventName,
                        "EventDesc" : EventDesc,
                        "EventStr" : EventStr,
                        "UpdatedBy" : UpdatedBy
                    }
                })
            .expect(httpStatus.INTERNAL_SERVER_ERROR)
            .then(function(res){
                expect(res.error.text).to.equal('{"success":"false","message":"EventMaster data missing in request"}');
                done();
            });
        });
        it('Should throw exception if Event Name is not provided', function(done){
            request(app)
            .post('/api/tracker/eventMaster')
            .send({
                    "EventMaster" : 
                    {
                        "EventName" : "",
                        "EventDesc" : EventDesc,
                        "EventStr" : EventStr,
                        "UpdatedBy" : UpdatedBy
                    }
                })
            .expect(httpStatus.INTERNAL_SERVER_ERROR)
            .then(function(res){
                expect(res.error.text).to.equal('{"success":"false","message":"EventName is either blank or null"}');
                done();
            });
        });
        it('Should throw exception if Event Desc is not provided', function(done){
            request(app)
            .post('/api/tracker/eventMaster')
            .send({
                    "EventMaster" : 
                    {
                        "EventName" : EventName,
                        "EventDesc" : "",
                        "EventStr" : EventStr,
                        "UpdatedBy" : UpdatedBy
                    }
                })
            .expect(httpStatus.INTERNAL_SERVER_ERROR)
            .then(function(res){
                expect(res.error.text).to.equal('{"success":"false","message":"EventDesc is either blank or null"}');
                done();
            });
        });
        it('Should throw exception if EventStr is not provided', function(done){
            request(app)
            .post('/api/tracker/eventMaster')
            .send({
                    "EventMaster" : 
                    {
                        "EventName" : EventName,
                        "EventDesc" : EventDesc,
                        "EventStr" : "",
                        "UpdatedBy" : UpdatedBy
                    }
                })
            .expect(httpStatus.INTERNAL_SERVER_ERROR)
            .then(function(res){
                expect(res.error.text).to.equal('{"success":"false","message":"Invalid JSON for EventStr"}');
                done();
            });
        });
        it('Should throw exception if EventStr is not valid json', function(done){
            request(app)
            .post('/api/tracker/eventMaster')
            .send({
                    "EventMaster" : 
                    {
                        "EventName" : EventName,
                        "EventDesc" : EventDesc,
                        "EventStr" : "test string",
                        "UpdatedBy" : UpdatedBy
                    }
                })
            .expect(httpStatus.INTERNAL_SERVER_ERROR)
            .then(function(res){
                expect(res.error.text).to.equal('{"success":"false","message":"Invalid JSON for EventStr"}');
                done();
            });
        });
    });
    describe('# Get /api/tracker/eventMaster/..', function(){
        it('Should return more that zero records', function(done){
            request(app)
            .get('/api/tracker/eventMaster?EventName=' +  EventName)
            .expect(httpStatus.OK)
            .then(function(res){
                expect(true).to.equal(Object.keys(res.body).length > 0);
                done();
            });
        });
    });
});