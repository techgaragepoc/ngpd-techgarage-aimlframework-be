var     chai        =   require('chai'),
        request     =   require('supertest-as-promised');
        httpStatus  =   require('http-status'),
        app         =   require('../index');

const expect = chai.expect;

var AppKey = "GBM",
    Source = [{"platform": "web", "Agent": "IE11" }],
    IP = "127.0.0.1",
    GlobalProfileId = "1026110",
    EventType = "Button_Click",
    AppEventType = "Address_Change",
    EventBody = [{
                    "EventStatus": 
                    {   "applicationname":"Harmonise",
                        "servicename":"UpdateUserEventList",
                        "serviceproviderid":12345,
                        "activities": [
                        {
                            "category": "Events",
                            "subcategory": "LifeAndCic",
                            "name": "Sandbox",
                            "value": "Completed",
                            "date": "2014-05-06T00:00:00.000"
                        }]
                    },
                    "EventLocationReference" : "Welcome_Page",
                    "EventNote" : "",
                    "EventResponseText" : "",
                    "RelatedAuditId" : ""
                }],
    UpdatedBy = "manish-kumar5";

var EventName = "Button_Click",
    EventDesc = "Button Click",
    EventStr = [{
                    "EventStatus": 
                    {   "applicationname":"Harmonise",
                        "servicename":"UpdateUserEventList",
                        "serviceproviderid":12345,
                        "activities": [
                        {
                            "category": "Events",
                            "subcategory": "LifeAndCic",
                            "name": "Sandbox",
                            "value": "Completed",
                            "date": "2014-05-06T00:00:00.000"
                        }]
                    },
                    "EventLocationReference" : "Welcome_Page",
                    "EventNote" : "",
                    "EventResponseText" : "",
                    "RelatedAuditId" : ""
                }],
    UpdatedBy = "manish-kumar5"    

var startDate = getCurrentDate();
var endDate = getTomorrowDate();

//console.log(startDate + '  ' + endDate);

describe('## Tracker Event API', function(){
    describe('# Put /api/tracker/event', function(){
        it('Should save Event Master record', function(done){
            request(app)
            .post('/api/tracker/eventMaster')
            .send({
                    "EventMaster" : 
                    {
                        "EventName" : EventName,
                        "EventDesc" : EventDesc,
                        "EventStr" : EventStr,
                        "UpdatedBy" : UpdatedBy
                    }
                })
            .expect(httpStatus.OK)
            .then(function(res){
                expect(res.text).to.equal('{"success":"true","message":"Event Master data saved successfully"}');
                done();
            });
        });
        it('Should throw exception if Event Payload is not provided', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "Event1" : 
                    {
                        "AppKey" : "",
                        "Source" : Source,
                        "IP" : IP,
                        "GlobalProfileId" : GlobalProfileId,
                        "EventType" : EventType,
                        "AppEventType" : AppEventType,
                        "EventBody" : EventBody
                    }
                })
            .expect(httpStatus.INTERNAL_SERVER_ERROR)
            .then(function(res){
                expect(res.error.text).to.equal('{"success":"false","message":"Event data missing in request"}');
                done();
            });
        });
        it('Should throw exception if AppKey blank', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : "",
                        "Source" : Source,
                        "IP" : IP,
                        "GlobalProfileId" : GlobalProfileId,
                        "EventType" : EventType,
                        "AppEventType" : AppEventType,
                        "EventBody" : EventBody,
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"App Key is either blank or null"}');
                    done();
                });
        });
        it('Should throw exception if AppKey is not provided', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "Source" : Source,
                        "IP" : IP,
                        "GlobalProfileId" : GlobalProfileId,
                        "EventType" : EventType,
                        "AppEventType" : AppEventType,
                        "EventBody" : EventBody,
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"App Key is either blank or null"}');
                    done();
                });
        });
        it('Should throw exception if Source has invalid json', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : "Invalid Json",
                        "IP" : IP,
                        "GlobalProfileId" : GlobalProfileId,
                        "EventType" : EventType,
                        "AppEventType" : AppEventType,
                        "EventBody" : EventBody,
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"Invalid JSON for Source"}');
                    done();
                });
        });
        it('Should throw exception if Source is not provided', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "IP" : IP,
                        "GlobalProfileId" : GlobalProfileId,
                        "EventType" : EventType,
                        "AppEventType" : AppEventType,
                        "EventBody" : EventBody,
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"Invalid JSON for Source"}');
                    done();
                });
        });
        it('Should not throw exception if IP is not provided', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "GlobalProfileId" : GlobalProfileId,
                        "EventType" : EventType,
                        "AppEventType" : AppEventType,
                        "EventBody" : EventBody,
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.OK)
                .then(function(res){
                    expect(res.text).to.equal('{"success":"true","message":"Event data saved successfully"}');
                    done();
                });
        });
        it('Should not throw exception if IP is provided blank', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "IP" : "",
                        "GlobalProfileId" : GlobalProfileId,
                        "EventType" : EventType,
                        "AppEventType" : AppEventType,
                        "EventBody" : EventBody,
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.OK)
                .then(function(res){
                    expect(res.text).to.equal('{"success":"true","message":"Event data saved successfully"}');
                    done();
                });
        });
        it('Should throw exception if GlobalProfileId (Global Profile ID) is not provided', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "IP": IP, 
                        "EventType" : EventType,
                        "AppEventType" : AppEventType,
                        "EventBody" : EventBody,
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.OK)
                .then(function(res){
                    expect(res.text).to.equal('{"success":"true","message":"Event data saved successfully"}');
                    done();
                });
        });
        it('Should throw exception if GlobalProfileId  is provided blank', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "IP": IP, 
                        "GlobalProfileId"  : "",
                        "EventType" : EventType,
                        "AppEventType" : AppEventType,
                        "EventBody" : EventBody,
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.OK)
                .then(function(res){
                    expect(res.text).to.equal('{"success":"true","message":"Event data saved successfully"}');
                    done();
                });
        });
        it('Should throw exception if EventType and App Event Type both are not provided', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "IP": IP, 
                        "GlobalProfileId"  : GlobalProfileId,
                        "EventBody" : EventBody,
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"EventType/ AppEventType data is not provided"}');
                    done();
                });
        });
        it('Should throw exception if Event Type is not provided and AppEventType is provided blank', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "IP": IP, 
                        "GlobalProfileId"  : GlobalProfileId,
                        "EventType" : "",
                        "AppEventType" : "",
                        "EventBody" : EventBody,
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"EventType/ AppEventType data is not provided"}');
                    done();
                });
        });
        it('Should through exception if EventBody is not a valid json', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "IP": IP, 
                        "GlobalProfileId"  : GlobalProfileId,
                        "EventType" : EventType,
                        "AppEventType" : AppEventType,
                        "EventBody" : "Invalid Json",
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"Invalid json for EventBody"}');
                    done();
                });
        });
        it('Should throw exception if Event Type is not provided and AppEventType is provided blank', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "IP": IP, 
                        "GlobalProfileId"  : GlobalProfileId,
                        "EventType" : "",
                        "AppEventType" : "",
                        "EventBody" : EventBody,
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"EventType/ AppEventType data is not provided"}');
                    done();
                });
        });
        it('Should throw exception if DateTime format (YYYY-MM-DDTHH:mm:ss Z) is not correct', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "IP": IP, 
                        "GlobalProfileId"  : GlobalProfileId,
                        "EventType" : "Button_Click",
                        "AppEventType" : "",
                        "EventBody" : EventBody,
                        "DateTime" : "2016-11-24T12:45 -330",
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"Invalid format for DateTime"}');
                    done();
                });
        });
        it('Should throw exception if invalid eventtype data is provided', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "IP": IP, 
                        "GlobalProfileId"  : GlobalProfileId,
                        "EventType" : "Button_Click1",
                        "AppEventType" : "",
                        "EventBody" : EventBody,
                        "DateTime" : "2016-11-24T12:45:23 -330",
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"Invalid EventType data"}');
                    done();
                });
        });
        it('Should throw exception if EventType is provided and EventBody is missing', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "IP": IP, 
                        "GlobalProfileId"  : GlobalProfileId,
                        "EventType" : "Button_Click",
                        "AppEventType" : "",
                        "EventBody" :"",
                        "DateTime" : "2016-11-24T12:45:23 -330",
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"EventBody missing"}');
                    done();
                });
        });
        it('Should throw exception if invalid EventBody structure is provided', function(done){
            request(app)
            .put('/api/tracker/event')
            .send({
                    "event" : 
                    {
                        "AppKey" : AppKey,
                        "Source" : Source,
                        "IP": IP, 
                        "GlobalProfileId"  : GlobalProfileId,
                        "EventType" : "Button_Click",
                        "AppEventType" : "",
                        "EventBody" :[{data1: {invalidtestevent: '1'}}],
                        "DateTime" : "2016-11-24T12:45:23 -330",
                        "UpdatedBy": UpdatedBy
                    }
                })
                .expect(httpStatus.INTERNAL_SERVER_ERROR)
                .then(function(res){
                    expect(res.error.text).to.equal('{"success":"false","message":"Invalid eventBody structure"}');
                    done();
                });
        });
        it('Should throw exception if invalid EventBody structure is provided', function(done){
            request(app)
            .post('/api//tracker/dataimport')
            .expect(httpStatus.OK)
            .then(function(res){
                    expect(res.text).to.equal('{"success":"true","message":"File data has beem imported succesfully"}');
                    done();
                });
        });
    });
    describe('# Get /api/tracker/event/..', function(){
        it('Should return more that zero records', function(done){
            request(app)
            .get('/api/tracker/event/userevent/' + GlobalProfileId +'/' + EventType + '/' + AppEventType)
            .expect(httpStatus.OK)
            .then(function(res){
                expect(true).to.equal(Object.keys(res.body).length > 0);
                done();
            });
        });
       it('Should return more that zero records', function(done){
            request(app)
            .get('/api/tracker/event/user/' + GlobalProfileId +'/' + startDate + '/' + endDate)
            .expect(httpStatus.OK)
            .then(function(res){
                expect(true).to.equal(Object.keys(res.body).length > 0);
                done();
            });
        });
      it('Should return more that zero records', function(done){
            request(app)
            .get('/api/tracker/event/user/' + GlobalProfileId +'/' + startDate)
            .expect(httpStatus.OK)
            .then(function(res){
                expect(true).to.equal(Object.keys(res.body).length > 0);
                done();
            });
        });
        it('Should return more that zero records', function(done){
            request(app)
            .get('/api/tracker/event/app/' + AppKey +'/' + startDate + '/' + endDate)
            .expect(httpStatus.OK)
            .then(function(res){
                expect(true).to.equal(Object.keys(res.body).length > 0);
                done();
            });
        });
        it('Should return error if invalid date format is passed for start date', function(done){
            request(app)
            .get('/api/tracker/event/app/' + AppKey +'/' + new Date() + '/' + endDate)
            .expect(httpStatus.INTERNAL_SERVER_ERROR)
            .then(function(res){
                expect(res.error.text).to.equal('{"success":"false","message":"Invalid date format"}');
                done();
            });
        });
        it('Should return more that zero records', function(done){
            request(app)
            .get('/api/tracker/event/app/' + AppKey +'/' + startDate )
            .expect(httpStatus.OK)
            .then(function(res){
                expect(true).to.equal(Object.keys(res.body).length > 0);
                done();
            });
        });
        it('Should return more that zero records', function(done){
            request(app)
            .get('/api/tracker/event?AppKey=' +  AppKey)
            .expect(httpStatus.OK)
            .then(function(res){
                expect(true).to.equal(Object.keys(res.body).length > 0);
                done();
            });
        });
        
    });
    
});

function getCurrentDate(){
    var now = new Date();
    var today = new Date(now.getTime() + (now.getTimezoneOffset() * 60000));
    
    var dd = today.getDate().toString();
    var mm = (today.getMonth()+1).toString(); //January is 0!
    var yyyy = today.getFullYear().toString();

    return ((mm.length < 2)? /* istanbul ignore next */ ("0" + mm): /* istanbul ignore next */mm)  + "-" + ((dd.length < 2)? /* istanbul ignore next */( "0" + dd) : /* istanbul ignore next */dd) + "-" + yyyy;
}

function getTomorrowDate(){
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    var dd = tomorrow.getDate().toString();
    var mm = (tomorrow.getMonth()+1).toString(); //January is 0!
    var yyyy = tomorrow.getFullYear().toString();

     return ((mm.length < 2)?/* istanbul ignore next */ ("0" + mm): /* istanbul ignore next */ mm)  + "-" + ((dd.length < 2)?/* istanbul ignore next */ ( "0" + dd) :/* istanbul ignore next */ dd) + "-" + yyyy;

}