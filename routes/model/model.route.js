const express = require('express');
const router = express.Router();
var controller = require('../../controllers/model/models.controller');

// routes
router.post('/', create);
router.get('/', getAll);
router.get('/:_id', getById);
router.put('/:_id', update);
router.delete('/:_id', _delete);

function getAll(req, res) {
    controller.getAll(req, res);
}

function getById(req, res) {
    controller.getById(req, res);
}

function update(req, res) {
    controller.update(req, res);
}

function _delete(req, res) {
    controller._delete(req, res);
}

function create(req, res) {
    controller.create(req, res);
}

module.exports = router;