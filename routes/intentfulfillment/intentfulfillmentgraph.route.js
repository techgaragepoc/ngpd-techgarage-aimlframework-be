const express = require('express');
const router = express.Router();
var { IntentFulfillmentModelController } = require('../../controllers/intentfulfillment/');


router.get('/', function (req, res) {
    IntentFulfillmentModelController.GetAll(req, res);
});

router.get('/:graphid', function (req, res) {
   IntentFulfillmentModelController.Get(req, res);
});

router.get('/name/:graphname', function (req, res) {
    IntentFulfillmentModelController.GetByName(req, res);
});

router.post('/', function (req, res) {
    console.log('post => intentffmodelgraph => ');
    IntentFulfillmentModelController.Create(req, res);
});

router.put('/', function (req, res) {
    IntentFulfillmentModelController.Update(req, res);
});

router.delete('/:graphid', function (req, res) {
    IntentFulfillmentModelController.Delete(req, res);
});

module.exports = router;
