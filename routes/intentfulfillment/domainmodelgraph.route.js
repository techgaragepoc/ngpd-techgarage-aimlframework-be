const express = require('express');
const router = express.Router();
var { DomainModelController } = require('../../controllers/intentfulfillment/');


router.get('/', function (req, res) {
    DomainModelController.GetAll(req, res);
});

router.get('/:graphid', function (req, res) {
    DomainModelController.Get(req, res);
});

router.get('/name/:graphname', function (req, res) {
    DomainModelController.GetByName(req, res);
});

router.post('/', function (req, res) {
    DomainModelController.Create(req, res);
});

router.put('/', function (req, res) {
    DomainModelController.Update(req, res);
});

router.delete('/:graphid', function (req, res) {
    DomainModelController.Delete(req, res);
});

module.exports = router;
