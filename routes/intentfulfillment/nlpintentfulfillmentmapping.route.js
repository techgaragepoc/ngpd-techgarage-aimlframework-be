const express = require('express');
const router = express.Router();
var { NLPIntentFulfillmentMappingController } = require('../../controllers/intentfulfillment/');


router.get('/', function (req, res) {
    NLPIntentFulfillmentMappingController.GetAll(req, res);
});

router.get('/:mappingid', function (req, res) {
    NLPIntentFulfillmentMappingController.Get(req, res);
});

router.get('/nlp/:nlpmodelid/:nlpintentid', function (req, res) {
    NLPIntentFulfillmentMappingController.GetByNLPModelAndIntent(req, res);
});

router.get('/intentffmodel/:intentffmodelid', function (req, res) {
    NLPIntentFulfillmentMappingController.GetByIntentFulfillmentModelGraphId(req, res);
});


router.post('/', function (req, res) {
    NLPIntentFulfillmentMappingController.Add(req, res);
});

router.put('/', function (req, res) {
    NLPIntentFulfillmentMappingController.Update(req, res);
});

router.put('/status/', function (req, res) {
    NLPIntentFulfillmentMappingController.UpdateImplementationStatus(req, res);
});

router.delete('/:intentffmodelid', function (req, res) {
    NLPIntentFulfillmentMappingController.Delete(req, res);
});

module.exports = router;
