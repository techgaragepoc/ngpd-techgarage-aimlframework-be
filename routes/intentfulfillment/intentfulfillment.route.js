const express = require('express');
const router = express.Router();
var { DomainModelController, IntentFulfillmentModelController,
    FunctionsController, SynonymsController, NLPIntentFulfillmentMappingController,
    IntentFulfillmentEngineController
} = require('../../controllers/intentfulfillment/');

const Utility = require("../../lib/utility");

// intentfulfillment model
router.get('/model/', function (req, res) {
    IntentFulfillmentModelController.GetAllTransformed(req, res);
});

router.get('/model/:transformmodelid', function (req, res) {
    IntentFulfillmentModelController.GetTransformed(req, res);
});

router.get('/model/graphid/:graphid', function (req, res) {
    IntentFulfillmentModelController.GetTransformedByGraphId(req, res);
});

router.get('/model/name/:modelname', function (req, res) {
    IntentFulfillmentModelController.GetTransformedByName(req, res);
});

router.put('/model/nodeproperties', function (req, res) {
    IntentFulfillmentModelController.UpdateTransformedNodeProperties(req, res);
});

router.get('/model/project/:projectid', function (req, res) {
    IntentFulfillmentModelController.GetProjectDetailById(req, res);
});


//domain model
router.get('/domainmodel/', function (req, res) {
    DomainModelController.GetAllTransformed(req, res);
});

router.get('/domainmodel/:transformmodelid', function (req, res) {
    DomainModelController.GetTransformed(req, res);
});

router.get('/domainmodel/name/:modelname', function (req, res) {
    DomainModelController.GetTransformedByName(req, res);
});

//node dataset mapping
router.get('/nodedatasetmappings/', function (req, res) {
    DomainModelController.GetAllNodeDatasetMappings(req, res);
});

router.get('/nodedatasetmappings/datasetid/:datasetid', function (req, res) {
    DomainModelController.GetNodeDatasetMappingByDatasetId(req, res);
});

router.get('/nodedatasetmappings/modelnode/:modelgraphid/:nodeid', function (req, res) {
    DomainModelController.GetNodeDatasetMappingByModelNode(req, res);
});


router.post('/nodedatasetmappings/', function (req, res) {
    DomainModelController.AddNodeDatasetMapping(req, res);
});

router.put('/nodedatasetmappings/', function (req, res) {
    DomainModelController.UpdateNodeDatasetMapping(req, res);
});

router.delete('/nodedatasetmappings/:mappingid', function (req, res) {
    DomainModelController.DeleteNodeDatasetMapping(req, res);
});


//nlp-intent fulfillment mapping
router.get('/nlpintentfulfillmentmappings/', function (req, res) {
    NLPIntentFulfillmentMappingController.GetAll(req, res);
});


//synonyms
router.get('/synonyms/', function (req, res) {
    SynonymsController.GetAll(req, res);
});

router.get('/synonyms/:synonymid', function (req, res) {
    SynonymsController.Get(req, res);
});

router.get('/synonyms/reference/:synonymrefid', function (req, res) {
    SynonymsController.GetByRefId(req, res);
});

//functions
router.get('/functions/', function (req, res) {
    FunctionsController.GetAll(req, res);
});

router.get('/functions/:functionid', function (req, res) {
    FunctionsController.Get(req, res);
});

router.get('/functions/name/:functionname', function (req, res) {
    FunctionsController.GetByName(req, res);
});

// intentfulfillment Engine
router.post('/engine/identifyandexecute/', function (req, res) {

    IntentFulfillmentEngineController.InitiateSession()
        .then(sessionId => {

            IntentFulfillmentEngineController.Initialize(sessionId)
                .then(startprocess => {

                    var requestbody = Utility.AsObject(req.body);
                    var nlpmodel = Utility.GetKeyValue(requestbody, 'nlpModel');
                    var nlpintent = Utility.GetKeyValue(requestbody, 'nlpIntent');
                    var nlpPOS = Utility.GetKeyValue(requestbody, 'nlpPOS');

                    IntentFulfillmentEngineController.IdentifyModel(nlpmodel, nlpintent, nlpPOS)
                        .then(intentffmodel => {

                            //req.body["intentffmodel"] = intentffmodel;
                            IntentFulfillmentEngineController.Execute(sessionId, intentffmodel, nlpPOS)
                                .then(result => {
                                    IntentFulfillmentEngineController.responseWithResult(res, 200, false, result)
                                }); // Execute

                        }); // Identify Model  
                }); // Initialize

        });

});

router.get('/engine/execute/:sessionid/:level', function(req, res) {
    var sessionId = req.params.sessionid;
    var level = req.params.level;
    IntentFulfillmentEngineController.GetSessionLogs(sessionId, level)
        .then(result => {
            IntentFulfillmentEngineController.responseWithResult(res, 200, false, result);
        });
});

router.post('/engine/execute/', function (req, res) {
    var requestbody = Utility.AsObject(req.body);
    var nlpPOS = Utility.GetKeyValue(requestbody, 'nlpPOS');
    var intentffmodel = Utility.GetKeyValue(requestbody, 'intentffmodel');

    IntentFulfillmentEngineController.InitiateSession()
        .then(sessionId => {

            IntentFulfillmentEngineController.Initialize(sessionId)
                .then(startprocess => {
                    console.log('intentffmodel =>', JSON.stringify(intentffmodel));
                    IntentFulfillmentEngineController.Execute(sessionId, intentffmodel, nlpPOS)
                        .then(result => {
                            result["logsessionid"] = sessionId;
                            IntentFulfillmentEngineController.responseWithResult(res, 200, false, result);
                        }); // Execute
                }); // Initialize

            // IntentFulfillmentEngineController.responseWithResult(res, 200, false, sessionId);
        });
});


module.exports = router;
