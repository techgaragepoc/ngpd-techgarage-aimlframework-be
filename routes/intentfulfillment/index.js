const DomainModelGraphRoute = require('./domainmodelgraph.route');
const IntentFulfillmentGraphRoute = require('./intentfulfillmentgraph.route');
const IntentFulfillmentRoute = require('./intentfulfillment.route');
const NLPIntentFulfillmentMappingRoute = require('./nlpintentfulfillmentmapping.route');

module.exports = {
    DomainModelGraphRoute,
    IntentFulfillmentGraphRoute,
    IntentFulfillmentRoute,
    NLPIntentFulfillmentMappingRoute
}