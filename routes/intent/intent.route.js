const express = require('express');
const router = express.Router();
var dataCtrl = require('../../controllers/intent/intent.controller.js');

router.get('/intentCategories', function (req, res) {
    dataCtrl.getByModel(req, res);
});
router.get('/getAll', function (req, res) {
    // console.log("In intentCategories routes");
    dataCtrl.getAll(req, res);
});
router.post('/createIntent', function (req, res) {
    // console.log("In create Intent routes");
    dataCtrl.createIntent(req, res);
});
router.delete('/:_id', function (req, res) {
    // console.log("In create Intent routes");
    dataCtrl.deleteIntent(req, res);
});

router.post('/addIntentUtterance', function (req, res) {
    // console.log("In add Intent utterance routes");
    dataCtrl.addIntentUtterance(req, res);
});
router.get('/utterance/:_id', function (req, res) {
    // console.log("In add Intent utterance routes");
    dataCtrl.getIntentUtteranceById(req, res);
});
router.get('/getIntentUtterance', function (req, res) {
    // console.log("In add Intent utterance routes");
    dataCtrl.getIntentUtterances(req, res);
});
router.get('/utterances/:_modelName', function (req, res) {
    // console.log("In add Intent utterance routes");
    dataCtrl.getUtterancesByModel(req, res);
});
router.put('/utterance', function (req, res) {
    // console.log("In add Intent utterance routes");
    dataCtrl.updateUtterance(req, res);
});
router.put('/utteranceScores', function (req, res) {
    // console.log("In add Intent utterance routes");
    dataCtrl.updateUtteranceScores(req, res);
});
router.delete('/IntentUtterance/:_id/:_modelName', function (req, res) {
    // console.log("In add Intent utterance routes");
    dataCtrl.deleteIntentUtterance(req, res);
});
router.post('/createEntity', function (req, res) {
    //console.log("In create Intent routes",req.params.intentData);
    dataCtrl.createEntity(req, res);
});

router.get('/getIntents', function (req, res) {
    // console.log("In create Intent routes", req.params.intentData);
    dataCtrl.getIntents(req, res);
});
router.get('/getIntentsByUser', function (req, res) {
    // console.log("In create Intent routes", req.params.intentData);
    dataCtrl.getIntentsByUser(req, res);
});
router.get('/getTree', function (req, res) {
    // console.log("In create Intent routes", req.params.intentData);
    dataCtrl.getTree(req, res);
});
router.get('/getTreeByUser', function (req, res) {
    // console.log("In create Intent routes", req.params.intentData);
    dataCtrl.getTreeByUser(req, res);
});

router.post('/trainModel', function (req, res) {
    //console.log("In create Intent routes",req.params.intentData);
    dataCtrl.trainModel(req, res);
});
router.post('/trainEntireModel', function (req, res) {
    //console.log("In create Intent routes",req.params.intentData);
    dataCtrl.trainEntireModel(req, res);
});

module.exports = router;