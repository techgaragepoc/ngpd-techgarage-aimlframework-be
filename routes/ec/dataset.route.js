const express = require('express');
const router = express.Router();
var datasetCtrl = require('../../controllers/ec/dataset.controller.js');


router.get('/', function (req, res) {
    datasetCtrl.GetAll(req, res);
});

router.get('/:datasetid', function (req, res) {
    datasetCtrl.Get(req, res);
});

router.get('/name/:datasetname', function (req, res) {
    datasetCtrl.GetByName(req, res);
});

router.post('/', function (req, res) {
    datasetCtrl.Create(req, res);
});

router.put('/', function (req, res) {
    datasetCtrl.Update(req, res);
});

router.delete('/:datasetid', function (req, res) {
    datasetCtrl.Delete(req, res);
});


module.exports = router;
