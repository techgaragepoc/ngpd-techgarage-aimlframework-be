const express = require('express');
const router = express.Router();
var connectorCtrl = require('../../controllers/ec/connector.controller.js');


router.get('/', function (req, res) {
    connectorCtrl.GetAll(req, res);
});

router.get('/:connectorid', function (req, res) {
    connectorCtrl.Get(req, res);
});

router.get('/name/:connectorname', function (req, res) {
    connectorCtrl.GetByName(req, res);
});

router.post('/', function (req, res) {
    connectorCtrl.Create(req, res);
});

router.put('/', function (req, res) {
    connectorCtrl.Update(req, res);
});

router.delete('/:connectorid', function (req, res) {
    connectorCtrl.Delete(req, res);
});

module.exports = router;
