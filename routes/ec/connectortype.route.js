const express = require('express');
const router = express.Router();
var connectorTypeCtrl = require('../../controllers/ec/connectortype.controller.js');


router.get('/', function (req, res) {
    connectorTypeCtrl.GetAll(req, res);
});

router.get('/:connectortypeid', function (req, res) {
    connectorTypeCtrl.Get(req, res);
});

router.get('/name/:connectortypename', function (req, res) {
    connectorTypeCtrl.GetByName(req, res);
});

router.get('/category/:category/subcategory/:subcategory', function (req, res) {
    connectorTypeCtrl.GetByCatgSubCatg(req, res);
});

router.post('/', function (req, res) {
    connectorTypeCtrl.Create(req, res);
});

router.put('/', function (req, res) {
    connectorTypeCtrl.Update(req, res);
});

router.delete('/:connectortypeid', function (req, res) {
    connectorTypeCtrl.Delete(req, res);
});

module.exports = router;
