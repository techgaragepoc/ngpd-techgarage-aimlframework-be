const express = require('express');
const router = express.Router();
var dataCtrl = require('../../controllers/ec/data.controller.js');


router.get('/', function (req, res) {
    dataCtrl.GetData(req, res);
});

router.get('/providerlist', function (req, res) {
    dataCtrl.GetProviders(req, res);
});

router.post('/testconnection', function (req, res) {
    dataCtrl.TestConnection(req, res);
});

module.exports = router;