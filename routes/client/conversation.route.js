const express = require('express');
const router = express.Router();
var conversationCtrl = require('../../controllers/client/conversation.controller.js');


router.get('/', function (req, res) {
    conversationCtrl.ShowHelp(req, res);
});

/**
 * To get the whole conversation for last session
 */
router.get('/chathistory', function (req, res) {
    conversationCtrl.GetChatHistory(req, res);
});

/**
 * To get the whole conversation for that session id
 */
router.get('/chathistory/:sessionid', function (req, res) {
    conversationCtrl.GetChatHistory(req, res);
});

/**
 * To post messages and get the response
 */
router.post('/', function (req, res) {
    conversationCtrl.ProcessMessage(req, res);
});

module.exports = router;
