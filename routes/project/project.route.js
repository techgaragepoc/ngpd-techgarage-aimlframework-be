const express = require('express');
const router = express.Router();
const projCtrl = require('../../controllers/project/project.controller');

router.get('/category', function (req, res) {
    // console.log('category route called');
    projCtrl.getCategories(req, res);
});

router.post('/category', function (req, res) {
    projCtrl.createCategory(req, res);
});

router.get('/options', function (req, res) {
    projCtrl.getProjectOptions(req, res);
});

router.get('/:_id', function (req, res) {
    projCtrl.getById(req, res);
});

router.get('/user/:_userId', function (req, res) {
    projCtrl.getByUserId(req, res);
});

router.get('/name/:_name', function (req, res) {
    projCtrl.getByName(req, res);
});

router.put('/:_id', function (req, res) {
    projCtrl.update(req, res);
});

router.delete('/:_id', function (req, res) {
    projCtrl._delete(req, res);
});

router.get('/', function (req, res) {
    projCtrl.getAll(req, res);
});

router.post('/', function (req, res) {
    projCtrl.create(req, res);
});

module.exports = router;
