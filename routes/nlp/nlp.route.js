const express = require('express');
const router = express.Router();
var nlpCtrl = require('../../controllers/nlp/nlp.controller');

router.post('/model', function (req, res) {
    nlpCtrl.createModel(req, res);
});
router.get('/:_id', function (req, res) {
    nlpCtrl.getById(req, res);
});
router.get('/name/:_name', function (req, res) {
    nlpCtrl.getByName(req, res);
});
router.get('/user/:_id', function (req, res) {
    nlpCtrl.getByUserId(req, res);
});
router.get('/', function (req, res) {
    nlpCtrl.getAll(req, res);
});
router.post('/', function (req, res) {
    nlpCtrl.createNlpLanguageModel(req, res);
});
router.put('/', function (req, res) {
    nlpCtrl.updateLanguageModel(req, res);
});
router.delete('/:_id', function (req, res) {
    // console.log("In create Intent routes");
    nlpCtrl.deleteLanguageModel(req, res);
});
router.get('/download/:_id', function (req, res) {
    nlpCtrl.downloadLanguageModel(req, res);
});
module.exports = router;