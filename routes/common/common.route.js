const express = require('express');
const router = express.Router();
var commonCtrl = require('../../controllers/common/common.controller');

router.get('/culture/:code', function (req, res) {
    commonCtrl.getCultureByCode(req, res);
});

router.get('/culture', function (req, res) {
    commonCtrl.getAllCultures(req, res);
});

module.exports = router;