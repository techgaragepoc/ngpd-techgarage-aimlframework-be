var log4js = require('log4js'),
    config = require('../config');

// ********** Function to get current date

var currentdate = new Date(); 
currentdate = currentdate.getDate() + "_" 
					+ (currentdate.getMonth()+1)  + "_" 
					+ currentdate.getFullYear();


//******* Initialize log4js 
log4js.loadAppender(config.logger.appender);
var logger = log4js.getLogger(config.logger.label);
logger.setLevel(config.logger.level);


module.exports = logger;


