
var config = require('../config'),
    moment = require('moment'),
    crypto = require('crypto-js'),
    bcrypt = require('bcryptjs'),
    passwordGen = require('generate-password'),
    mailer = require("nodemailer"),
    Q = require('q'),
    mongoose = require('mongoose');

module.exports = {
    isJson: function (item) {
        item = typeof item !== "string"
            ? /* istanbul ignore next */JSON.stringify(item)
            : /* istanbul ignore next */item;

        try {
            item = JSON.parse(item);
        } catch (e) {
            return false;
        }
        /* istanbul ignore else */
        if (typeof item === "object" && item !== null) {
            return true;
        }

        return false;
    },
    isDate: function (item) {
        return moment(item, config.logger.dateformat, true).isValid();
    },
    isDateTime: function (item) {
        var dateTimeArray = item.split('T');
        if (dateTimeArray.length !== 2) {
            return false;
        } else {
            var timeArray = dateTimeArray[1].split(' ');
            if (timeArray.length !== 2) {
                return false;
            }
            if (isNaN(timeArray[1])) {
                return false;
            }

            return moment(dateTimeArray[0] + " " + timeArray[0], config.logger.datetimeformat, true).isValid();
        }
    },
    getDateTime: function (item) {
        var dateTimeArray = item.split('T');
        var timeArray = dateTimeArray[1].split(' ');
        return moment(dateTimeArray[0] + " " + timeArray[0], config.logger.datetimeformat).valueOf() + (timeArray[1] * 60 * 1000);
    },
    getDate: function (item) {
        return moment(item, "MM-DD-YYYY").valueOf();
    },
    getDateString: function (item) {
        return moment(item, "MM-DD-YYYY");
    },
    isMatchingJsonStr: function (obj1, obj2) {
        var map1 = {}, map2 = {};
        // get all properties in obj1 into a map
        getProps(obj1, map1);
        getProps(obj2, map2);

        for (var prop in map1) {
            if (prop in map2) {
                //
            } else {
                return false;
            }

        }
        return true;
    },
    // enCrypt test/string data using below Function
    encryptData: function (value) {
        try {
            var encrypted = crypto.AES.encrypt(value, config.security.secretkey);
            //console.log('encrypted Data:' + encrypted.toString());
            return encrypted.toString();
        }
        catch (error) {
            console.log('encryptData:error:', error);
            return null;
        }
    },

    // enCrypt test/string data using below Function
    decryptData: function (value) {
        try {
            var decrypted = crypto.AES.decrypt(value, config.security.secretkey).toString(crypto.enc.Utf8);
            //console.log('decrypted Data:' + decrypted);
            return decrypted;
        }
        catch (error) {
            console.log('decryptData:error:', error);
            return null;
        }
    },

    createHash: function (value) {
        const hashSalt = config.mongo.hashsalt;
        return bcrypt.hashSync(value, hashSalt);
    },

    compareHash: function (value, hashedValue) {
        return bcrypt.compareSync(value, hashedValue);
    },

    generatePassword: function () {
        var password = passwordGen.generate({
            length: config.security.randompassword.length || 15,
            numbers: config.security.randompassword.numbers || false
        });

        return password;
    },

    verifyMailServerConnection: function () {
        const smtpTransport = getSmtpTransport();

        let result = false;

        var deferred = Q.defer();

        smtpTransport.verify((err, response) => {
            result = (!err);

            if (result == false) {
                // console.log(err);
                deferred.reject(err.name + ': ' + err.message);
            }
            else {
                deferred.resolve(result);
                // console.log('verify:' + result);
            }

            smtpTransport.close();           
        });

        return deferred.promise;
    },

    sendMail: function (receiverEmail, subject, msgTxt, msgHtml) {
        const smtpTransport = getSmtpTransport();

        const mailOptions = {
            from: config.mail.senderemail,
            to: receiverEmail,
            subject: subject,
            text: msgTxt,
            html: msgHtml
        };

        smtpTransport.sendMail(mailOptions, function (error, info) {
            if (error) {
                return console.log(error);
                // throw error;
            }

            console.info('Message sent: %s', info.messageId);

            // Preview only available when sending through an Ethereal account
            console.info('Preview URL: %s', mailer.getTestMessageUrl(info));
            smtpTransport.close();
        });
    },
    AsObject: function (body) {
        var bodyobj = {};

        if (body && typeof body !== "object") {
            bodyobj = JSON.parse(body);
        } else {
            bodyobj = body;
        }

        return bodyobj;
    },
    GetKeyValue: function (obj, searchkey) {

        if (obj != null && obj != 'undefined' && typeof obj === 'object') {
            for (var i = 0; i < Object.keys(obj).length; i++) {
                var key = Object.keys(obj)[i];
                if (key.toLowerCase() == searchkey.toLowerCase()) {
                    return obj[key];
                }
            }
        }

        return null;
    },
    AsObjectId: function (id) {
        return new mongoose.Types.ObjectId((id.length < 12) ? "123456789012" : id);
    },

    IsArray: function(item) {
        if (item!=null && item!=='undefined' && isArray(item)) {
            return true;
        }
        else {
            return false;
        }
    }
};

function isArray(item) {
    return Object.prototype.toString.call(item) === "[object Array]";
}


function getProps(item, map) {
    if (typeof item === "object") {
        if (isArray(item)) {
            // iterate through all array elements
            for (var i = 0; i < item.length; i++) {
                getProps(item[i], map);
            }
        } else {
            for (var prop in item) {
                map[prop] = true;
                // recursively get any nested props
                // if this turns out to be an object or array
                getProps(item[prop], map);
            }
        }
    }
}

function getSmtpTransport() {
    // Use Smtp Protocol to send Email

    // var smtpTransport = mailer.createTransport(config.mail.protocol, {
    //     // service: config.mail.service,
    //     host: config.mail.server,
    //     secure: config.mail.secure,
    //     auth: {
    //         user: config.mail.auth.username,
    //         pass: config.mail.auth.password
    //     }
    // });

    const connectionInfo = 'smtp' + (config.mail.secure ? 's' : '') +
        '://' + config.mail.auth.username +
        ':' + config.mail.auth.password +
        '@' + config.mail.server +
        ':' + config.mail.port;

    // console.log('connection info:' + connectionInfo);
    return mailer.createTransport(connectionInfo);
}
