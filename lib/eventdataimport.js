var mongoose                = require('mongoose'),
    eventStagingModel       = require('../models/eventstagingmodel'),
    eventModel              = require('../models/eventmodel'),
    config                  = require('../config'),
    logger                  = require('./logger'),
    csv                     = require('fast-csv'),
    fs 			            = require('fs');;


module.exports.importFile = function(filepath, res) {
    
    var csvHeaders = {
        Events: {
                headers: ['EventIdentifier', 'EventStatus','EventStatusDate', 'EventLocationReference',
                'EventNote', 'EventResponseText', 'RelatedAuditId', 'EventUpdatedby', 'EventUpdatedDate',
                'UserIdentifier', 'EventTypeID']
            }
        };
        try{

       
        eventStagingModel.remove({}, function(){
            csv
            .fromPath(filepath, {headers: csvHeaders.Events.headers})
            .on('data', function(data) {
                try{
                var obj = new eventStagingModel();


                obj.EventIdentifier = data['EventIdentifier'];
                obj.GlobalProfileId = data['UserIdentifier'];
                obj.EventType = data['EventTypeID'];
                obj.EventBody.EventStatus = data['EventStatus'];
                obj.EventBody.EventLocationReference = data['EventLocationReference'];
                obj.EventBody.EventStatusDate = data['EventStatusDate'];
                obj.EventBody.EventNote = data['EventNote'];
                obj.EventBody.EventResponseText = data['EventResponseText'];
                obj.EventBody.RelatedAuditId = data['RelatedAuditId'];
                obj.UpdatedBy = data['EventUpdatedby'];
                obj.DateTime = data['EventUpdatedDate'];
                
                    obj.save();
                }
                catch(err){
                    logger.error(err.message)
                }
                
            })
            .on('end', function() {
                eventStagingModel.find({}, function(err, docs){
                    docs.forEach(function(doc) {
                        var event = new eventModel();
                        event.AppKey = "IIB_Import";
                        event.GlobalProfileId = doc.GlobalProfileId ;
                        event.EventType = doc.EventType ;
                        event.EventBody = doc.EventBody;
                        event.DateTime = new Date(doc.DateTime).getTime();
                        event.UpdatedBy = doc.UpdatedBy;
                        event.EventIdentifier = doc.EventIdentifier;
                        //*** Es6 assign
                        var eventToUpdate = {};
                        eventToUpdate = Object.assign(eventToUpdate, event._doc);
                        delete eventToUpdate._id;
                        //*** execute upsert
                        try{
                            eventModel.findOneAndUpdate({"EventIdentifier": event.EventIdentifier}, eventToUpdate, {upsert: true});
                        }
                        catch(err){
                            logger.error(err.message); 
                        }
                    });
                    var source = fs.createReadStream(filepath);
                    var dest = fs.createWriteStream(config.processedfilename + new Date().getTime() + '.csv');
                    
                    source.pipe(dest);
                    source.on('end', function() { fs.unlinkSync(filepath); });
                    source.on('error', function(err) { logger.error(err.message); });
                    res.status(200).json({success: "true", message: "File data has beem imported succesfully"});
                });                

            });    
        });
        }
        catch(err){
            logger.error(err.message);
        }
    
}