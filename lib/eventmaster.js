var core = require('./core'),
	logger = require('./logger'),
 	mongoose 		= require('mongoose'),
	config = require('../config');


var modEventMaster = require('../models/eventmastermodel');
var errorstring = '';

function tracker(EventMaster) {
	var trackerCore = core(sendPayload);
	
	trackerCore.setEventName(EventMaster.EventName);
	trackerCore.setEventDesc(EventMaster.EventDesc);
	trackerCore.setEventStr(EventMaster.EventStr);
	trackerCore.setUpdatedBy(EventMaster.UpdatedBy);
	trackerCore.setUpdatedts(EventMaster.Updatedts);

	/**
	 * Send the payload for an event to MongoDB
	 *
	 * @param object payload Dictionary of name-value pairs for the querystring
	 */
	function sendPayload(payload) {
		var builtPayload = payload.build();
		var EventMaster = new modEventMaster();
		/* istanbul ignore else */
		if(builtPayload){
			EventMaster.EventName = builtPayload.EventName; 
			EventMaster.EventDesc = builtPayload.EventDesc;
			EventMaster.EventStr = builtPayload.EventStr;
			EventMaster.UpdatedBy =  builtPayload.UpdatedBy;
			EventMaster.Updatedts=  builtPayload.Updatedts;
			
			//*** Es6 assign
			var eventMasterToUpdate = {};
			eventMasterToUpdate = Object.assign(eventMasterToUpdate, EventMaster._doc);
			delete eventMasterToUpdate._id;
  			//*** execute upsert
			modEventMaster.findOneAndUpdate({"EventName": EventMaster.EventName}, eventMasterToUpdate, {upsert: true} , function(err){
						/* istanbul ignore next */
						if (err){
							logger.error(err.message); 
							throw new Error(err.message);
						}
						else{
							logger.info("EventMaster data Saved");
						}
			});

		}
		else{
			logger.error("PayLoad initialization failed");
		}
	}

	return trackerCore;
}

module.exports = tracker;
