var payload = require('./payload.js');

/**
 * Create a tracker core object
 *
 * @param callback function Function applied to every payload dictionary object
 * @return object Tracker core
 */
function trackerCore(callback) {
	// Dictionary of key-value pairs which get added to every payload, e.g. tracker version
	var payloadPairs = {};
	/**
	 * Set a persistent key-value pair to be added to every payload
	 *
	 * @param string key Field name
	 * @param string value Field value
	 */
	function addPayloadPair(key, value) {
		if(value)
			payloadPairs[key] = value;
	}

    var sb = payload.payloadBuilder();
	/**
	 * Adds context and payloadPairs name-value pairs to the payload
	 * Applies the callback to the built payload 
	 *
	 * @return object Payload after the callback is applied
	 */
	function track() {
        
		sb.addDict(payloadPairs);
		sb.add('dtm', new Date().getTime());
		/* istanbul ignore else */		
		callback(sb);
		return sb;
	}
	return {
		
		addPayloadPair: addPayloadPair,
		track : track,
	
		/**
		 * Set the application ID
		 *
		 * @param string appId
		 */
		setAppKey: function (AppKey) {
			addPayloadPair('AppKey', AppKey);
		},
		/**
		 * Set the user ID
		 *
		 * @param string userId
		 */
		setGlobalProfileId: function (GlobalProfileId) {
				addPayloadPair('GlobalProfileId', GlobalProfileId);
		},
		/**
		 * Set the IP address
		 *
		 * @param string appId
		 */
		setClientIpAddress: function (ip) {
			addPayloadPair('ClientIP', ip);
		},

        /**
		 * Set the event source
		 *
		 * @param string soruce
		 */
		setSource: function (source) {
			/* istanbul ignore else */
			    sb.addJson('Source', source);			
		},
        
        /**
		 * Set the App event type
		 *
		 * @param string appeventtype
		 */
		setAppEventType: function (appeventtype) {
				addPayloadPair('AppEventType', appeventtype);
		},
        /**
		 * Set the event type
		 *
		 * @param string eventtype
		 */
		setEventType: function (eventtype) {
				addPayloadPair('EventType', eventtype);
		},
		setEventBody: function (eventbody) {
			    sb.addJson('EventBody',eventbody);
		},
		setDateTime: function (datetime) {
			   addPayloadPair('DateTime',datetime);
		},

		setEventName: function (eventname) {
			   addPayloadPair('EventName',eventname);
		},
		setEventDesc: function (eventdesc) {
			   addPayloadPair('EventDesc',eventdesc);
		},
		setEventStr : function (eventstr) {
			   sb.addJson('EventStr',eventstr);
		},
		setUpdatedBy : function (updatedby) {
			   addPayloadPair('UpdatedBy',updatedby);
		},
		setUpdatedts : function (updatedts) {
			   addPayloadPair('Updatedts',updatedts);
		}		
        					
	};
};

module.exports = trackerCore;
