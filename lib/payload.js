(function () {

	var
		json2 = require('JSON'),

		object = typeof exports !== 'undefined' ? /* istanbul ignore next */exports : /* istanbul ignore next */this;

	/*
	 * Is property a JSON?
	 */
	object.isJson = function (property) {
		return (typeof property !== 'undefined' && property !== null &&
			(property.constructor === {}.constructor ||/* istanbul ignore next */ property.constructor === [].constructor));
	};

	/*
	 * Is property a non-empty JSON?
	 */
	object.isNonEmptyJson = function (property) {
		/* istanbul ignore else */
		if (!object.isJson(property)) {// NOSONAR
			return false;
		}
		for (var key in property) {
			/* istanbul ignore else */
			if (property.hasOwnProperty(key)) { // NOSONAR
				return true;
			}
		}
		return false;
	}

	/**
	 * A helper to build a request string from an
	 * an optional initial value plus a set of individual
	 * name-value pairs, provided using the add method.
	 *
	 * @param boolean base64Encode Whether or not JSONs should be
	 * Base64-URL-safe-encoded
	 *
	 * @return object The request string builder, with add, addRaw and build methods
	 */
	object.payloadBuilder = function () {
		var dict = {};

		var add = function (key, value) {
			/* istanbul ignore else */
			if (value !== undefined && value !== null && value !== '') { // NOSONAR
				dict[key] = value;
			}
		};

		var addDict = function (dict) {
			for (var key in dict) {
				/* istanbul ignore else */
				if (dict.hasOwnProperty(key)) {// NOSONAR
					add(key, dict[key]);
				}
			}
		};

		var addJson = function (key, json) {
			/* istanbul ignore else */
			var flag = object.isNonEmptyJson(json);
			if (flag) {// NOSONAR
				var str = json2.stringify(json);
				add(key, str);
			}
		};

		return {
			add: add,
			addDict: addDict,
			addJson: addJson,
			build: function () {
				return dict;
			}
		};
	};

}());
