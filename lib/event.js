var core = require('./core'),
	logger = require('./logger'),
 	mongoose 		= require('mongoose'),
	config = require('../config');


var modEvents = require('../models/eventmodel');
var errorstring = '';

function tracker(Event) {
	var trackerCore = core(sendPayload);
	
	trackerCore.setAppKey(Event.AppKey);
	trackerCore.setSource(Event.Source);
	trackerCore.setClientIpAddress(Event.IP);
	trackerCore.setGlobalProfileId(Event.GlobalProfileId);
	trackerCore.setEventType(Event.EventType);
	trackerCore.setAppEventType(Event.AppEventType);	
	trackerCore.setEventBody(Event.EventBody);
	trackerCore.setDateTime(Event.DateTime);
	trackerCore.setUpdatedBy(Event.UpdatedBy);

	/**
	 * Send the payload for an event to MongoDB
	 *
	 * @param object payload Dictionary of name-value pairs for the querystring
	 */
	function sendPayload(payload) {
		var builtPayload = payload.build();
		var Events = new modEvents();
		/* istanbul ignore else */
		if(builtPayload){
			Events.AppKey = builtPayload.AppKey ; 
			Events.Source = builtPayload.Source;
			Events.IP = builtPayload.ClientIP;
			Events.GlobalProfileId =  builtPayload.GlobalProfileId ;
			Events.EventType=  builtPayload.EventType ;
			Events.AppEventType = builtPayload.AppEventType ;
			Events.EventBody = builtPayload.EventBody ;
			Events.DateTime = builtPayload.DateTime;
			Events.UpdatedBy = builtPayload.UpdatedBy ;
			Events.EventIdentifier = "";

			Events.save(function (err) {
				/* istanbul ignore next */
				if (err){
					logger.error(err.message); 
				}
				else{
					logger.info("Event data Saved");
				}
			});
			
		}
		else{
			logger.error("PayLoad initialization failed");
		}
	}

	return trackerCore;
}

module.exports = tracker;
