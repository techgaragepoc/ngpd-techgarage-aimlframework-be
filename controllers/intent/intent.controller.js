var express = require('express');
var intentService = require('../../services/intent/intent.service');
var intentUtteranceService = require('../../services/intent/intent.utterance.service');

module.exports = {

    getAll: function (req, res) {
        intentService.getAll()
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getByModel: function (req, res) {
        // console.log("Inside BE controller", req.query.intentData);
        // console.log("Request Param", req.query.modelName);
        intentService.getByModel(decodeURIComponent(req.query.modelName))
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    createIntent: function (req, res) {
        intentService.createIntent(req.body)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    deleteIntent: function (req, res) {
        intentService.deleteIntent(req.params._id)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    addIntentUtterance: function (req, res) {
        // console.log(req.body);
        // var intent = JSON.parse(req.body);
        // console.log(intent);

        intentUtteranceService.addIntentUtterance(req.body)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getIntentUtterances: function (req, res) {
        // console.log("Inside Get Utterance", req.query.categoryName);
        intentUtteranceService.getIntentUtterances(decodeURIComponent(req.query.categoryName), decodeURIComponent(req.query.modelName))
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getIntentUtteranceById: function (req, res) {
        // console.log("Inside Get Utterance", req.params._id);
        intentUtteranceService.getIntentUtteranceById(decodeURIComponent(req.params._id))
            .then(function (utterance) {
                res.send(utterance);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    deleteIntentUtterance: function (req, res) {
        intentUtteranceService.deleteIntentUtterance(req.params._id, decodeURIComponent(req.params._modelName))
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getIntents: function (req, res) {
        // console.log("Inside BE controller", req.query.intentData);
        intentUtteranceService.getIntents(req.query.intentData, req.query.modelName)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getIntentsByUser: function (req, res) {
        // console.log("Inside BE controller", req.query.intentData);
        intentUtteranceService.getIntentsByUser(
            // decodeURIComponent(req.query.userName),
            // decodeURIComponent(req.query.modelName),
            // decodeURIComponent(req.query.intentData)
            req.query.userName, req.query.modelName, req.query.intentData
        )
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getTree: function (req, res) {
        // console.log("Inside BE controller", req.query.intentData);
        intentUtteranceService.getTreeByUser(
            decodeURIComponent(req.query.intentData),
            decodeURIComponent(req.query.modelName)
        )
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getTreeByUser: function (req, res) {
        intentUtteranceService.getTreeByUser(
            // decodeURIComponent(req.query.userName),
            // decodeURIComponent(req.query.modelName),
            // decodeURIComponent(req.query.intentData)
            req.query.userName, req.query.modelName, req.query.intentData
        )
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    createEntity: function (req, res) {
        // console.log("Inside BE controller", req.query.entityData);
        intentUtteranceService.createEntity(decodeURIComponent(req.body.modelName), req.body.entityData)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    trainModel: function (req, res) {
        // console.log("Inside BE controller", req.query.trainModelString);
        intentUtteranceService.trainModel(
            decodeURIComponent(req.query.trainModelString),
            decodeURIComponent(req.query.modelName)
        )
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    trainEntireModel: function (req, res) {
        // console.log(decodeURIComponent(req.query.modelName || req.body.modelName));
        // console.log(decodeURIComponent(req.query.userName || req.body.userName));

        intentUtteranceService.trainEntireModel
            (
            decodeURIComponent(req.query.modelName || req.body.modelName),
            decodeURIComponent(req.query.userName || req.body.userName)
            )
            .then(function (data) {
                res.send(data);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getUtterancesByModel: function (req, res) {
        intentUtteranceService.getUtterancesByModel(decodeURIComponent(req.params._modelName || req.body.modelName))
            .then(function (data) {
                res.send(data);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    updateUtteranceScores: function (req, res) {
        intentUtteranceService.updateUtteranceScores(decodeURIComponent(req.body.modelName), req.body.utterances)
            .then(function (data) {
                res.send(data);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    updateUtterance: function (req, res) {
        intentUtteranceService.updateUtterance(decodeURIComponent(req.body.modelName), req.body.info)
            .then(function (data) {
                res.send(data);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    }
}
