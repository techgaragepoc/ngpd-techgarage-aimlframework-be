var express = require('express');
var commonService = require('../../services/common/common.service');

module.exports = {

    getAllCultures: function (req, res) {
        commonService.getAllCultures()
            .then(function (cultures) {
                res.send(cultures);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getCultureByCode: function (req, res) {
        commonService.getCultureByCode(req.params.code)
            .then(function (culture) {
                res.send(culture);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    }
}