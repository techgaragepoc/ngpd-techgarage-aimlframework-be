const intentUtteranceService = require("../../services/intent/intent.utterance.service");
const Rx = require("rxjs");

const processUserQuery = (req, res) => {
  const model_name = req.body.model_name;
  const user_id = req.body.user_id;
  const user_query = req.body.user_query;
  intentUtteranceService
    .getNlpResponse(user_id, model_name, user_query)
    .map(result => {
      return getHighScoreIntent(result);
    })
    .subscribe(
      result => {
        console.log('Highest scoring intent:', result);
        res.status(200).send('Highest scoring intent:' +  result);
      },
      error => {
        console.log(error);
        res.status(500).send(error.message);
      }
    );
};

const getHighScoreIntent = intentTree => {
  let intent_array = JSON.parse(intentTree).message;
  //console.log("intentTree", JSON.parse(intent_array).Intents[0]);

  if (intent_array && JSON.parse(intent_array).Intents) {
    const intent_data = JSON.parse(intent_array).Intents[0];
    var maxProp = null;
    var maxValue = -1;
    for (var prop in intent_data) {
      if (intent_data.hasOwnProperty(prop)) {
        var value = intent_data[prop];
        if (value > maxValue) {
          maxProp = prop;
          maxValue = value;
        }
      }
    }
    return maxProp;
  } else {
    return new Error("No intent found");
  }
};

module.exports = {
  processUserQuery
};
