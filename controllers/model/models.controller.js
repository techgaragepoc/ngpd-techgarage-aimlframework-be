var express = require('express');
var modelService = require('../../services/model/model.service');
var utility = require('../../lib/utility');

module.exports = {
    getAll: function (req, res) {
        modelService.getAll()
            .then(function (models) {
                res.json({ success: true, data: models });
                //res.send(models);
            })
            .catch(function (err) {
                res.status(400).json({ success: false, message: err });
            });
    },

    getById: function (req, res) {
        modelService.getById(req.params._id)
            .then(function (model) {
                if (model) {
                    //res.send(model);
                    res.json({ success: true, data: model });
                }
                else {
                    res.sendStatus(404);
                }
            })
            .catch(function (err) {
                res.status(400).json({ success: false, message: err });
                //res.status(400).send(err);
            });
    },

    update: function (req, res) {
        modelService.update(req.params._id, req.body)
            .then(function () {
                //res.json('success');
                res.json({ 'success': 'true' });
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },

    _delete: function (req, res) {
        modelService._delete(req.params._id)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },

    create: function (req, res) {

    }
}
