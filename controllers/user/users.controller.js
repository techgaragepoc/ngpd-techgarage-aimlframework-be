var express = require('express');
var userService = require('../../services/user/user.service');
var utility = require('../../lib/utility');
var router = express.Router();

// routes
router.post('/authenticate', authenticate);
router.post('/register', register);
router.get('/', getAll);
router.get('/current', getCurrent);
router.put('/:_id', update);
router.delete('/:_id', _delete);
router.put('/changepassword/:_id', changePassword);
router.post('/resetpassword', resetPassword);

module.exports = router;

function authenticate(req, res) {
    var username = req.body.username;
    var password = req.body.password;

    //userService.authenticate(req.body.username, req.body.password)
    userService.authenticate(username, utility.decryptData(password))
        .then(function (output) {
            res.status((output && output.success && output.result) ? 200 : 400).send(output);
            // res.status(output ? 200 : 400).send(output);
        })
        .catch(function (err) {
            res.status(400).json(err);
        });
}

function register(req, res) {
    userService.create(req.body)
        .then(function () {
            res.json({ success: true });
        })
        .catch(function (err) {
            res.status(400).json({ success: false, message: err });
            //res.status(400).send(err);
        });
}

function getAll(req, res) {
    userService.getAll()
        .then(function (users) {
            res.json({ success: true, data: users });
            //res.send(users);
        })
        .catch(function (err) {
            res.status(400).json({ success: false, message: err });
            //res.status(400).send(err);
        });
}

function getCurrent(req, res) {
    userService.getById(req.user.sub)
        .then(function (user) {
            if (user) {
                //res.send(user);
                res.json({ success: true, data: user });
            }
            else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).json({ success: false, message: err });
            //res.status(400).send(err);
        });
}

function update(req, res) {
    userService.update(req.params._id, req.body)
        .then(function () {
            //res.json('success');
            res.json({ 'success': 'true' });
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function _delete(req, res) {
    userService.delete(req.params._id)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function changePassword(req, res) {
    var body = req.body;

    if (body) {
        body.currpwd = utility.decryptData(body.currpwd);
        body.newpwd = utility.decryptData(body.newpwd);
    }

    // console.log('controller: ' + JSON.stringify(body));
    userService.changePassword(req.params._id, body)
        .then(function () {
            //res.json('success');
            // console.log('password changed');
            res.status(200).send({ 'success': 'true' });
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function resetPassword(req, res) {
    // console.log('controller: ' + JSON.stringify(body));
    userService.resetPassword(req.body)
        .then(function () {
            //res.json('success');
            res.status(200).send({ 'success': 'true' });
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
