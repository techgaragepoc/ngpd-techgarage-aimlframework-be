const { BaseRestController } = require('../baserestcontroller');
const { DomainModelService, DomainModelGraphService, 
            NodeDatasetMappingService } = require("../../services/intentfulfillment/");
const Utility = require("../../lib/utility");

class DomainModelController extends BaseRestController {

    /**
     * To get all the domain model graphs
     * @param {*} req 
     * @param {*} res 
     */
    GetAll(req, res) {
        DomainModelGraphService.GetAll().then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the domain model graph by providing Model Graph Id
     * @param {*} req 
     * @param {*} res 
     */
    Get(req, res) {
        var graphid = req.params.graphid;

        DomainModelGraphService.Get(graphid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the domain model graph by providing Model Graph Name
     * @param {*} req 
     * @param {*} res 
     */
    GetByName(req, res) {
        var graphname = req.params.graphname;

        DomainModelGraphService.GetByName(graphname).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get all the transformed domain model of domain model graphs
     * @param {*} req 
     * @param {*} res 
     */
    GetAllTransformed(req, res) {
        DomainModelService.GetAll().then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the transformed domain model by providing Domain Model Id
     * @param {*} req 
     * @param {*} res 
     */
    GetTransformed(req, res) {
        var transformmodelid = req.params.transformmodelid;

        DomainModelService.Get(transformmodelid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the transformed domain model providing Domain Model Name
     * @param {*} req 
     * @param {*} res 
     */
    GetTransformedByName(req, res) {
        var modelname = req.params.modelname;

        DomainModelService.GetByName(modelname).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To create the domain model graph (and its tranformation form)
     * @param {*} req 
     * @param {*} res 
     */
    Create(req, res) {
        var domainmodelgraph = Utility.AsObject(req.body);
        console.log('Domain model =>', domainmodelgraph);
        DomainModelGraphService.Create(domainmodelgraph).then(
            graphdata => {
                DomainModelService.TransformedJSON(graphdata).then(
                    transformjson => {
                        var domainmodel = {
                            modelgraphid: graphdata._id,
                            name: graphdata.name,
                            nodes: transformjson.nodes,
                            nodeproperties: transformjson.nodeproperties,
                            createdby: graphdata.createdby,
                        };

                        DomainModelService.Create(domainmodel).then(
                            data => this.responseWithResult(res, 200, false, graphdata)
                            , err => this.responseWithResult(res, 500, true, err));
                    },
                    transformerr => {
                        this.responseWithResult(res, 500, true, transformerr);
                    }); // DomainModelService.TransformedJSON
            },
            grapherr => {
                this.responseWithResult(res, 500, true, grapherr)
            }); // DomainModelGraphService.Create
    }

    /**
     * To update the domain model graph (and its tranformation form)
     * @param {*} req 
     * @param {*} res 
     */
    Update(req, res) {
        var domainmodelgraph = Utility.AsObject(req.body);

        DomainModelGraphService.Update(domainmodelgraph).then(
            graphdata => {
                DomainModelGraphService.Get(domainmodelgraph._id).then(
                    storedgraphdata => {
                        DomainModelService.TransformedJSON(storedgraphdata).then(
                            transformjson => {
                                var domainmodel = {
                                    modelgraphid: storedgraphdata._id,
                                    name: storedgraphdata.name,
                                    nodes: transformjson.nodes,
                                    nodeproperties: transformjson.nodeproperties,
                                    lastmodifiedby: storedgraphdata.lastmodifiedby,
                                    lastmodifiedon: Date.now()
                                };

                                DomainModelService.Update(domainmodel).then(
                                    data => this.responseWithResult(res, 200, false, storedgraphdata)
                                    , err => this.responseWithResult(res, 500, true, err));
                            },
                            transformerr => {
                                this.responseWithResult(res, 500, true, transformerr);
                            }); // DomainModelService.TransformedJSON
                    },
                    getgraphdataerr => {
                        this.responseWithResult(res, 500, true, getgraphdataerr);
                    });
            },
            grapherr => {
                this.responseWithResult(res, 500, true, grapherr)
            }); // DomainModelGraphService.Update
    }

    /**
     * To delete the domain model graph (and its tranformation form)
     * @param {*} req 
     * @param {*} res 
     */
    Delete(req, res) {
        var graphid = req.params.graphid;

        DomainModelGraphService.Delete(graphid).then(
            graphdata => {
                DomainModelService.GetByGraphId(graphid).then(
                    transformdata => {
                        DomainModelService.Delete(transformdata._id).then(
                            data => this.responseWithResult(res, 200, false, data)
                            , err => this.responseWithResult(res, 500, true, err));
                    },
                    trandformerr => {
                        this.responseWithResult(res, 500, true, trandformerr)
                    }); // DomainModelService.TransformedJSON
            },
            grapherr => {
                this.responseWithResult(res, 500, true, grapherr)
            }); // DomainModelGraphService.Delete
    }

    /**
     * To get all the node dataset mappings
     * @param {*} req 
     * @param {*} res 
     */
    GetAllNodeDatasetMappings(req, res) {

        NodeDatasetMappingService.GetAll().then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the Node Dataset Mapping by dataset id
     * @param {*} req 
     * @param {*} res 
     */
    GetNodeDatasetMappingByDatasetId(req, res) {
        var datasetid = req.params.datasetid;

        NodeDatasetMappingService.GetByDatasetId(datasetid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    GetNodeDatasetMappingByModelNode(req, res) {
        var modelgraphid = req.params.modelgraphid;
        var nodeid = req.params.nodeid;

        NodeDatasetMappingService.GetByDomainModelGraphId(modelgraphid).then(
            data => { 
                var mappings = [];
                if (data!=null && data!='undefined' && data.length>0) {  
                    for(var i=0;i<data.length;i++) {
                        if (data[i].nodeid == nodeid) {
                            mappings.push(data[i]);
                        }
                    }
                }
                this.responseWithResult(res, 200, false, mappings) 
            }
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     * To add node dataset mapping
     * @param {*} req 
     * @param {*} res 
     */
    AddNodeDatasetMapping(req, res) {
        var nodeDatasetMapping = Utility.AsObject(req.body);

        NodeDatasetMappingService.Add(nodeDatasetMapping).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));

    }

    /**
     * To update node dataset mapping
     * @param {*} req 
     * @param {*} res 
     */
    UpdateNodeDatasetMapping(req, res) {
        var nodeDatasetMapping = Utility.AsObject(req.body);

        NodeDatasetMappingService.Update(nodeDatasetMapping).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));

    }

    /**
     * To delete node dataset mapping
     * @param {*} req 
     * @param {*} res 
     */
    DeleteNodeDatasetMapping(req, res) {
        var mappingid = req.params.mappingid;

        NodeDatasetMappingService.Delete(mappingid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

}

module.exports = new DomainModelController();
