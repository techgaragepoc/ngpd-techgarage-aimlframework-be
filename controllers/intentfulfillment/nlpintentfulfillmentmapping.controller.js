const { BaseRestController } = require('../baserestcontroller');
const { NLPIntentFulfillmentMappingService } = require("../../services/intentfulfillment/");
const Utility = require("../../lib/utility");

class NLPIntentFulfillmentMappingController extends BaseRestController {

    /**
     * To get all the nlp-intent fulfillment mapping data
     * @param {*} req 
     * @param {*} res 
     */
    GetAll(req, res) {
        NLPIntentFulfillmentMappingService.GetAll().then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the nlp-intent fulfillment mapping data by providing mapping id
     * @param {*} req 
     * @param {*} res 
     */
    Get(req, res) {
        var mappingid = req.params.mappingid;
        NLPIntentFulfillmentMappingService.Get(mappingid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the nlp-intent fulfillment mapping data providing intentffmodel graphid
     * @param {*} req 
     * @param {*} res 
     */
    GetByIntentFulfillmentModelGraphId(req, res) {
        var intentffmodelid = req.params.intentffmodelid;
        NLPIntentFulfillmentMappingService.GetByIntentFulfillmentModelGraphId(intentffmodelid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the nlp-intent fulfillment mapping data providing intentffmodelid
     * @param {*} req 
     * @param {*} res 
     */
    GetByNLPModelAndIntent(req, res) {
        var nlpdetail = {
            model: req.params.nlpmodelid,
            intent: req.params.nlpintentid
        };

        NLPIntentFulfillmentMappingService.GetByNLP(nlpdetail).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }
    
    /**
     *  To create the nlp-intent fulfillment mapping data
     * @param {*} req 
     * @param {*} res 
     */
    Add(req, res) {
        var mappingdata = Utility.AsObject(req.body);
        NLPIntentFulfillmentMappingService.Add(mappingdata).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     * To update the nlp-intent fulfillment mapping data
     * @param {*} req 
     * @param {*} res 
     */
    Update(req, res) {
        var mappingdata = Utility.AsObject(req.body);
        NLPIntentFulfillmentMappingService.Update(mappingdata).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     * To update the nlp-intent fulfillment implementation status
     * @param {*} req 
     * @param {*} res 
     */
    UpdateImplementationStatus(req, res) {
        var mappingdata = Utility.AsObject(req.body);
        NLPIntentFulfillmentMappingService.UpdateImplementationStatus(mappingdata).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     * To delete the nlp-intent fulfillment mapping data based on provided mapping id
     * @param {*} req 
     * @param {*} res 
     */
    Delete(req, res) {
        var mappingid = req.params.mappingid;
        NLPIntentFulfillmentMappingService.Delete(mappingid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }
}

module.exports = new NLPIntentFulfillmentMappingController();
