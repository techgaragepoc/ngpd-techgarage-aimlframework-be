const { BaseRestController } = require('../baserestcontroller');
const { IntentFulfillmentModelService,
    IntentFulfillmentModelGraphService, NLPIntentFulfillmentMappingService } = require("../../services/intentfulfillment");

const ProjectService = require('../../services/project/project.service');
const Utility = require("../../lib/utility");

class IntentFulfillmentModelController extends BaseRestController {

    /**
     * To get all the intent model graphs
     * @param {*} req 
     * @param {*} res 
     */
    GetAll(req, res) {
        IntentFulfillmentModelGraphService.GetAll().then(
            data => { this.responseWithResult(res, 200, false, data) }
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the intent model graph by providing Model Graph Id
     * @param {*} req 
     * @param {*} res 
     */
    Get(req, res) {
        var graphid = req.params.graphid;

        IntentFulfillmentModelGraphService.Get(graphid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the intent model graph by providing Model Graph Name
     * @param {*} req 
     * @param {*} res 
     */
    GetByName(req, res) {
        var graphname = req.params.graphname;

        IntentFulfillmentModelGraphService.GetByName(graphname).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get all the transformed intent model of intent model graphs
     * @param {*} req 
     * @param {*} res 
     */
    GetAllTransformed(req, res) {
        console.log('getall ctrl=>');
        IntentFulfillmentModelService.GetAll().then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the transformed intent model by providing Intent Model Id
     * @param {*} req 
     * @param {*} res 
     */
    GetTransformed(req, res) {
        var transformmodelid = req.params.transformmodelid;

        IntentFulfillmentModelService.Get(transformmodelid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     * To get the intent fulfillment model by graph id
     * @param {*} req 
     * @param {*} res 
     */
    GetTransformedByGraphId(req, res) {
        var graphid = req.params.graphid;

        IntentFulfillmentModelService.GetByGraphId(graphid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }


    /**
     *  To get the transformed intent model providing Intent Model Name
     * @param {*} req 
     * @param {*} res 
     */
    GetTransformedByName(req, res) {
        var modelname = req.params.modelname;

        IntentFulfillmentModelService.GetByName(modelname).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     * To update intentff model node properties
     * @param {*} req 
     * @param {*} res 
     */
    UpdateTransformedNodeProperties(req, res) {
        var intentfulfillmentmodel = Utility.AsObject(req.body);

        IntentFulfillmentModelService.UpdateNodeProperty(intentfulfillmentmodel).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To create the intent model graph (and its tranformation form)
     * @param {*} req 
     * @param {*} res 
     */
    Create(req, res) {
        var modelgraph = Utility.AsObject(req.body);
        console.log('body =>', modelgraph);
        var intentfulfillmentmodelgraph = modelgraph.intentffmodelgraph;
        var nlpmodelid = modelgraph.nlpmodelid;
        var nlpintentid = modelgraph.nlpintentid;

        IntentFulfillmentModelGraphService.Create(intentfulfillmentmodelgraph).then(
            graphdata => {
                var nlpMapping = {
                    nlpmodelid: nlpmodelid, nlpintentid: nlpintentid,
                    intentfulfillmentmodelgraphid: (graphdata._id).toString(),
                    createdby: intentfulfillmentmodelgraph.createdby,
                    lastmodifiedby: intentfulfillmentmodelgraph.createdby

                };

                NLPIntentFulfillmentMappingService.Add(nlpMapping).then(
                    mapped => {

                        IntentFulfillmentModelService.TransformedJSON(graphdata).then(
                            transformjson => {
                                var intentfulfillmentmodel = {
                                    modelgraphid: graphdata._id,
                                    name: graphdata.name,
                                    pos: graphdata.pos,
                                    details: transformjson.details,
                                    nodes: transformjson.nodes,
                                    nodeproperties: transformjson.nodeproperties,
                                    createdby: graphdata.createdby,
                                };

                                IntentFulfillmentModelService.Create(intentfulfillmentmodel).then(
                                    data => this.responseWithResult(res, 200, false, graphdata)
                                    , err => this.responseWithResult(res, 500, true, err));
                            },
                            transformerr => {
                                this.responseWithResult(res, 500, true, transformerr);
                            }); // IntentFulfillmentModelService.TransformedJSON
                    },
                    mapperr => {
                        this.responseWithResult(res, 500, true, mapperr);
                    }); // mapperr
            },
            grapherr => {
                this.responseWithResult(res, 500, true, grapherr)
            }); // IntentFulfillmentModelGraphService.Create
    }

    /**
     * To update the intent model graph (and its tranformation form)
     * @param {*} req 
     * @param {*} res 
     */
    Update(req, res) {
        var modelgraph = Utility.AsObject(req.body);
        var intentfulfillmentmodelgraph = modelgraph.intentffmodelgraph;
        var nlpmodelid = modelgraph.nlpmodelid;
        var nlpintentid = modelgraph.nlpintentid;

        IntentFulfillmentModelGraphService.Update(intentfulfillmentmodelgraph).then(
            graphdata => {
                var nlpMapping = {
                    nlpmodelid: nlpmodelid, nlpintentid: nlpintentid,
                    intentfulfillmentmodelgraphid: (intentfulfillmentmodelgraph._id).toString(),
                    createdby: intentfulfillmentmodelgraph.lastmodifiedby,
                    lastmodifiedby: intentfulfillmentmodelgraph.lastmodifiedby
                };

                NLPIntentFulfillmentMappingService.Add(nlpMapping).then(
                    mapped => {

                        IntentFulfillmentModelGraphService.Get(intentfulfillmentmodelgraph._id).then(
                            storedgraphdata => {
                                IntentFulfillmentModelService.TransformedJSON(storedgraphdata).then(
                                    transformjson => {
                                        var intentfulfillmentmodel = {
                                            modelgraphid: storedgraphdata._id,
                                            name: storedgraphdata.name,
                                            pos: storedgraphdata.pos,
                                            details: transformjson.details,
                                            nodes: transformjson.nodes,
                                            nodeproperties: transformjson.nodeproperties,
                                            lastmodifiedby: storedgraphdata.lastmodifiedby,
                                            lastmodifiedon: Date.now()
                                        };
                                        //console.log('intentfulfillmentmodel =>', intentfulfillmentmodel);
                                        IntentFulfillmentModelService.Update(intentfulfillmentmodel).then(
                                            data => this.responseWithResult(res, 200, false, storedgraphdata)
                                            , err => this.responseWithResult(res, 500, true, err));
                                    },
                                    transformerr => {
                                        this.responseWithResult(res, 500, true, transformerr);
                                    }); // IntentFulfillmentModelService.TransformedJSON
                            },
                            mapperr => {
                                this.responseWithResult(res, 500, true, mapperr);
                            }); // mapperr
                    },
                    getgraphdataerr => {
                        this.responseWithResult(res, 500, true, getgraphdataerr);
                    });
            },
            grapherr => {
                this.responseWithResult(res, 500, true, grapherr)
            }); // IntentFulfillmentModelGraphService.Update
    }

    /**
     * To delete the intent model graph (and its tranformation form)
     * @param {*} req 
     * @param {*} res 
     */
    Delete(req, res) {
        var graphid = req.params.graphid;

        IntentFulfillmentModelGraphService.Delete(graphid).then(
            graphdata => {
                IntentFulfillmentModelService.GetByGraphId(graphid).then(
                    transformdata => {
                        IntentFulfillmentModelService.Delete(transformdata._id).then(
                            data => this.responseWithResult(res, 200, false, data)
                            , err => this.responseWithResult(res, 500, true, err));
                    },
                    trandformerr => {
                        this.responseWithResult(res, 500, true, trandformerr)
                    }); // IntentFulfillmentModelService.TransformedJSON
            },
            grapherr => {
                this.responseWithResult(res, 500, true, grapherr)
            }); // IntentFulfillmentModelGraphService.Delete
    }

    GetProjectDetailById(req, res) {
        var projectid = req.params.projectid;

        ProjectService.getCategory().then(
            categories => {
                ProjectService.getById(projectid).then(
                    project => {
                        project["categoryname"] = '';
                        for (var i = 0; i < categories.length; i++) {
                            if (categories[i]._id.equals(project.category)) {
                                project["categoryname"] = categories[i].name;
                                break;
                            }
                        }
                        this.responseWithResult(res, 200, false, project)
                    },
                    prjerr => { this.responseWithResult(res, 500, true, prjerr) });  // getById
            },
            catgerr => { this.responseWithResult(res, 500, true, catgerr) }); // getCategory
    }


}

module.exports = new IntentFulfillmentModelController();
