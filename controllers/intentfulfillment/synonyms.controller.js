const { BaseRestController } = require('../baserestcontroller');
const { SynonymsService } = require("../../services/intentfulfillment/");
const Utility = require("../../lib/utility");

class SynonymsController extends BaseRestController {

    /**
     * To get all the synonym meta data
     * @param {*} req 
     * @param {*} res 
     */
    GetAll(req, res) {
        SynonymsService.GetAll().then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the synonym meta data by providing synonym id
     * @param {*} req 
     * @param {*} res 
     */
    Get(req, res) {
        var synonymid = req.params.synonymid;
        SynonymsService.Get(synonymid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the synonym meta data providing synonym refid
     * @param {*} req 
     * @param {*} res 
     */
    GetByRefId(req, res) {
        var synonymrefid = req.params.synonymrefid;
        SynonymsService.GetByRefId(synonymrefid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To create the synonym meta data
     * @param {*} req 
     * @param {*} res 
     */
    Create(req, res) {
        var synonymmetadata = Utility.AsObject(req.body);
        SynonymsService.Create(synonymmetadata).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     * To update the synonym meta data
     * @param {*} req 
     * @param {*} res 
     */
    Update(req, res) {
        var synonymmetadata = Utility.AsObject(req.body);
        SynonymsService.Update(synonymmetadata).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     * To delete the synonym meta data based on provided synonym id
     * @param {*} req 
     * @param {*} res 
     */
    Delete(req, res) {
        var synonymid = req.params.synonymid;
        SynonymsService.Delete(synonymid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }
}

module.exports = new SynonymsController();
