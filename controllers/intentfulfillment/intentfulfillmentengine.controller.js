const { BaseRestController } = require('../baserestcontroller');
const { DomainModelService, IntentFulfillmentModelService,
    NLPIntentFulfillmentMappingService, FunctionsService,
    SynonymsService, NodeDatasetMappingService, ExecutionLoggerService } = require("../../services/intentfulfillment/");
const Utility = require("../../lib/utility");
const config = require('../../config');

const IntentFulfillmentAPI = require('ngpd-techgarage-intentfulfillment-be');

class IntentFulfillmentEngineController extends BaseRestController {

    constructor() {
        super();
        this.MetaData = {
            "domainmodels": [],
            "intentfulfillmentmodels": [],
            "functions": [],
            "synonyms": [],
            "nlpintentfulfillmentmapping": []
        };

        this.Logger = ExecutionLoggerService;
    }

    /**
     * Initiate a session
     */
    InitiateSession() {
        var self = this;

        return new Promise(function (resolve, reject) {
            self.Logger.Initialize()
                .then(sessionId =>
                    resolve(sessionId)
                );
        });
    }

    GetSessionLogs(sessionid, level) {
        var self = this;

        if (level==null || level=='undefined' || isNaN(level)) {
            level = 0;
        }

        return new Promise(function (resolve, reject) {
            self.Logger.GetLog(sessionid, level)
                .then(result =>
                    resolve({sessionid: sessionid, result: result})
                );
        });
    }

    /**
     * To initialize the engine
     * @param {*} req 
     * @param {*} res 
     */
    Initialize(sessionId) {
        var self = this;

        return new Promise(function (resolve, reject) {
            DomainModelService.GetAll()
                .then(domainmodels => {
                    self.MetaData.domainmodels = domainmodels;
                    IntentFulfillmentModelService.GetAll()
                        .then(intentffmodel => {
                            self.MetaData.intentfulfillmentmodels = intentffmodel;
                            FunctionsService.GetAll()
                                .then(functions => {
                                    self.MetaData.functions = functions;
                                    SynonymsService.GetAll()
                                        .then(synonyms => {
                                            self.MetaData.synonyms = synonyms;

                                            NLPIntentFulfillmentMappingService.GetAll()
                                                .then(mapping => {
                                                    self.MetaData.nlpintentfulfillmentmapping = mapping;

                                                    NodeDatasetMappingService.GetAll()
                                                        .then(datasetmappings => {

                                                            IntentFulfillmentAPI.Initialize(self.MetaData.domainmodels, self.MetaData.intentfulfillmentmodels,
                                                                self.MetaData.functions, self.MetaData.synonyms, self.MetaData.nlpintentfulfillmentmapping,
                                                                datasetmappings, config.modules.adminbe.baseurl, self.Logger, sessionId)
                                                                .then(initialized => {
                                                                    resolve(initialized);
                                                                }); // IntentFulfillmentAPI.Initialize

                                                        }); // NodeDatasetMappingService

                                                }); // NLPIntentFulfillmentMappingService
                                        }); // SynonymsService
                                }); // FunctionsService
                        }); // IntentFulfillmentModelService
                }); // DomainModelService
        }); // Promise
    }

    /**
     * To identify mapped intentfulfillment model
     * @param {*} req 
     * @param {*} res 
     */
    IdentifyModel(nlpmodel, nlpintent, nlpPOS) {
        var self = this;
        return new Promise(function (resolve, reject) {
            IntentFulfillmentAPI.IdentifyModel(nlpmodel, nlpintent, nlpPOS)
                .then(intentffmodel => {
                    console.log('intentffmodel => ', JSON.stringify(intentffmodel));
                    resolve(intentffmodel);
                }); // IntentFulfillmentAPI.IdentifyModel
        }); // Promise
    }


    /**
     * To execute the mapped intentfulfillment model
     * @param {*} req 
     * @param {*} res 
     */
    Execute(sessionId, intentffmodel, nlpPOS) {
        var self = this;
        return new Promise(function (resolve, reject) {
            IntentFulfillmentAPI.ExecuteModel(intentffmodel, nlpPOS)
                .then(result => {
                   //  console.log('result => ', JSON.stringify(result));
                    self.Logger.Log(sessionId, {step: 'result model', message: JSON.stringify(result)});
                    self.Logger.Log(sessionId, {step: '$$result$$', message: JSON.stringify(result.resultrows)});
                    resolve(result);
                }); // IntentFulfillmentAPI.IdentifyAndExecuteModel
        });
    }

}

module.exports = new IntentFulfillmentEngineController();