const { BaseRestController } = require('../baserestcontroller');
const { FunctionsService } = require("../../services/intentfulfillment/");
const Utility = require("../../lib/utility");

class FunctionsController extends BaseRestController {

    /**
     * To get all the function meta data
     * @param {*} req 
     * @param {*} res 
     */
    GetAll(req, res) {
        FunctionsService.GetAll().then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the function meta data by providing function id
     * @param {*} req 
     * @param {*} res 
     */
    Get(req, res) {
        var functionid = req.params.functionid;
        FunctionsService.Get(functionid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To get the function meta data providing function Name
     * @param {*} req 
     * @param {*} res 
     */
    GetByName(req, res) {
        var functionname = req.params.functionname;
        FunctionsService.GetByName(functionname).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     *  To create the function meta data
     * @param {*} req 
     * @param {*} res 
     */
    Create(req, res) {
        var functionmetadata = Utility.AsObject(req.body);
        FunctionsService.Create(functionmetadata).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     * To update the function meta data
     * @param {*} req 
     * @param {*} res 
     */
    Update(req, res) {
        var functionmetadata = Utility.AsObject(req.body);
        FunctionsService.Update(functionmetadata).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }

    /**
     * To delete the function meta data based on provided function id
     * @param {*} req 
     * @param {*} res 
     */
    Delete(req, res) {
        var functionid = req.params.functionid;
        FunctionsService.Delete(functionid).then(
            data => this.responseWithResult(res, 200, false, data)
            , err => this.responseWithResult(res, 500, true, err));
    }
}

module.exports = new FunctionsController();
