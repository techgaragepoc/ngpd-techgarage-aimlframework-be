var DomainModelController = require('./domainmodel.controller');
var IntentFulfillmentModelController = require('./intentfulfillmentmodel.controller');
var FunctionsController = require('./functions.controller');
var SynonymsController = require('./synonyms.controller');
var NLPIntentFulfillmentMappingController = require('./nlpintentfulfillmentmapping.controller');

var IntentFulfillmentEngineController = require('./intentfulfillmentengine.controller');

module.exports = {
    DomainModelController,
    IntentFulfillmentModelController,
    FunctionsController,
    SynonymsController,
    NLPIntentFulfillmentMappingController,
    IntentFulfillmentEngineController
}