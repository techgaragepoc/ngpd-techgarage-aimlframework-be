const HttpError = require('./http-error');

class BaseRestController {
    constructor(options) {
        const opts = options || {};
        this.successCode = opts.successCode || 200;
        this.notFoundCode = opts.notFoundCode || 404;
        this.errorCode = opts.errorCode || 500;
    }

    checkIfEntityFound(msg) {
        return (entity) => {
            if (!entity) {
                throw new HttpError(this.notFoundCode, msg || 'Not found');
            }
            return entity;
        };
    }

    /*
    responseWithResult(res, statusCode) {
        return (entity) => {
            //console.log('response with result...', entity);
            //  To not respond with internal applications errors
            statusCode = typeof statusCode === 'string' && statusCode.startsWith(this.errorCode) ? this.errorCode : statusCode;
            res.status(statusCode || this.successCode).send(entity);
        };
    }
*/

    responseWithResult(res, statusCode, isError, entity) {
       // console.log('entity=>', entity); 
        var responseObject = {
                success: true,
                data: null,
                error: null
            };

        responseObject["success"] = !isError;
        if (isError) {
            responseObject["error"] = entity;
        }
        else {
            responseObject["data"] = entity;
        }

       // return () => {
            //  To not respond with internal applications errors
            statusCode = typeof statusCode === 'string' && statusCode.startsWith(this.errorCode) ? this.errorCode : statusCode;
            res.status(statusCode || this.successCode).send(responseObject);
       // };
    }


}

module.exports = BaseRestController;
