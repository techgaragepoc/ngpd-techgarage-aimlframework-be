const Q = require('q');
const Utility = require('../../lib/utility');

class ResponseBuilder {
    constructor() {

    }

    Build(session, userContext, result) {
        var deferred = Q.defer();

       setTimeout(function(){
        deferred.resolve({"response": getResponseObject(result), "sessionid": session._id, "user": userContext});
       }, 100);

        return deferred.promise;
    }

}

function getResponseObject(result) {
    var messagetype = 1;
    var messagestr = '';

if (result!=null && result!=='undefined') {
    if (result.constructor === Array) {
        messagestr =  "information is ..." + JSON.stringify(result);
    }
    else {
        messagestr =  "information is ..." + JSON.stringify(result);
    }
}   

return {"messagetype": messagetype, "message": messagestr};
}

module.exports = new ResponseBuilder();