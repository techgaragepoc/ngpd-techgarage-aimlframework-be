const { BaseRestController } = require('../baserestcontroller');
const Utility = require("../../lib/utility");
const ContextManger = require('./contextmanager');
const ResponseBuilder = require('./responsebuilder');
const Q = require('q');

class ConversationController extends BaseRestController {

    /**
     * To return some welcome text
     * @param {*} req 
     * @param {*} res 
     */
    ShowHelp(req, res) {
        const data = { "message": "Welcome to conversation service api - part of AI Framework" }
        this.responseWithResult(res, 200, false, data)
    }


    AuthenticateAndValidate(headerdata, messagedata, operation) {
        var deferred = Q.defer();

        //1. Extract header and body (user context, session context) and validate that all required params are here
        ContextManger.ValidateParameters(headerdata, messagedata, operation).then(
            clientrequest => {

                if (clientrequest && clientrequest.success) {

                    //2. Authenticate the client from header information
                    ContextManger.AuthenticateClient(clientrequest.params.applicationid, clientrequest.params.authtoken, clientrequest.params.projectid).then(
                        authresult => {

                            if (authresult) {
                                console.log('Client application authenticated successfully...');

                                //3. Verify the session, either already initiated or new to intiate
                                ContextManger.ValidateSession(clientrequest.params.applicationid, clientrequest.params.projectid, clientrequest.params.userContext).then(
                                    session => {
                                        console.log('Session validated successfully...');
                                        deferred.resolve({ "clientrequest": clientrequest, "session": session });
                                    },
                                    sessionerr => { deferred.reject(sessionerr); }
                                );
                            }
                            else {
                                //send the message - Unauthorised Client
                                var unauthmessage = { "message": "This application is not authorised to use the service" };
                                deferred.reject(unauthmessage);
                            }
                        },
                        autherr => {
                            console.log('Error occured while authenticating client app (unknown application) ...', autherr);
                            var unauthmessage = { "message": "This application is not authorised to use the service" };
                            deferred.reject(unauthmessage);
                        }
                    );
                }
                else {
                    var validationmessage = { "message": clientrequest.errmessages };
                    deferred.reject(validationmessage);
                }
            });

        return deferred.promise;
    }

    /**
     * To process the message
     * @param {*} req 
     * @param {*} res 
     */
    ProcessMessage(req, res) {
        var { headers, body } = req;
        var headerdata = Utility.AsObject(headers);
        var messagedata = Utility.AsObject(body);

        // console.log("header data => ", headerdata);

        this.AuthenticateAndValidate(headerdata, messagedata, 'POST').then(
            validresp => {
                console.log('Start Processing message...');
                var clientrequest = validresp.clientrequest;
                var session = validresp.session;
                //4. After successful authentication, process the message 
                ContextManger.ProcessMessage(session, clientrequest.params.userContext, clientrequest.params.message).then(
                    result => {
                        // 5. build the response and send it back
                        ResponseBuilder.Build(session, clientrequest.params.userContext, result).then(
                            msgresp => {
                                //update chat history 
                                ContextManger.UpdateChatHistory(session._id, clientrequest.params.message, msgresp.response).then(
                                    hist => { return; },
                                    histerr => { console.log('error occured while updating chat history...'); }
                                );

                                console.log('Response sent...');
                                this.responseWithResult(res, 200, false, msgresp);
                            },
                            msgresperr => { this.responseWithResult(res, 500, true, msgresperr); }
                        );
                    },
                    err => { this.responseWithResult(res, 500, true, err); }
                );
            },
            validresperr => { this.responseWithResult(res, 500, true, validresperr); }
        );
    }

    /**
     * To get the chat history
     * @param {*} req 
     * @param {*} res 
     */
    GetChatHistory(req, res) {
        var { headers, params } = req;
        var headerdata = Utility.AsObject(headers);
        var paramsdata = Utility.AsObject(params);
        var sessionid = paramsdata["sessionid"];

        this.AuthenticateAndValidate(headerdata, null, 'GET').then(
            validresp => {
                var clientrequest = validresp.clientrequest;
                var session = validresp.session;

                var result = {
                    applicationid: clientrequest.params.applicationid,
                    projectid: clientrequest.params.projectid,
                    user: clientrequest.params.userContext,
                    sessionid: '',
                    chathistory: []
                };

                //if no session id is provided then get the last session history
                if (!sessionid || sessionid === 'undefined' || (session && session._id == sessionid)) {
                    var hist = (session.chathistory && Utility.IsArray(session.chathistory)) ? session.chathistory : [];
                    result["sessionid"] = session._id;
                    result["chathistory"] = hist;
                    this.responseWithResult(res, 200, false, result);
                }
                else {
                    ContextManger.GetChatHistory(sessionid).then(
                        hist => {
                            result["sessionid"] = sessionid;
                            result["chathistory"] = hist;
                            this.responseWithResult(res, 200, false, result);
                        },
                        histerr => {
                            console.log('Error occured while accessing chat history...', histerr);
                            this.responseWithResult(res, 500, true, { "message": "Error occured while accessing chat history..." });
                        }
                    );
                }
            },
            validresperr => { this.responseWithResult(res, 500, true, validresperr); }
        );

    }


}


module.exports = new ConversationController();
