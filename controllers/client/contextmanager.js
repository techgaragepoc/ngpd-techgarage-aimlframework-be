const Q = require('q');
const Utility = require('../../lib/utility');
const { ClientApplicationService, ClientSessionService } = require('../../services/client');
const NLPService = require('../../services/nlp/nlp.service');
const { IntentImplementationService } = require('../../services/intentfulfillment');

class ContextManager {
    constructor() {

    }

    ValidateParameters(msgheader, msgbody, operation) {
        var deferred = Q.defer();
        var validationmsgs = [];
        var result = true;

        const hdrauthinfo = Utility.GetKeyValue(msgheader, 'authinfo');
        const authinfo = (hdrauthinfo != null && hdrauthinfo !== 'undefined') ? Utility.AsObject(hdrauthinfo) : {};

        const applicationid = Utility.GetKeyValue(authinfo, 'applicationid');
        const authtoken = Utility.GetKeyValue(authinfo, 'authtoken');
        const projectid = Utility.GetKeyValue(authinfo, 'projectid');
        const userContext = Utility.GetKeyValue(authinfo, 'user');
        var message = '';


        if (applicationid == null || applicationid === 'undefined') {
            validationmsgs.push('applicationid is not defined in header');
            result = false;
        }

        if (authtoken == null || authtoken === 'undefined') {
            validationmsgs.push('authtoken is not defined in header');
            result = false;
        }

        if (projectid == null || projectid === 'undefined') {
            validationmsgs.push('projectid is not defined in header');
            result = false;
        }

        if (userContext == null || userContext === 'undefined' || userContext.userid == null || userContext.userid === 'undefined') {
            validationmsgs.push('user context is not defined in body');
            result = false;
        }

        if (operation == 'POST') {
            message = Utility.GetKeyValue(msgbody, 'message');
            if (message == null || message === 'undefined') {
                validationmsgs.push('message is not defined in body');
                result = false;
            }
        }

        deferred.resolve({
            "success": result,
            "errmessages": validationmsgs,
            "params": {
                applicationid: applicationid,
                authtoken: authtoken,
                projectid: projectid,
                message: message,
                userContext: userContext,
            }
        });

        return deferred.promise;
    }

    AuthenticateClient(applicationid, authtoken, projectid) {
        return ClientApplicationService.Authenticate(applicationid, authtoken, projectid);
    }

    ValidateSession(applicationid, projectid, userContext) {
        var deferred = Q.defer();

        ClientSessionService.Validate(applicationid, projectid, userContext).then(
            session => {
                //valid session exists

                if (session) {
                    deferred.resolve(session);
                }
                else {
                    //initiate a new session
                    var clientsession = {
                        applicationid: applicationid,
                        projectid: projectid,
                        usercontext: userContext,
                        chathistory: [],
                        createdby: (userContext && userContext !== 'undefined'
                            && userContext.userid != null && userContext.userid !== 'undefined') ? userContext.userid : 'unknown',
                    };

                    ClientSessionService.Initiate(clientsession).then(
                        newsession => { deferred.resolve(newsession); },
                        initerr => { deferred.reject({ "message": "Error occured while initiating session", error: initerr }); }
                    )
                }
            },
            err => { deferred.reject({ "message": "Error occured while validating session", error: err }); }
        )

        return deferred.promise;
    }

    ProcessMessage(session, userContext, message) {
        var deferred = Q.defer();

        /*
        // identify the nlp model and category
        NLPService.GetNLPIntentInfo(message).then(
            nlp => {

                // Get the mapped model
                IntentImplementationService.GetMappedFulfillmentModel(nlp).then(
                    model => {

                        // Execute the mapped model
                        IntentImplementationService.ExecuteModel(model, nlp, userContext).then(
                            result => {

                            },
                            execerr => { this.responseWithResult(res, 200, true, execerr); }
                        );
                    },
                    modelerr => { this.responseWithResult(res, 200, true, modelerr); }
                );
            },
            nlperr => { this.responseWithResult(res, 200, true, nlperr); }
        );
        */

        setTimeout(function () {
            deferred.resolve({ "result": [{ "row": 0, "col1": "test#1" }, { "row": 1, "col1": "test#2" }] });
        }, 1000);

        return deferred.promise;
    }

    UpdateChatHistory(sessionid, message, response) {
        return ClientSessionService.UpdateChatHistory(sessionid, message, response);
    }

    GetChatHistory(sessionid) {
        var deferred = Q.defer();

        ClientSessionService.Get(sessionid).then(
            session => {
                if (session && session.chathistory) {
                    deferred.resolve(session.chathistory);
                } else {
                    deferred.resolve([]);
                }
            },
            err => { deferred.reject(err); }
        );
        return deferred.promise;
    }

}

module.exports = new ContextManager();