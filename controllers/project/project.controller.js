var express = require('express');
var projectService = require('../../services/project/project.service');
var connection = require('../../models/connection');

module.exports = {
    getAll: function (req, res) {
        projectService.getAll()
            .then(function (projs) {
                res.json({ success: true, data: projs });
                //res.send(users);
            })
            .catch(function (err) {
                res.status(400).json({ success: false, message: err });
                //res.status(400).send(err);
            });
    },
    getById: function (req, res) {
        projectService.getById(req.params._id)
            .then(function (proj) {
                if (proj) {
                    //res.send(user);
                    res.json({ success: true, data: proj });
                }
                else {
                    res.sendStatus(404);
                }
            })
            .catch(function (err) {
                res.status(400).json({ success: false, message: err });
                //res.status(400).send(err);
            });
    },
    getByUserId: function (req, res) {
        projectService.getByUserId(req.params._userId)
            .then(function (proj) {
                if (proj) {
                    //res.send(user);
                    res.json({ success: true, data: proj });
                }
                else {
                    res.sendStatus(404);
                }
            })
            .catch(function (err) {
                res.status(400).json({ success: false, message: err });
                //res.status(400).send(err);
            });
    },
    getByName: function (req, res) {
        projectService.getByName(req.params._name)
            .then(function (proj) {
                if (proj) {
                    //res.send(user);
                    res.json({ success: true, data: proj });
                }
                else {
                    res.sendStatus(404);
                }
            })
            .catch(function (err) {
                res.status(400).json({ success: false, message: err });
                //res.status(400).send(err);
            });
    },
    getProjectOptions: function (req, res) {
        projectService.getProjectOptions()
            .then(function (projoptions) {
                res.json({ success: true, data: projoptions });
                //res.send(users);
            })
            .catch(function (err) {
                res.status(400).json({ success: false, message: err });
                //res.status(400).send(err);
            });
    },
    create: function (req, res) {
        projectService.create(req.body)
            .then(function () {
                res.json({ success: true });
            })
            .catch(function (err) {
                res.status(400).json({ success: false, message: err });
                //res.status(400).send(err);
            });
    },
    getCategories: function (req, res) {
        projectService.getCategory()
            .then(function (cats) {
                res.json({ success: true, data: cats });
                //res.send(users);
            })
            .catch(function (err) {
                res.status(400).json({ success: false, message: err });
                //res.status(400).send(err);
            });
    },
    createCategory: function (req, res) {
        projectService.createCategory(req.body)
            .then(function (result) {
                res.json({ success: true, data: result });
            })
            .catch(function (err) {
                res.status(400).json({ success: false, message: err });
                //res.status(400).send(err);
            });
    },
    update: function (req, res) {
        projectService.update(req.params._id, req.body)
            .then(function () {
                //res.json('success');
                res.json({ 'success': 'true' });
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    _delete: function (req, res) {
        projectService.delete(req.params._id)
            .then(function () {
                res.json('success');
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    }
}
