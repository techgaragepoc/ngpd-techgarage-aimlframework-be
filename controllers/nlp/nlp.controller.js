var express = require('express');
var nlpService = require('../../services/nlp/nlp.service');
var connection = require('../../models/connection');

module.exports = {
    getAll: function (req, res) {
        nlpService.getAll()
            .then(function (nlpLanguageModel) {
                res.send(nlpLanguageModel);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getById: function (req, res) {
        nlpService.getById(req.params._id)
            .then(function (nlpLanguageModel) {
                res.send(nlpLanguageModel);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getByName: function (req, res) {
        nlpService.getByName(req.params._name)
            .then(function (nlpLanguageModel) {
                res.send(nlpLanguageModel);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    getByUserId: function (req, res) {
        nlpService.getByUserId(req.params._id)
            .then(function (nlpLanguageModel) {
                res.send(nlpLanguageModel);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    createNlpLanguageModel: function (req, res) {
        // console.log(req.body);
        nlpService.createNlpLanguageModel(req.body)
            .then(function (nlpLanguageModel) {
                res.send(nlpLanguageModel);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    createModel: function (req, res) {
        // console.log(req.body);

        nlpService.createModel(req.body)
            .then(function (nlpLanguageModel) {
                res.send(nlpLanguageModel);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    updateLanguageModel: function (req, res) {
        nlpService.updateLanguageModel(req.body)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    deleteLanguageModel: function (req, res) {
        nlpService.deleteLanguageModel(req.params._id)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    downloadLanguageModel: function (req, res) {
        nlpService.downloadLanguageModel(req.params._id)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    }
}
