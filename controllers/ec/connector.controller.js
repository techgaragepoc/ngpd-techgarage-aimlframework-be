const connectorService = require("../../services/ec/connector.service");
const commonMethods = require("../../lib/utility");

module.exports = {
  GetAll: function (req, res) {
    connectorService.GetAll(function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.Connector - GetAll method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Get: function (req, res) {
    var connectorid = req.params.connectorid;

    connectorService.Get(connectorid, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.Connector - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  GetByName: function (req, res) {
    var connectorname = req.params.connectorname;

    connectorService.GetByName(connectorname, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.connector - GetByName method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Create: function (req, res) {
    var connector = commonMethods.AsObject(req.body);

    connectorService.Create(connector, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.connector - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Update: function (req, res) {
    var connector = commonMethods.AsObject(req.body);

    connectorService.Update(connector, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.connector - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Delete: function (req, res) {
    var connectorid = req.params.connectorid;

    connectorService.Delete(connectorid, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.connector - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  }
};
