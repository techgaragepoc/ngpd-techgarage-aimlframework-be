const datasetService = require("../../services/ec/dataset.service");
const commonMethods = require("../../lib/utility");

module.exports = {
  GetAll: function (req, res) {
    datasetService.GetAll(function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.dataset - GetAll method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Get: function (req, res) {
    var datasetid = req.params.datasetid;

    datasetService.Get(datasetid, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.dataset - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  GetByName: function (req, res) {
    var datasetname = req.params.datasetname;

    datasetService.GetByName(datasetname, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.dataset - GetByName method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Create: function (req, res) {
    var dataset = commonMethods.AsObject(req.body);

    datasetService.Create(dataset, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.dataset - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Update: function (req, res) {
    var dataset = commonMethods.AsObject(req.body);

    datasetService.Update(dataset, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.dataset - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Delete: function (req, res) {
    var datasetid = req.params.datasetid;

    datasetService.Delete(datasetid, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.dataset - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  }
};
