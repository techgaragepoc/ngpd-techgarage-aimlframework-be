
const connectortypeservice = require('../../services/ec/connectortype.service');
const connectorservice = require('../../services/ec/connector.service');
const datasetservice = require('../../services/ec/dataset.service');

const entconnector = require('../../services/ec/data.service');
const commonMethods = require('../../lib/utility');

function getDataSet(entityid, entityname, callback) {
    if (entityid != null && entityid != 'undefined' && entityid != '') {
        datasetservice.Get(entityid, function (err, data) {
            callback(err, data);
        });
    }
    else {
        datasetservice.GetByName(entityname, function (err, data) {
            callback(err, data);
        })
    }
}

function getData(entityid, entityname, parameters, maxrows, callback) {
    var entityinfo = {};

    // datasetservice.GetByName(entityname, function (enterr, dataset) {
    getDataSet(entityid, entityname, function (enterr, dataset) {
        if (enterr || dataset == null) {
            callback({ message: "Error occured on getData - connector entity not found", error: enterr }, null);
            return;
        }
        else {

            if (dataset) {
                // console.log('connector dataset definition....', dataset);
                entityinfo.dataset = dataset;

                connectorservice.Get(dataset.connectorid, function (cnterr, connector) {
                    //  console.log('connector connection....', connector);
                    if (cnterr || connector == null) {
                        callback({ message: "Error occured on getData - connector detail not found", error: cnterr }, null);
                        return;
                    }
                    else {
                        entityinfo.connector = connector;
                        connectortypeservice.Get(connector.connectortypeid, function (cntyperr, connectortype) {
                            //  console.log('connector connection type....', connectortype);
                            if (cntyperr || connectortype == null) {
                                callback({ message: "Error occured on getData - connectortype detail not found", error: cntyperr }, null);
                                return;
                            }
                            else {
                                entityinfo.connectortype = connectortype;
                                entconnector.GetData(entityinfo, parameters, maxrows, function (err, data) {
                                    //   console.log('connector data....', data);
                                    if (err) {
                                        callback({ message: "Error occured on getData", error: err }, null);
                                        return;
                                    }
                                    else {
                                        // console.log('data controller - data', data);
                                        callback(null, { message: "executed succesfully", data: data });
                                        return;
                                    }
                                });

                            } //cntyperr

                        });

                    } //cnterr

                });
            }
            else {
                callback({ message: "Error occured on getData - priorities dataset not found", error: err }, null);
                return;
            }
        }
    });

}


module.exports = {

    GetProviders: function (req, res) {

        entconnector.GetConnectorList(function (err, providers) {
            if (err) {
                res.status(500).json({
                    message: "Error occured on getting provider list from enterprise connector",
                    error: err
                });
                return;
            }
            else {
                res.status(200).json({ message: "executed succesfully", providers: providers });
            }
        });
    },

    TestConnection: function (req, res) {
        var connectiondetail = commonMethods.AsObject(req.body);
        console.log('Test Connection....', connectiondetail);

        entconnector.TestConnection(connectiondetail, function (err, data) {
            if (err) {
                res.status(200).json({ message: "Error occured on TestConnection", result: { "success": false, error: err } });
            }
            else {
                // console.log('data controller - data', data);
                res.status(200).json({ message: "TestConnection executed succesfully", result: data });
            }
        });
    },

    GetData: function (req, res) {

        //console.log('header..reqdata..',req);
        var reqdata = commonMethods.AsObject(req.header("reqdata"));
        console.log(reqdata, reqdata.maxrows);

        getData(reqdata.entityid, reqdata.entity, reqdata.parameters, reqdata.maxrows, function (err, success) {
            if (err) {
                res.status(500).json(err); return;
            }
            else {
                res.status(200).json(success); return;
            }
        });
    },
    /*
        GetDataForFulfillmentService: function(entityname, parameters, maxrows, callback) {
    
            getData(entityname, parameters, maxrows, function(err, success) {
                            callback(err,success); return;
            });
    
        }, //getData
    */

}    