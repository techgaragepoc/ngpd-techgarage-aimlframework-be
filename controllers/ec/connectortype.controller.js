const connectorTypeService = require("../../services/ec/connectortype.service");
const commonMethods = require("../../lib/utility");

module.exports = {
  GetAll: function (req, res) {
    connectorTypeService.GetAll(function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.ConnectorType - GetAll method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Get: function (req, res) {
    var connectortypeid = req.params.connectortypeid;

    connectorTypeService.Get(connectortypeid, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.ConnectorType - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  GetByCatgSubCatg: function (req, res) {
    var category = req.params.category;
    var subcategory = req.params.subcategory;

    connectorTypeService.GetByCatgSubCatg(category, subcategory, function (
      err,
      data
    ) {
      if (err) {
        res
          .status(500)
          .json({
            message:
            "Error occured on EC.ConnectorType - GetByCatgSubCatg method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  GetByName: function (req, res) {
    var connectortypename = req.params.connectortypename;

    connectorTypeService.GetByName(connectortypename, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.ConnectorType - GetByName method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Create: function (req, res) {
    var connectortype = commonMethods.AsObject(req.body);

    connectorTypeService.Create(connectortype, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.ConnectorType - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Update: function (req, res) {
    var connectortype = commonMethods.AsObject(req.body);

    connectorTypeService.Update(connectortype, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.ConnectorType - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  },

  Delete: function (req, res) {
    var connectortypeid = req.params.connectortypeid;

    connectorTypeService.Delete(connectortypeid, function (err, data) {
      if (err) {
        res
          .status(500)
          .json({
            message: "Error occured on EC.ConnectorType - Get method",
            error: err
          });
        return;
      } else {
        res.status(200).json({ message: "executed succesfully", data: data });
      }
    });
  }
};
