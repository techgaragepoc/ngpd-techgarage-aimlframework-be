var request = require("request");

var postdata = {
    "nlpModel": "Health01",
    "nlpIntent": "nlp005",
    "nlpPOS":  {
                   "Nouns": [
                               { "type" : "NOUN", "value" : "What", "lemma" : "what"},
                               { "type" : "PROPN", "value" : "Remuneration", "lemma" : "remuneration"},
                               { "type" : "PROPN", "value" : "Nestle", "lemma" : "Nestle"}
                           ],
                   "Verbs": [
                               { "type" : "VERB", "value" : "is", "lemma" : "be"},
                               { "type" : "VERB", "value" : "broking", "lemma" : "brok"}
                           ],
                   "ADJ" : [
                               { "type" : "ADJ", "value" : "total", "lemma" : "total"} 
                           ],
                   "Parse_Tree" : [
                                       { "node" : "What", "parent" : "is"},
                                       { "node" : "is", "parent" : "is"},
                                       { "node" : "the", "parent" : "Remuneration"},
                                       { "node" : "total", "parent" : "Remuneration"},
                                       { "node" : "broking", "parent" : "Remuneration"},
                                       { "node" : "Remuneration", "parent" : "is"},
                                       { "node" : "from", "parent" : "Remuneration"},
                                       { "node" : "Nestle", "parent" : "from"} 
                               ], 
                   "Entities" : [
                                   { "entity_type" : "CLIENT", "value" : "Nestle"}
                               ],
                   "POS" : [   { "word" : "What", "pos" : "NOUN_WP"},
                               { "word" : "is", "pos" : "VERB_VBZ"},
                               { "word" : "the", "pos" : "DET_DT"},
                               { "word" : "total", "pos" : "ADJ_JJ"},
                               { "word" : "broking", "pos" : "VERB_VBG"},
                               { "word" : "Remuneration", "pos" : "PROPN_NNP"},
                               { "word" : "from", "pos" : "ADP_IN"},
                               { "word" : "Nestle", "pos" : "PROPN_NNP"}
                           ]
       }
   
   };

var options = { 
  method: 'POST',
  url: 'http://localhost:5001/api/intentfulfillment/engine/execute',
  headers: 
   { 
     'cache-control': 'no-cache' 
    },
  body: JSON.stringify(postdata)
};

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});
