var config = require('../../config');
var logger = require('../../lib/logger');
var utility = require('../../lib/utility');

var NlpLanguageModel = require('../../models/nlp/nlplanguagemodel');
var IntentUtterance = require('../../models/intent/IntentUtterance');
var Intent = require('../../models/intent/intent');
var Project = require('../../models/project/project.model');
var ProjectOption = require('../../models/project/projectoption.model');
var IntentUtteranceService = require('../intent/intent.utterance.service');

var request = require('request');
var _ = require('lodash');
var Q = require('q');

module.exports = {
    getByName: function (_modelName) {
        var deferred = Q.defer();

        NlpLanguageModel.findOne({ name: _modelName })
            .populate({ path: 'cultureCode', select: { 'code': 1, 'name': 1 } })
            .populate({ path: 'updatedBy', select: 'username' })
            .exec(function (err, data) {
                if (err) {
                    deferred.reject(err.name + ': ' + err.message);
                }
                else {
                    deferred.resolve(data);
                }
            });

        return deferred.promise;
    },
    getById: function (_id) {
        var deferred = Q.defer();

        NlpLanguageModel.findById(_id)
            .populate({ path: 'cultureCode', select: { 'code': 1, 'name': 1 } })
            .populate({ path: 'updatedBy', select: 'username' })
            .exec(function (err, data) {
                if (err) {
                    deferred.reject(err.name + ': ' + err.message);
                }
                else {
                    deferred.resolve(data);
                }
            });

        return deferred.promise;
    },
    // find the user
    getAll: function () {
        var deferred = Q.defer();

        NlpLanguageModel.find()
            .populate({ path: 'cultureCode', select: { 'code': 1, 'name': 1 } })
            .populate({ path: 'updatedBy', select: 'username' })
            .exec(function (err, data) {
                if (err) {
                    deferred.reject(err.name + ': ' + err.message);
                }
                else {
                    deferred.resolve(data);
                    // console.log(data);
                }
            });

        return deferred.promise;
    },
    getByUserId: function (_id) {
        var deferred = Q.defer();

        NlpLanguageModel.find(
            {
                $or:
                [
                    { sysdefined: true },
                    { shared: true },
                    { updatedBy: utility.AsObjectId(_id) }
                ]
            },
            null,
            // { sort: { sysdefined: -1, shared: -1, name: 1 } }
            { sort: { name: 1 } }
        )
            .populate({ path: 'cultureCode', select: { 'code': 1, 'name': 1 } })
            .populate({ path: 'updatedBy', select: 'username' })
            .exec(function (err, data) {
                if (err) {
                    deferred.reject(err.name + ': ' + err.message);
                }
                else {
                    deferred.resolve(data);
                }
            });

        return deferred.promise;
    },
    createNlpLanguageModel: function (nlpParam) {
        var deferred = Q.defer();

        function createLangModel() {
            NlpLanguageModel.create(nlpParam,
                function (err) {
                    if (err) { deferred.reject(err.name + ': ' + err.message); }
                    else {
                        var intent = new Intent({ CategoryName: 'none', ModelName: nlpParam.name });
                        // Create none intent 
                        intent.save(
                            function (intentError, result) {
                                if (intentError) { deferred.reject(intentError.name + ': ' + intentError.message); }

                                if (result) {
                                    // console.log('result', result);

                                    // Create a dummy utterance for returning score and none intent
                                    var utterance = new IntentUtterance({ Category: result._id, UtteranceText: config.nlp.defaults.utterance });

                                    utterance.save(
                                        function (uttError, utt) {
                                            if (uttError) { deferred.reject(uttError.name + ': ' + uttError.message); }

                                            if (utt) {
                                                IntentUtteranceService.trainEntireModel(nlpParam.name, nlpParam.userName)
                                                    .then(deferred.resolve({ success: true, data: 'Model created successfully' }))
                                                    .catch((error) => deferred.reject(error));
                                            }
                                        });
                                }
                            });
                    }
                });
        }

        nlpParam.modified = new Date();

        // ensure a unique model name and sysdefined combination on create action
        if (nlpParam.sysdefined) {
            NlpLanguageModel.findOne({ name: nlpParam.name, sysdefined: true }, function (error, langmodel) {
                if (error) deferred.reject(err.name + ': ' + err.message);

                if (langmodel) {
                    deferred.resolve(
                        {
                            success: false,
                            data: 'A system defined model with the same name already exists. Please use a different model name.'
                        });
                }
                else {
                    createLangModel();
                }
            });
        }
        else {
            createLangModel();
        }

        return deferred.promise;
    },
    createModel: function (modelName) {
        var deferred = Q.defer();

        request.post({
            url: config.externalendpoints.nlp + '/addnewmodel',
            method: 'POST',
            headers: {
                'content-type': 'application/json',
            },
            json: { 'modelname': modelName.LanguageModelName }
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                deferred.resolve(body); // Show the HTML for the Google homepage. 
            }
        });

        return deferred.promise;
    },
    updateLanguageModel: function (model) {
        var deferred = Q.defer();

        // console.log('model', model);

        function updateLangModel() {
            NlpLanguageModel.updateOne(
                { _id: model._id },
                {
                    $set:
                    {
                        name: model.name,
                        cultureCode: model.cultureCode,
                        description: model.description,
                        sysdefined: model.sysdefined,
                        shared: model.shared,
                        updatedBy: model.updatedBy
                    }
                },
                function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    deferred.resolve({ success: true, data: 'Model details updated successfully.' });
                });
        }

        if (model == null) {
            deferred.reject({ success: false, message: 'No Model Info found' });
        }

        let checkDuplicate = true;
        // Get the current values, and apply attribute specific validations
        NlpLanguageModel.findById(model._id, function (err, nlpLangModel) {
            // console.log('nlpLangModel', nlpLangModel);

            checkDuplicate = (nlpLangModel !== null);
            checkDuplicate = checkDuplicate && (model.sysdefined === true && nlpLangModel.sysdefined === false);
            checkDuplicate = checkDuplicate || (model.name !== nlpLangModel.name);

            // ensure a unique model name and sysdefined combination on update action
            if (checkDuplicate) {
                NlpLanguageModel.findOne({ name: model.name, sysdefined: true }, function (error, langmodel) {
                    if (error) deferred.reject(err.name + ': ' + err.message);

                    if (langmodel) {
                        deferred.resolve(
                            {
                                success: false,
                                data: 'A system defined model with the same name already exists. Please use a different model name.'
                            });
                    }
                    else {
                        updateLangModel();
                    }
                });
            }
            else if
            (
                (model.sysdefined === false && nlpLangModel.sysdefined === true) ||
                (model.shared === false && nlpLangModel.shared === true)
            ) {
                // Check if the model is in use in any project
                // Check if project opted for KA and then match the model id
                ProjectOption.findOne(
                    { name: 'nlp.ka.values' },
                    (optError, option) => {
                        if (optError) { deferred.reject({ success: false, message: 'Error finding project option' }); }

                        Project.find(
                            // Find projects that have chosen the KA model in the options
                            { 'options': { $elemMatch: { id: option._id, values: { $in: [model._id] } } } },
                            (projError, projects) => {
                                if (projError) {
                                    deferred.reject({ success: false, message: 'Error finding project' });
                                }
                                else if (projects != null && projects.length > 0) {
                                    // console.log('projects', projects);
                                    let projNames = projects.map(x => { return x.name }).join(",");

                                    deferred.reject({
                                        success: false,
                                        message: 'Model is in use in projects [' + projNames + ']'
                                    });
                                }
                                else {
                                    updateLangModel();
                                }
                            });
                    });
            }
            else {
                updateLangModel();
            }
        });

        return deferred.promise;
    },
    deleteLanguageModel: function (_id) {
        var deferred = Q.defer();
        var successResponse;

        NlpLanguageModel.findById(_id, function (err, model) {
            if (err) { deferred.reject({ success: false, message: 'Matching model not found' }); }

            if (model != null) {
                // console.log('model', model);

                if (model.sysdefined) {
                    deferred.reject({ success: false, message: 'System defined models cannot be deleted' });
                }
                else {
                    // Check if the model is in use in any project
                    // Check if project opted for KA and then match the model id
                    ProjectOption.findOne(
                        { name: 'nlp.ka.values' },
                        (optError, option) => {
                            if (optError) { deferred.reject({ success: false, message: 'Error finding project option' }); }

                            // console.log('projoption', option);

                            Project.find(
                                // Find projects that have chosen the KA model in the options
                                { 'options': { $elemMatch: { id: option._id, values: { $in: [model._id] } } } },
                                (projError, projects) => {
                                    if (projError) {
                                        deferred.reject({ success: false, message: 'Error finding project' });
                                    }
                                    else if (projects != null && projects.length > 0) {
                                        let projNames = projects.map(x => { return x.name }).join(",");

                                        deferred.reject({
                                            success: false,
                                            message: 'Model is in use in projects [' + projNames + ']'
                                        });
                                    }
                                    else {
                                        NlpLanguageModel.deleteOne(
                                            {
                                                _id: model._id
                                            },
                                            (modelError) => {
                                                if (modelError) { deferred.reject({ success: false, message: 'Error deleting model' }); }

                                                Intent.find(
                                                    {
                                                        ModelName: model.name
                                                    },
                                                    (error, intents) => {
                                                        if (error) { deferred.reject({ success: false, message: 'Error finding intents' }); }

                                                        // console.log('intents', intents);

                                                        IntentUtterance.deleteMany(
                                                            // IntentUtterance.find(
                                                            {
                                                                'Category': { '$in': intents.map(filter => filter._id) }
                                                            },
                                                            (utteranceError) => {
                                                                if (utteranceError) { deferred.reject({ success: false, message: 'Error deleting utterances' }); }

                                                                Intent.deleteMany(
                                                                    {
                                                                        ModelName: model.name
                                                                    },
                                                                    (intentError) => {
                                                                        if (intentError) { deferred.reject({ success: false, message: 'Error deleting intents' }); }
                                                                    });
                                                            });
                                                    });
                                            });

                                        deferred.resolve({ success: true, data: 'Model deleted successfully' });
                                    }
                                });
                        });
                }

                // Intent.find(
                //     {
                //         ModelName: model.name
                //     },
                //     (error, intents) => {
                //         if (error) { deferred.reject({ success: false, message: 'Error finding intents.' }); }

                //         IntentUtterance.find(
                //             {
                //                 'Category': { '$in': intents.map(filter => filter._id) },
                //                 'Entity': null || undefined || ''
                //             })
                //             .aggregate()
                //             .project({ 'Utterances': { '$size': '$Category' } })
                //             .group({ 'Category': '$Category', 'count': { '$sum': '$Utterances' } })
                //             .exec(function (err, result) {
                //                 if (error) { deferred.reject({ success: false, message: 'Error deleting utterance' }); }

                //                 deferred.resolve({ success: true, data: result });
                //             })
                //     })
            }
            else {
                deferred.resolve({ success: true, data: 'Matching Model not found' });
            }
        });

        return deferred.promise;
    },
    downloadLanguageModel: function (_id) {
        var deferred = Q.defer();
        deferred.resolve({ modelname: 'test' + _id });
        return deferred.promise;
    }
}
