var config = require('../../config');
var logger = require('../../lib/logger');
var utility = require('../../lib/utility');

// var db = require('../../models/connection');
var Project = require('../../models/project/project.model');
var ProjectOption = require('../../models/project/projectoption.model');
var Category = require('../../models/category/category.model');
var User = require('../../models/user/user');

var _ = require('lodash');
var jwt = require('jsonwebtoken');
var Q = require('q');

var service = {};

service.getAll = getAll;
service.getById = getById;
service.getByUserId = getByUserId;
service.getByName = getByName;

service.create = create;
service.update = update;
service.delete = _delete;

service.getCategory = getCategory;
service.createCategory = createCategory;

service.getProjectOptions = getProjectOptions;
service.createProjectOption = createProjectOption;
service.updateProjectOption = updateProjectOption;

module.exports = service;

function getAll() {
    var deferred = Q.defer();

    Project.find({})
        // .populate({ path: "category" })
        .populate('category', 'name')
        // .populate({ path: "options" })
        .populate({ path: "options.id", select: 'name' })
        //.populate({ path: "updatedBy" })
        .exec(function (err, projects) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve(projects);
        });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    // Check if the id passed contains hex chars, then assume it to be a objectid value else use the value as-is
    // var hex = /[0-9A-Fa-f]{6}/g;
    // var id = (hex.test(id)) ? mongoose.Schema.Types.ObjectId(_id) : _id;

    Project.findById(utility.AsObjectId(_id))
        .populate('category', 'name')
        // .populate({ path: "options" })
        .populate({ path: "options.id", select: 'name' })
        // .populate({ path: "updatedBy" })
        .exec(function (err, proj) {
            // console.log('proj: ' + JSON.stringify(proj));

            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve(proj);
        });

    return deferred.promise;
}

function getByUserId(_userId) {
    var deferred = Q.defer();
    // var objId = new mongoose.Types.ObjectId((_userId.length < 12) ? "123456789012" : _userId);

    Project.find({ updatedBy: utility.AsObjectId(_userId) })
        .populate('category', 'name')
        // .populate({ path: "options" })
        .populate({ path: "options.id", select: 'name' })
        // .populate({ path: "updatedBy" })
        .exec(function (err, projects) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve(projects);
        });

    return deferred.promise;
}

function getByName(_name) {
    var deferred = Q.defer();

    Project.findOne({ name: _name })
        // .populate("category")
        // .populate("updatedBy")
        .exec(function (err, proj) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

function getCategory() {
    // console.log('category service method called');
    var deferred = Q.defer();

    Category.find({}, function (err, cats) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        deferred.resolve(cats);
    });

    return deferred.promise;
}

function getProjectOptions() {
    var deferred = Q.defer();

    ProjectOption.find({})
        .exec(function (err, options) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve(options);
        });

    return deferred.promise;
}

function createCategory(catParam) {
    var deferred = Q.defer();

    // validation
    Category.findOne(
        {
            name: catParam.name
        },
        function (err, cat) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (cat) {
                // cat name already exists
                deferred.reject('Category "' + catParam.name + '" already exists.');
            }
            else {
                createCat(catParam);
            }
        });

    function createCat(cat) {
        var data = new Category(cat);

        // console.log(JSON.stringify(data));
        data.save(cat,
            function (err, savedCat) {
                if (err) deferred.reject(err.name + ':' + err.message);

                deferred.resolve('Category created successfully');
            });
    }

    return deferred.promise;
}

function create(projParam) {
    var deferred = Q.defer();

    // validation
    Project.findOne(
        {
            name: projParam.name
        },
        function (err, proj) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (proj) {
                // proj name already exists
                deferred.reject('Project Name "' + projParam.name + '" is already in use');
            }
            else {
                const data = new Project(projParam);

                data.save(
                    function (err) {
                        if (err) deferred.reject(err.name + ': ' + err.message);

                        deferred.resolve();
                    });
            }
        });

    return deferred.promise;
}

function update(_id, projParam) {
    var deferred = Q.defer();

    // validation
    Project.findById(_id, function (err, proj) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (proj.name !== projParam.name) {
            // projname has changed so check if the new projname is already taken
            Project.findOne(
                {
                    name: projParam.name
                },
                function (err, proj) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (proj) {
                        // projname already exists
                        deferred.reject('Project name "' + projParam.name + '" is already taken')
                    }
                    else {
                        updateProj();
                    }
                });
        }
        else {
            updateProj();
        }
    });

    function updateProj() {
        // fields to update
        var set = {
            options: projParam.options,
            modified: new Date()
        };

        //Project.update()
        Project.updateOne(
            { _id: _id },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    Project.deleteOne(
        //{ _id: mongo.helper.toObjectID(_id) },
        { _id: _id },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

function createProjectOption(option) {
    var deferred = Q.defer();

    // validation
    ProjectOption.findOne(
        {
            name: option.name
        },
        function (err, projOption) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (projOption) {
                // proj name already exists
                deferred.reject('Project Option "' + option.name + '" already exists');
            }
            else {
                const data = new ProjectOption(option);

                data.save(
                    function (err) {
                        if (err) deferred.reject(err.name + ': ' + err.message);

                        deferred.resolve();
                    });
            }
        });

    return deferred.promise;
}

function updateProjectOption(option) {
    var deferred = Q.defer();

    // validation
    ProjectOption.findOne(
        {
            name: option.name
        },
        function (err, projOption) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (projOption) {
                ProjectOption.updateOne(
                    { _id: projOption._id },
                    {
                        $set: {
                            datatype: option.datatype,
                            multichoice: option.multichoice,
                            modified: new Date()
                        }
                    },
                    // { $set: { istrained: true } },
                    function (err, upd) {
                        if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });
                        deferred.resolve();
                    });
            }
        });

    return deferred.promise;
}