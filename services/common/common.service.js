var config = require('../../config');
var logger = require('../../lib/logger');
var db = require('../../models/connection');
var Cultures = require('../../models/culture/culture.model');
var utility = require('../../lib/utility');

var Q = require('q');

var service = {};

service.getAllCultures = getAllCultures;
service.getCultureByCode = getCultureByCode;
service.createCulture = createCulture;

module.exports = service;

function getAllCultures() {
    var deferred = Q.defer();

    Cultures.find()
        .populate({ path: "updatedBy", select: 'username' })
        .exec(function (err, cultures) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve(cultures);
        });

    return deferred.promise;
}

function getCultureByCode(cultureCode) {
    // console.log(cultureCode);
    var deferred = Q.defer();

    Cultures.findOne({ code: cultureCode })
        .populate({ path: "updatedBy" })
        .exec(function (err, culture) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            //  console.log(culture);
            deferred.resolve(culture);
        });

    return deferred.promise;
}

function createCulture(culture) {
    var deferred = Q.defer();

    this.getCultureByCode(culture.code)
        .then(cult => {
            // console.log('cult', cult);

            if (cult == null) {
                const newCult = new Cultures(culture);

                newCult.save(
                    function (err) {
                        if (err) deferred.reject(err.name + ': ' + err.message);

                        deferred.resolve();
                    });
            }
            else {
                deferred.reject('Culture Code "' + culture.code + '" already exists')
            }
        })
        .catch(error => deferred.reject(error));

    return deferred.promise;
}