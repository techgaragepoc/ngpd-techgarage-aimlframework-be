const mongoose = require('mongoose');
const Connector = require('../../models/ec/connector.model');
const uuid = require('uuid/v4');


module.exports = {
    
        Create: function(connector,callback) {
                              var randomid = uuid();
        
                              if (connector && connector.name) {
    
                                this.GetByName(connector.name,function(fnderr, existingconnector) {
    
                                     //if error occurred while check the record existance
                                     if (fnderr) {
                                        callback({
                                            message: "An Error has occured while checking existance before creating connector",
                                            error: fnderr
                                        },null);
                                     }   
                                     else {
                                         //if not found
                                         if (!existingconnector) {
    
                                                var newconnector = new Connector({ 
                                                    connectorid: randomid,
                                                    name: connector.name, 
                                                    connectortypeid: connector.connectortypeid,
                                                    connectiondetail: connector.connectiondetail,
                                                    sysdefined: connector.sysdefined,
                                                    createdby: connector.createdby.toLowerCase(),
                                                });
    
                                                newconnector.save(function (err, result) {
                                                    if (err) {
                                                        callback({
                                                                    message: "An Error has occured while creating a new connector",
                                                                    error: err
                                                                },null);
                                                        
                                                    } else {
                                                        callback(null, {
                                                                    message: "Connector has been created",
                                                                    connector: result
                                                                });
                                                    }
                                                });
                                         }
                                         else {  //else throw error - already exists
                                            callback({
                                                message: "Connector already exists!!",
                                                error: existingconnector
                                            },null);
                                         }
    
    
                                     }
    
    
                                });
    
                                
                            }
                            else {
                               var error =   {
                                                message: 'An error occurred - connector not defined',
                                                error: null
                                            };
                                callback(error,null);                                            
                            }
          },  
    
        Update: function(connector,callback) {
    
                            if (connector) {
                                Connector.findOne({"connectorid": connector.connectorid}, function (err, existingconnector) {
                                    if (err) {
                                        callback({
                                                message: 'An error occurred while updating a connector detail',
                                                error: err
                                            },null);
                                    }
    
                                    if (!existingconnector) {
                                        callback({
                                                    message: 'connector not found',
                                                    error: {connectorid: connector.connectorid,"err":"connector not found"}
                                                },null);
                                    }
                                    else {
    
                                        Connector.update({"connectorid": connector.connectorid},
                                                    {$set: {  
                                                        name: connector.name, 
                                                        connectortypeid: connector.connectortypeid,
                                                        connectiondetail: connector.connectiondetail,
                                                        sysdefined: connector.sysdefined,
                                                        lastmodifiedby: connector.lastmodifiedby.toLowerCase(),
                                                        lastmodifiedon: Date.now(),
                                                     }},
                                                    function (err, result) {
                                                    if (err) {
                                                        callback({
                                                                    message: "An Error has occured while updating connector details",
                                                                    error: err
                                                                },null);
                                                        
                                                    } else {
                                                        callback(null, {
                                                                    message: "Connector detail has been updated",
                                                                    connector: result
                                                                });
                                                    }
                                        });
                                    }
                                });
                            }
                            else {
                               var error =   {
                                                message: 'An error occurred - connectorid not defined',
                                                error: null
                                            };
                                callback(error,null);                                            
                            }
          },  
    
          Get: function(connectorid,callback) {
        
                            if (connectorid) {
                                Connector.findOne({"connectorid": connectorid}, function (err, existingconnector) {
                                    if (err) {
                                        callback({
                                                message: 'An error occurred while fetcing connector detail',
                                                error: err
                                            },null);
                                    }
    
                                    
                                    callback(null,existingconnector);
                                    
                                });
                            }
                            else {
                               var error =   {
                                                message: 'An error occurred - connectorid not defined',
                                                error: null
                                            };
                                callback(error,null);                                            
                            }
                                      
          },
    
          GetByName: function(connectorname,callback) {
            
                                if (connectorname) {
                                    Connector.findOne({"name": { $regex: new RegExp("^" + connectorname.toLowerCase() + "$", "i") }}, 
                                        function (err, existingconnector) {
                                            if (err) {
                                                callback({
                                                        message: 'An error occurred while fetcing connector detail',
                                                        error: err
                                                    },null);
                                            }
        
                                        
                                            callback(null,existingconnector);
                                        
                                    });
                                }
                                else {
                                   var error =   {
                                                    message: 'An error occurred - connectorname not defined',
                                                    error: null
                                                };
                                    callback(error,null);                                            
                                }
                                          
              },
    
          GetAll: function(callback) {
                               
                                    Connector.find({}, function (err, existingconnectors) {
                                        if (err) {
                                            callback({
                                                    message: 'An error occurred while fetcing connectors',
                                                    error: err
                                                },null);
                                        }
        
                                        
                                        callback(null,existingconnectors);
                                        
                                    });
            },
    
            Delete: function(connectorid, callback) {
                
                                    if (connectorid) {
                                        Connector.findOne({"connectorid": connectorid}, 
                                            function (err, existingconnector) {
                                                if (err) {
                                                    callback({
                                                            message: 'An error occurred while fetching connector detail',
                                                            error: err
                                                        }, null);
                                                }
            
                                            if (!existingconnector) {
                                                callback({
                                                            message: 'connector not found',
                                                            error: { "connectorid": connectorid,
                                                                     "err":"connector not found"}
                                                        }, null);
                                            }
                                            else {
        
                                                //remove Connector here then callback
                                                Connector.remove({"connectorid": connectorid},function(remerr){
                                                    if (remerr) { 
                                                        callback({
                                                            message: 'An error occurred while removing connector type',
                                                            error: err
                                                        },null);
                                                    }
                                                    else {
                                                        callback(null,existingconnector);
                                                    }
                                                });
                                                
                                            }
                                            
                                            
                                        });
                                    }
                                    else {
                                       var error =   {
                                                        message: 'An error occurred - connectorid not defined',
                                                        error: null
                                                    };
                                        callback(error,null);                                            
                                    }
                                              
                  }
             
    }
    