const request = require('request');
const config = require('../../config');
const commonMethods = require('../../lib/utility');
const baseurl = config.modules.enterpriseconnector.baseurl;

module.exports = {
    GetConnectorList: function (callback) {

        var url = baseurl + '/connectorlist';
        var options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        request(url, options, function (err, response, body) {

            // console.log('reponse from ec...',JSON.parse(body));
            if (err) {
                callback(err, null);
            }
            else {
                console.log('statuscode=', response.statusCode);
                if (response.statusCode >= 400) {
                    callback(response.statusMessage, null);
                }
                else {
                    var result = commonMethods.AsObject(body);
                    callback(null, result.data);
                }
            }
        });
    },

    TestConnection: function (connectiondetail, callback) {

        var url = baseurl + '/testconnection';
        var options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            json: true,
            body: {
                connectiondetail: connectiondetail
            }
        };

        request(url, options, function (err, response, body) {
            // console.log('reponse from ec...',JSON.parse(body));
            if (err) {
                callback(err, null);
            }
            else {
                console.log('statuscode=', response.statusCode, url);
                if (response.statusCode >= 400) {
                    callback(response.statusMessage, null);
                }
                else {
                    var result = commonMethods.AsObject(body);
                   // console.log(result);
                    callback(null, result);
                }
            }
        });

    },

    GetProrityDataSet: function (datasets, decidingfactor, callback) {

        var url = baseurl + '/prioritydataset';
        var options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            json: true,
            body: {
                datasets: datasets,
                decidingfactor: decidingfactor
            }
        };

        request(url, options, function (err, response, body) {
            // console.log('reponse from ec...',JSON.parse(body));
            if (err) {
                callback(err, null);
            }
            else {
                console.log('statuscode=', response.statusCode);
                if (response.statusCode >= 400) {
                    callback(response.statusMessage, null);
                }
                else {
                    var result = commonMethods.AsObject(body);
                    callback(null, result.data);
                }
            }
        });
    },

    GetData: function (entityinfo, parameters, maxrows, callback) {

        var url = baseurl + '/data';
        var options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            json: true,
            body: {
                entityinfo: entityinfo,
                parameters: parameters,
                maxrows: maxrows
            }
        };

        request(url, options, function (err, response, body) {
            // console.log('reponse from ec...',JSON.parse(body));
            if (err) {
                callback(err, null);
            }
            else {
                console.log('statuscode=', response.statusCode);
                if (response.statusCode >= 400) {
                    callback(response.statusMessage, null);
                }
                else {
                    var result = commonMethods.AsObject(body);
                    callback(null, result.data);
                }
            }
        });
    }
}