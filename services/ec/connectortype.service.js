const mongoose = require('mongoose');
const ConnectorType = require('../../models/ec/connectortype.model');
const uuid = require('uuid/v4');

module.exports = {
    Create: function (connectortype, callback) {
        var randomid = uuid();

        if (connectortype && connectortype.name) {

            this.GetByName(connectortype.name, function (fnderr, existingconnectortype) {

                //if error occurred while check the record existance
                if (fnderr) {
                    callback({
                        message: "An Error has occured while checking existance before creating connector type",
                        error: fnderr
                    }, null);
                }
                else {
                    //if not found
                    if (!existingconnectortype) {

                        var newconnectortype = new ConnectorType({
                            connectortypeid: randomid,
                            name: connectortype.name,
                            category: connectortype.category.toLowerCase(),
                            subcategory: connectortype.subcategory.toLowerCase(),
                            version: connectortype.version,
                            provider: connectortype.provider,
                            sysdefined: connectortype.sysdefined,
                            createdby: connectortype.createdby.toLowerCase(),
                        });

                        newconnectortype.save(function (err, result) {
                            if (err) {
                                callback({
                                    message: "An Error has occured while creating connector type",
                                    error: err
                                }, null);

                            } else {
                                callback(null, {
                                    message: "ConnectorType has been created",
                                    connectortype: result
                                });
                            }
                        });
                    }
                    else {  //else throw error - already exists
                        callback({
                            message: "Connector Type already exists!!",
                            error: existingconnectortype
                        }, null);
                    }
                }

            });




        }
        else {
            var error = {
                message: 'An error occurred - connectortype not defined',
                error: null
            };
            callback(error, null);
        }
    },

    Update: function (connectortype, callback) {

        if (connectortype) {
            ConnectorType.findOne({ "connectortypeid": connectortype.connectortypeid }, function (err, existingconnectortype) {
                if (err) {
                    callback({
                        message: 'An error occurred',
                        error: err
                    }, null);
                }

                if (!existingconnectortype) {
                    callback({
                        message: 'connectortype not found',
                        error: { connectortypeid: connectortype.connectortypeid, "err": "connectortype not found" }
                    }, null);
                }
                else {

                    ConnectorType.update({ "connectortypeid": connectortype.connectortypeid },
                        {
                            $set: {
                                name: connectortype.name,
                                category: connectortype.category.toLowerCase(),
                                subcategory: connectortype.subcategory.toLowerCase(),
                                version: connectortype.version,
                                provider: connectortype.provider,
                                sysdefined: connectortype.sysdefined,
                                lastmodifiedby: connectortype.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                callback({
                                    message: "An Error has occured while updating connectortype details",
                                    error: err
                                }, null);

                            } else {
                                callback(null, {
                                    message: "ConnectorType detail has been updated",
                                    connectortype: result
                                });
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - connectortypeid not defined',
                error: null
            };
            callback(error, null);
        }
    },

    Get: function (connectortypeid, callback) {

        if (connectortypeid) {
            ConnectorType.findOne({ "connectortypeid": connectortypeid }, function (err, existingconnectortype) {
                if (err) {
                    callback({
                        message: 'An error occurred',
                        error: err
                    }, null);
                }


                callback(null, existingconnectortype);

            });
        }
        else {
            var error = {
                message: 'An error occurred - connectortypeid not defined',
                error: null
            };
            callback(error, null);
        }

    },

    GetByCatgSubCatg: function (category, subcategory, callback) {

        if (category && subcategory) {
            ConnectorType.findOne({
                "category": category.toLowerCase(),
                "subcategory": subcategory.toLowerCase()
            },
                function (err, existingconnectortype) {
                    if (err) {
                        callback({
                            message: 'An error occurred',
                            error: err
                        }, null);
                    }


                    callback(null, existingconnectortype);

                });
        }
        else {
            var error = {
                message: 'An error occurred - connectortypeid not defined',
                error: null
            };
            callback(error, null);
        }

    },

    GetByName: function (connectortypename, callback) {

        if (connectortypename) {
            ConnectorType.findOne({ "name": { $regex: new RegExp("^" + connectortypename.toLowerCase() + "$", "i") } },
                function (err, existingconnectortype) {
                    if (err) {
                        callback({
                            message: 'An error occurred',
                            error: err
                        }, null);
                    }


                    callback(null, existingconnectortype);

                });
        }
        else {
            var error = {
                message: 'An error occurred - connectortypeid not defined',
                error: null
            };
            callback(error, null);
        }

    },

    GetAll: function (callback) {

        ConnectorType.find({}, function (err, existingconnectortypes) {
            if (err) {
                callback({
                    message: 'An error occurred',
                    error: err
                }, null);
            }


            callback(null, existingconnectortypes);

        });
    },

    Delete: function (connectortypeid, callback) {

        if (connectortypeid) {
            ConnectorType.findOne({ "connectortypeid": connectortypeid },
                function (err, existingconnectortype) {
                    if (err) {
                        callback({
                            message: 'An error occurred',
                            error: err
                        }, null);
                    }

                    if (!existingconnectortype) {
                        callback({
                            message: 'connectortype not found',
                            error: {
                                "connectortypeid": connectortypeid,
                                "err": "connectortype not found"
                            }
                        }, null);
                    }
                    else {

                        //remove ConnectorType here then callback
                        ConnectorType.remove({ "connectortypeid": connectortypeid }, function (remerr) {
                            if (remerr) {
                                callback({
                                    message: 'An error occurred while removing connector type',
                                    error: err
                                }, null);
                            }
                            else {
                                callback(null, existingconnectortype);
                            }
                        });

                    }


                });
        }
        else {
            var error = {
                message: 'An error occurred - connectortypeid not defined',
                error: null
            };
            callback(error, null);
        }

    }

}
