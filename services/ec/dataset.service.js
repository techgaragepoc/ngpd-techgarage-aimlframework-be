const mongoose = require('mongoose');
const DataSet = require('../../models/ec/dataset.model');
const uuid = require('uuid/v4');

module.exports = {
    Create: function (dataset, callback) {
        var randomid = uuid();

        if (dataset && dataset.name) {

            this.GetByName(dataset.name, function (fnderr, existingdataset) {

                //if error occurred while check the record existance
                if (fnderr) {
                    callback({
                        message: "An Error has occured while checking existance before creating dataset",
                        error: fnderr
                    }, null);
                }
                else {
                    //if not found
                    if (!existingdataset) {

                        var newdataset = new DataSet({
                            datasetid: randomid,
                            name: dataset.name,
                            connectorid: dataset.connectorid,
                            source: dataset.source,
                            ispublic: dataset.ispublic,
                            sysdefined: dataset.sysdefined,
                            createdby: dataset.createdby.toLowerCase(),
                        });

                        newdataset.save(function (err, result) {
                            if (err) {
                                callback({
                                    message: "An Error has occured while creating a new dataset",
                                    error: err
                                }, null);

                            } else {
                                callback(null, {
                                    message: "DataSet has been created",
                                    dataset: result
                                });
                            }
                        });
                    }
                    else {  //else throw error - already exists
                        callback({
                            message: "DataSet already exists!!",
                            error: existingdataset
                        }, null);
                    }
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - dataset not defined',
                error: null
            };
            callback(error, null);
        }
    },

    Update: function (dataset, callback) {

        if (dataset) {
            DataSet.findOne({ "datasetid": dataset.datasetid }, function (err, existingdataset) {
                if (err) {
                    callback({
                        message: 'An error occurred while updating a dataset detail',
                        error: err
                    }, null);
                }

                if (!existingdataset) {
                    callback({
                        message: 'dataset not found',
                        error: { datasetid: dataset.datasetid, "err": "dataset not found" }
                    }, null);
                }
                else {

                    DataSet.update({ "datasetid": dataset.datasetid },
                        {
                            $set: {
                                name: dataset.name,
                                connectorid: dataset.connectorid,
                                source: dataset.source,
                                ispublic: dataset.ispublic,
                                sysdefined: dataset.sysdefined,
                                lastmodifiedby: dataset.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                callback({
                                    message: "An Error has occured while updating dataset details",
                                    error: err
                                }, null);

                            } else {
                                callback(null, {
                                    message: "DataSet detail has been updated",
                                    dataset: result
                                });
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - datasetid not defined',
                error: null
            };
            callback(error, null);
        }
    },

    Get: function (datasetid, callback) {

        if (datasetid) {
            DataSet.findOne({ "datasetid": datasetid }, function (err, existingdataset) {
                if (err) {
                    callback({
                        message: 'An error occurred while fetcing dataset detail',
                        error: err
                    }, null);
                }


                callback(null, existingdataset);

            });
        }
        else {
            var error = {
                message: 'An error occurred - datasetid not defined',
                error: null
            };
            callback(error, null);
        }

    },

    GetByName: function (datasetname, callback) {

        if (datasetname) {
            DataSet.findOne({ "name": { $regex: new RegExp("^" + datasetname.toLowerCase() + "$", "i") } }, function (err, existingdataset) {
                if (err) {
                    callback({
                        message: 'An error occurred while fetcing dataset detail',
                        error: err
                    }, null);
                }


                callback(null, existingdataset);

            });
        }
        else {
            var error = {
                message: 'An error occurred - datasetname not defined',
                error: null
            };
            callback(error, null);
        }

    },

    GetAll: function (callback) {

        DataSet.find({}, function (err, existingdatasets) {
            if (err) {
                callback({
                    message: 'An error occurred while fetcing datasets',
                    error: err
                }, null);
            }


            callback(null, existingdatasets);

        });
    },

    Delete: function (datasetid, callback) {

        if (datasetid) {
            DataSet.findOne({ "datasetid": datasetid },
                function (err, existingdataset) {
                    if (err) {
                        callback({
                            message: 'An error occurred while fetching dataset detail',
                            error: err
                        }, null);
                    }

                    if (!existingdataset) {
                        callback({
                            message: 'dataset not found',
                            error: {
                                "datasetid": datasetid,
                                "err": "dataset not found"
                            }
                        }, null);
                    }
                    else {

                        //remove DataSet here then callback
                        DataSet.remove({ "datasetid": datasetid }, function (remerr) {
                            if (remerr) {
                                callback({
                                    message: 'An error occurred while removing dataset',
                                    error: err
                                }, null);
                            }
                            else {
                                callback(null, existingdataset);
                            }
                        });

                    }


                });
        }
        else {
            var error = {
                message: 'An error occurred - datasetid not defined',
                error: null
            };
            callback(error, null);
        }

    }

}

