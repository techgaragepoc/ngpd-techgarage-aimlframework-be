const mongoose = require('mongoose');
const { NodeDatasetMapping } = require('../../models/intentfulfillment/');
var Q = require('q');

module.exports = {

    Add: function (nodeDatasetMapping) {
        var deferred = Q.defer();
        if (nodeDatasetMapping && nodeDatasetMapping.domainmodelgraphid
            && nodeDatasetMapping.nodeid && nodeDatasetMapping.datasetid) {

            this.GetByMapping(nodeDatasetMapping.domainmodelgraphid, nodeDatasetMapping.nodeid,
                nodeDatasetMapping.datasetid).then(
                    existingnodeDatasetMapping => {
                        console.log('mapping service => ', existingnodeDatasetMapping);
                        //if not found
                        if (existingnodeDatasetMapping == null || existingnodeDatasetMapping.length == 0) {

                            var newnodeDatasetMapping = new NodeDatasetMapping({
                                domainmodelgraphid: nodeDatasetMapping.domainmodelgraphid,
                                nodeid: nodeDatasetMapping.nodeid,
                                datasetid: nodeDatasetMapping.datasetid,
                                attributemapping: nodeDatasetMapping.attributemapping,
                                default: nodeDatasetMapping.default,
                                createdby: nodeDatasetMapping.createdby.toLowerCase(),
                            });

                            newnodeDatasetMapping.save(function (err, result) {
                                if (err) {
                                    deferred.reject({
                                        message: "An Error has occured while adding a new Node Dataset Mapping",
                                        error: err
                                    });

                                } else {
                                    deferred.resolve(result);
                                }
                            });
                        }
                        else {  //else update
                            nodeDatasetMapping["_id"] = existingnodeDatasetMapping[0]["_id"];
                            nodeDatasetMapping["default"] = existingnodeDatasetMapping[0]["default"];;
                            nodeDatasetMapping["lastmodifiedby"] = nodeDatasetMapping["createdby"];

                            this.Update(nodeDatasetMapping).then(
                                result => {
                                    deferred.resolve(result);
                                },
                                err => {
                                    deferred.reject({
                                        message: "An Error has occured while updating a Node Dataset Mapping",
                                        error: err,
                                        input: existingnodeDatasetMapping[0]
                                    });
                                }
                            );
                        }
                    },
                    fnderr => {
                        deferred.reject({
                            message: "An Error has occured while checking existance before adding Node Dataset Mapping",
                            error: fnderr
                        });
                    });
        }
        else {
            var error = {
                message: 'An error occurred - Node Dataset Mapping not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Update: function (nodeDatasetMapping) {
        var deferred = Q.defer();
        if (nodeDatasetMapping) {
            var objId = mongoose.Types.ObjectId(nodeDatasetMapping._id);
            NodeDatasetMapping.findOne({ "_id": objId }, function (err, existingnodeDatasetMapping) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a Node Dataset Mapping detail',
                        error: err
                    });
                }

                if (!existingnodeDatasetMapping) {
                    deferred.reject({
                        message: 'nodeDatasetMapping not found',
                        error: { "_id": nodeDatasetMapping._id, "err": "Node Dataset Mapping not found" }
                    });
                }
                else {

                    NodeDatasetMapping.update({ "_id": objId },
                        {
                            $set: {
                                domainmodelgraphid: nodeDatasetMapping.domainmodelgraphid,
                                nodeid: nodeDatasetMapping.nodeid,
                                datasetid: nodeDatasetMapping.datasetid,
                                attributemapping: nodeDatasetMapping.attributemapping,
                                default: nodeDatasetMapping.default,
                                lastmodifiedby: nodeDatasetMapping.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating Node Dataset Mapping details",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - Node Dataset Mapping id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Get: function (nodeDatasetMappingid) {
        var deferred = Q.defer();
        if (nodeDatasetMappingid) {
            try {
                var objId = mongoose.Types.ObjectId(nodeDatasetMappingid);
                NodeDatasetMapping.findOne({ "_id": objId }, function (err, existingnodeDatasetMapping) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing Node Dataset Mapping detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingnodeDatasetMapping);
                });
            }
            catch (ex) {
                deferred.reject({
                    message: 'An error occurred while fetcing Node Dataset Mapping detail, nodeDatasetMappingid is not valid',
                    error: ex
                });
            }
        }
        else {
            var error = {
                message: 'An error occurred - nodeDatasetMappingid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetByMapping: function (domainmodelgraphid, nodeid, datasetid) {
        var deferred = Q.defer();
        if (domainmodelgraphid != null && nodeid != null && datasetid != null
            && domainmodelgraphid != 'undefined' && nodeid != 'undefined' && datasetid != 'undefined') {
            NodeDatasetMapping.find({
                "domainmodelgraphid": domainmodelgraphid,
                "nodeid": nodeid,
                "datasetid": datasetid
            },
                function (err, existingnodeDatasetMappings) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing nodeDatasetMapping detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingnodeDatasetMappings);
                });
        }
        else {
            var error = {
                message: 'An error occurred - nodeDatasetMapping not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetByDomainModelGraphId: function (domainmodelgraphid) {
        var deferred = Q.defer();
        if (domainmodelgraphid) {
            //var objId = mongoose.Types.ObjectId(intentffmodelgraphid);
            NodeDatasetMapping.find({ "domainmodelgraphid": domainmodelgraphid },
                function (err, existingnodeDatasetMappings) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing nodeDatasetMapping detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingnodeDatasetMappings);
                });
        }
        else {
            var error = {
                message: 'An error occurred - domainmodelgraphid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetByDatasetId: function (datasetid) {
        var deferred = Q.defer();
        if (datasetid) {
            //var objId = mongoose.Types.ObjectId(intentffmodelgraphid);
            NodeDatasetMapping.find({ "datasetid": datasetid },
                function (err, existingnodeDatasetMappings) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing nodeDatasetMapping detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingnodeDatasetMappings);
                });
        }
        else {
            var error = {
                message: 'An error occurred - datasetid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },


    GetAll: function () {
        var deferred = Q.defer();
        NodeDatasetMapping.find({}, function (err, existingnodeDatasetMappings) {
            if (err) {
                deferred.reject({
                    message: 'An error occurred while fetcing nodeDatasetMappings',
                    error: err
                });
            }
            deferred.resolve(existingnodeDatasetMappings);
        });
        return deferred.promise;
    },

    Delete: function (nodeDatasetMappingid) {
        var deferred = Q.defer();
        if (nodeDatasetMappingid) {
            var objId = mongoose.Types.ObjectId(nodeDatasetMappingid);
            NodeDatasetMapping.findOne({ "_id": objId },
                function (err, existingnodeDatasetMapping) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetching nodeDatasetMapping detail',
                            error: err
                        });
                    }

                    if (!existingnodeDatasetMapping) {
                        deferred.reject({
                            message: 'nodeDatasetMapping not found',
                            error: {
                                "nodeDatasetMappingid": nodeDatasetMappingid,
                                "err": "nodeDatasetMapping not found"
                            }
                        });
                    }
                    else {

                        //remove NodeDatasetMapping here then callback
                        NodeDatasetMapping.remove({ "_id": objId }, function (remerr) {
                            if (remerr) {
                                deferred.reject({
                                    message: 'An error occurred while removing Node Dataset Mapping',
                                    error: err
                                });
                            }
                            else {
                                deferred.resolve(existingnodeDatasetMapping);
                            }
                        });
                    }
                });
        }
        else {
            var error = {
                message: 'An error occurred - nodeDatasetMappingid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }
}
