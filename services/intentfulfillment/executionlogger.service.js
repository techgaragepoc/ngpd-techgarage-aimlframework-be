const mongoose = require('mongoose');
const { IntentffModelExecutionResult } = require('../../models/intentfulfillment/');
const Utility = require('../../lib/utility');
const Q = require('q');
const uuid = require('uuid/v4');

class ExecutionLogger {
    constructor() {
        this.sessionid = '';
    }

    Initialize() {
        var deferred = Q.defer();
        var sessionid = uuid();
        var execResultStart = new IntentffModelExecutionResult({
            sessionid: sessionid,
            level: 0,
            message:
            {
                "step": "Step 0",
                "message": "Session Initiated..." + sessionid
            }

        });

        execResultStart.save(function (err, data) {
            if (err) {
                deferred.reject(err);
            }
            else {
                deferred.resolve(sessionid);
            }
        });

        return deferred.promise;
    }

    Log(sessionid, logmessage, level=0) {

        var execResultStart = new IntentffModelExecutionResult({
            sessionid: sessionid,
            level: level,
            message: logmessage
        });

        execResultStart.save(function (err, data) {
            if (err) {
                console.log('Error while adding log...', err);
            }
            else {
               // console.log('Log has been added for session...', sessionid);
            }
        });


    }

    GetLog(sessionid, level=0) {
        var deferred = Q.defer();

        IntentffModelExecutionResult.find({ sessionid: sessionid, level: {$lte: level} }).sort({lastmodifiedon: 1})
            .exec(function(err, messages) {
            if (err) {
                console.log('Error while updating result...finding the session ' + sessionid);
                deferred.reject(err);
            }
            else {
                deferred.resolve(messages);
            }
        });

        return deferred.promise;
    }
}

module.exports = new ExecutionLogger();