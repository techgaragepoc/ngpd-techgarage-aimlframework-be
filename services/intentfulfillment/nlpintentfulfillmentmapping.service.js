const mongoose = require('mongoose');
const { NLPIntentFulfillmentMapping } = require('../../models/intentfulfillment/');
var Q = require('q');

module.exports = {

    Add: function (nlpIntentfulfillmentMapping) {
        var deferred = Q.defer();

        if (nlpIntentfulfillmentMapping != null && typeof nlpIntentfulfillmentMapping != 'undefined') {
            // console.log('nlpIntentfulfillmentMapping =>', nlpIntentfulfillmentMapping);
            this.IsMappingExists(nlpIntentfulfillmentMapping.nlpmodelid,
                nlpIntentfulfillmentMapping.nlpintentid,
                nlpIntentfulfillmentMapping.intentfulfillmentmodelgraphid).then(
                    existingnlpIntentfulfillmentMapping => {
                        // console.log('existing mapping =>', existingnlpIntentfulfillmentMapping)
                        //if not found
                        if (existingnlpIntentfulfillmentMapping.length <= 0) {

                            var newnlpIntentfulfillmentMapping = new NLPIntentFulfillmentMapping({
                                nlpmodelid: nlpIntentfulfillmentMapping.nlpmodelid,
                                nlpintentid: nlpIntentfulfillmentMapping.nlpintentid,
                                intentfulfillmentmodelgraphid: nlpIntentfulfillmentMapping.intentfulfillmentmodelgraphid,
                                createdby: nlpIntentfulfillmentMapping.createdby.toLowerCase(),
                            });

                            newnlpIntentfulfillmentMapping.save(function (err, result) {
                                if (err) {
                                    deferred.reject({
                                        message: "An Error has occured while adding a new NLP Intentfulfillment Mapping",
                                        error: err
                                    });
                                } else {
                                    deferred.resolve(result);
                                }
                            });
                        }
                        else {
                            deferred.resolve(nlpIntentfulfillmentMapping);
                        }
                    },
                    fnderr => {
                        deferred.reject({
                            message: "An Error has occured while checking existance before adding NLP Intentfulfillment Mapping",
                            error: fnderr
                        });
                    });
        }
        else {
            var error = {
                message: 'An error occurred - NLP Intentfulfillment Mapping not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Update: function (nlpIntentfulfillmentMapping) {
        var deferred = Q.defer();
        if (nlpIntentfulfillmentMapping) {
            var objId = mongoose.Types.ObjectId(nlpIntentfulfillmentMapping._id);
            NLPIntentFulfillmentMapping.findOne({ "_id": objId }, function (err, existingnlpIntentfulfillmentMapping) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a NLP Intentfulfillment Mapping detail',
                        error: err
                    });
                }

                if (!existingnlpIntentfulfillmentMapping) {
                    deferred.reject({
                        message: 'nlpIntentfulfillmentMapping not found',
                        error: { "_id": nlpIntentfulfillmentMapping._id, "err": "NLP Intentfulfillment Mapping not found" }
                    });
                }
                else {

                    NLPIntentFulfillmentMapping.update({ "_id": objId },
                        {
                            $set: {
                                nlpmodelid: nlpIntentfulfillmentMapping.nlpmodelid,
                                nlpintentid: nlpIntentfulfillmentMapping.nlpintentid,
                                intentfulfillmentmodelgraphid: nlpIntentfulfillmentMapping.intentfulfillmentmodelgraphid,
                                lastmodifiedby: nlpIntentfulfillmentMapping.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating NLP Intentfulfillment Mapping details",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - NLP Intentfulfillment Mapping id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    UpdateImplementationStatus: function (nlpIntentfulfillmentMapping) {
        var deferred = Q.defer();
        if (nlpIntentfulfillmentMapping) {

            NLPIntentFulfillmentMapping.findOne({ "intentfulfillmentmodelgraphid": nlpIntentfulfillmentMapping.intentfulfillmentmodelgraphid }, function (err, existingnlpIntentfulfillmentMapping) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a NLP Intentfulfillment Mapping detail - implementation status',
                        error: err
                    });
                }

                if (!existingnlpIntentfulfillmentMapping) {
                    deferred.reject({
                        message: 'nlpIntentfulfillmentMapping not found',
                        error: { "nlpIntentfulfillmentMapping": nlpIntentfulfillmentMapping, "err": "NLP Intentfulfillment Mapping not found - implementation status" }
                    });
                }
                else {

                    NLPIntentFulfillmentMapping.update({ "intentfulfillmentmodelgraphid": nlpIntentfulfillmentMapping.intentfulfillmentmodelgraphid },
                        {
                            $set: {
                                implementationstatus: nlpIntentfulfillmentMapping.implementationstatus,
                                lastmodifiedby: nlpIntentfulfillmentMapping.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating NLP Intentfulfillment Mapping details - implementation status",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - NLP Intentfulfillment Mapping id not defined - implementation status',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Get: function (nlpIntentfulfillmentMappingid) {
        var deferred = Q.defer();
        if (nlpIntentfulfillmentMappingid) {
            try {
                var objId = mongoose.Types.ObjectId(nlpIntentfulfillmentMappingid);
                NLPIntentFulfillmentMapping.findOne({ "_id": objId }, function (err, existingnlpIntentfulfillmentMapping) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing NLP Intentfulfillment Mapping detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingnlpIntentfulfillmentMapping);
                });
            }
            catch (ex) {
                deferred.reject({
                    message: 'An error occurred while fetcing NLP Intentfulfillment Mapping detail, nlpIntentfulfillment Mapping id is not valid',
                    error: ex
                });
            }
        }
        else {
            var error = {
                message: 'An error occurred - nlpIntentfulfillmentMappingid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetByNLP: function (nlpdetail) {
        var deferred = Q.defer();
        if (nlpdetail) {
            NLPIntentFulfillmentMapping.find({
                "nlpmodelid": nlpdetail.model,
                "nlpintentid": nlpdetail.intent
            },
                function (err, existingnlpIntentfulfillmentMapping) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing nlpIntentfulfillmentMapping detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingnlpIntentfulfillmentMapping);
                });
        }
        else {
            var error = {
                message: 'An error occurred - nlpdetail not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetByIntentFulfillmentModelGraphId: function (intentffmodelgraphid) {
        var deferred = Q.defer();
        if (intentffmodelgraphid) {
            //  var objId = mongoose.Types.ObjectId(intentffmodelgraphid);
            NLPIntentFulfillmentMapping.find({ "intentfulfillmentmodelgraphid": intentffmodelgraphid },
                function (err, existingnlpIntentfulfillmentMapping) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing nlpIntentfulfillmentMapping detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingnlpIntentfulfillmentMapping);
                });
        }
        else {
            var error = {
                message: 'An error occurred - nlpdetail not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    IsMappingExists: function (nlpmodelid, nlpintentid, intentffmodelgraphid) {
        var deferred = Q.defer();

        NLPIntentFulfillmentMapping.find({
            "intentfulfillmentmodelgraphid": intentffmodelgraphid,
            "nlpmodelid": nlpmodelid,
            "nlpintentid": nlpintentid
        },
            function (err, mapping) {
                if (err) {
                    console.log('err =>', err)
                    deferred.reject({
                        message: 'An error occurred while fetcing nlpIntentfulfillmentMapping detail',
                        error: err
                    });
                }

                deferred.resolve(mapping);
            });

        return deferred.promise;
    },

    GetAll: function () {
        var deferred = Q.defer();
        NLPIntentFulfillmentMapping.find({}, function (err, existingnlpIntentfulfillmentMappings) {
            if (err) {
                deferred.reject({
                    message: 'An error occurred while fetcing nlpIntentfulfillmentMappings',
                    error: err
                });
            }
            deferred.resolve(existingnlpIntentfulfillmentMappings);
        });
        return deferred.promise;
    },

    Delete: function (nlpIntentfulfillmentMappingid) {
        var deferred = Q.defer();
        if (nlpIntentfulfillmentMappingid) {
            var objId = mongoose.Types.ObjectId(nlpIntentfulfillmentMappingid);
            NLPIntentFulfillmentMapping.findOne({ "_id": objId },
                function (err, existingnlpIntentfulfillmentMapping) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetching nlpIntentfulfillmentMapping detail',
                            error: err
                        });
                    }

                    if (!existingnlpIntentfulfillmentMapping) {
                        deferred.reject({
                            message: 'nlpIntentfulfillmentMapping not found',
                            error: {
                                "nlpIntentfulfillmentMappingid": nlpIntentfulfillmentMappingid,
                                "err": "nlpIntentfulfillmentMapping not found"
                            }
                        });
                    }
                    else {

                        //remove NLPIntentFulfillmentMapping here then callback
                        NLPIntentFulfillmentMapping.remove({ "_id": objId }, function (remerr) {
                            if (remerr) {
                                deferred.reject({
                                    message: 'An error occurred while removing NLP Intentfulfillment Mapping',
                                    error: err
                                });
                            }
                            else {
                                deferred.resolve(existingnlpIntentfulfillmentMapping);
                            }
                        });
                    }
                });
        }
        else {
            var error = {
                message: 'An error occurred - nlpIntentfulfillmentMappingid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }
}
