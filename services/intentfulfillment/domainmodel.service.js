const mongoose = require('mongoose');
const { DomainModel } = require('../../models/intentfulfillment/');
const TransformGraphJSONHandler = require('../../lib/transformgraphmodel.handler');

const Q = require('q');

class DomainModelService {

    Create(domainmodel) {
        var deferred = Q.defer();
        if (domainmodel && domainmodel.name) {

            this.GetByName(domainmodel.name).then(
                existingdomainmodel => {
                    //if not found
                    if (!existingdomainmodel) {

                        var newdomainmodel = new DomainModel({
                            modelgraphid: domainmodel.modelgraphid,
                            name: domainmodel.name,
                            nodes: domainmodel.nodes,
                            nodeproperties: domainmodel.nodeproperties,
                            createdby: domainmodel.createdby.toLowerCase(),
                        });

                        newdomainmodel.save(function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while creating a new domain model",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                    }
                    else {  //else throw error - already exists
                        deferred.reject({
                            message: "Domain Model already exists!!",
                            error: existingdomainmodel
                        });
                    }
                },
                fnderr => {
                    deferred.reject({
                        message: "An Error has occured while checking existance before creating domainmodel",
                        error: fnderr
                    });
                });
        }
        else {
            var error = {
                message: 'An error occurred - domain model not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }

    Update(domainmodel) {
        var deferred = Q.defer();
        if (domainmodel) {
            // var objId = mongoose.Types.ObjectId(domainmodel._id);
            DomainModel.findOne({ "modelgraphid": domainmodel.modelgraphid }, function (err, existingdomainmodel) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a domain model detail',
                        error: err
                    });
                }

                if (!existingdomainmodel) {
                    deferred.reject({
                        message: 'domain model not found',
                        error: { modelgraphid: domainmodel.modelgraphid, "err": "domain model not found" }
                    });
                }
                else {

                    DomainModel.update({ "modelgraphid": domainmodel.modelgraphid },
                        {
                            $set: {
                                modelgraphid: domainmodel.modelgraphid,
                                name: domainmodel.name,
                                nodes: domainmodel.nodes,
                                nodeproperties: domainmodel.nodeproperties,
                                lastmodifiedby: domainmodel.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating domain model details",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - domain model id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }

    Get(domainmodelid) {
        var deferred = Q.defer();
        if (domainmodelid) {
            try {
                var objId = mongoose.Types.ObjectId(domainmodelid);
                DomainModel.findOne({ "_id": objId }, function (err, existingdomainmodel) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing domain model detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingdomainmodel);

                });
            }
            catch (ex) {
                deferred.reject({
                    message: 'An error occurred while fetcing domain model detail, domainmodel id is invalid',
                    error: ex
                });
            }
        }
        else {
            var error = {
                message: 'An error occurred - domainmodelid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }

    GetByGraphId(domainmodelgraphid) {
        var deferred = Q.defer();
        if (domainmodelid) {
            //var objId = mongoose.Types.ObjectId(domainmodelgraphid);
            DomainModel.findOne({ "modelgraphid": domainmodelgraphid }, function (err, existingdomainmodel) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while fetcing domain model detail',
                        error: err
                    });
                }
                deferred.resolve(existingdomainmodel);

            });
        }
        else {
            var error = {
                message: 'An error occurred - domainmodelid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }

    GetByName(domainmodelname) {
        var deferred = Q.defer();
        if (domainmodelname) {
            DomainModel.findOne({ "name": { $regex: new RegExp("^" + domainmodelname.toLowerCase() + "$", "i") } },
                function (err, existingdomainmodel) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing domain model detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingdomainmodel);
                });
        }
        else {
            var error = {
                message: 'An error occurred - domain model name not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }

    GetAll() {
        var deferred = Q.defer();
        DomainModel.find({}, function (err, existingdomainmodels) {
            if (err) {
                deferred.reject({
                    message: 'An error occurred while fetcing domain models',
                    error: err
                });
            }
            deferred.resolve(existingdomainmodels);
        });
        return deferred.promise;
    }

    Delete(domainmodelid) {
        var deferred = Q.defer();
        if (domainmodelid) {
            var objId = mongoose.Types.ObjectId(domainmodelid);
            DomainModel.findOne({ "_id": objId },
                function (err, existingdomainmodel) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetching domain model detail',
                            error: err
                        });
                    }

                    if (!existingdomainmodel) {
                        deferred.reject({
                            message: 'domain model not found',
                            error: {
                                "_id": domainmodelid,
                                "err": "domain model not found"
                            }
                        });
                    }
                    else {

                        //remove DomainModel here then callback
                        DomainModel.remove({ "_id": objId }, function (remerr) {
                            if (remerr) {
                                deferred.reject({
                                    message: 'An error occurred while removing domain model type',
                                    error: err
                                });
                            }
                            else {
                                deferred.resolve(existingdomainmodel);
                            }
                        });
                    }
                });
        }
        else {
            var error = {
                message: 'An error occurred - domain model id not defined',
                error: null
            };
            deferred.reject(error);
        }

        return deferred.promise;
    }

    TransformedJSON(graphdata) {
        var deferred = Q.defer();

        TransformGraphJSONHandler.GetDomainModel(graphdata).then(
            resultjson => { deferred.resolve(resultjson); },
            err => {
                console.log('Error occured while transforming domain model graph json...');
                deferred.reject({ error: err, message: 'Error occured while transforming domain model graph json...' });
            }
        );

        return deferred.promise;
    }
}

module.exports = new DomainModelService();
