const mongoose = require('mongoose');
const { Functions } = require('../../models/intentfulfillment/');
var Q = require('q');

module.exports = {

    Add: function (aggregatefunction) {
        var deferred = Q.defer();
        if (aggregatefunction && aggregatefunction.code) {

            this.GetByCode(aggregatefunction.code).then(
                existingaggregatefunction => {


                    //if not found
                    if (!existingaggregatefunction) {

                        var newaggregatefunction = new Functions({
                            code: aggregatefunction.code,
                            name: aggregatefunction.name,
                            synonyms: aggregatefunction.synonyms,
                            implementation: aggregatefunction.implementation,
                            createdby: aggregatefunction.createdby.toLowerCase(),
                        });

                        newaggregatefunction.save(function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while adding a new aggregate function",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                    }
                    else {  //else throw error - already exists
                        deferred.reject({
                            message: "Function of same code already exists!!",
                            error: existingaggregatefunction
                        });
                    }
                },
                fnderr => {
                    deferred.reject({
                        message: "An Error has occured while checking existance before adding aggregate function",
                        error: fnderr
                    });
                });
        }
        else {
            var error = {
                message: 'An error occurred - aggregate function not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Update: function (aggregatefunction) {
        var deferred = Q.defer();
        if (aggregatefunction) {
            var objId = mongoose.Types.ObjectId(aggregatefunction._id);
            Functions.findOne({ "_id": objId }, function (err, existingaggregatefunction) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a aggregate function detail',
                        error: err
                    });
                }

                if (!existingaggregatefunction) {
                    deferred.reject({
                        message: 'aggregatefunction not found',
                        error: { "_id": aggregatefunction._id, "err": "aggregate function not found" }
                    });
                }
                else {

                    Functions.update({ "_id": objId },
                        {
                            $set: {
                                code: aggregatefunction.code,
                                name: aggregatefunction.name,
                                synonyms: aggregatefunction.synonyms,
                                implementation: aggregatefunction.implementation,
                                lastmodifiedby: aggregatefunction.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating aggregate function details",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - aggregate function id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Get: function (aggregatefunctionid) {
        var deferred = Q.defer();
        if (aggregatefunctionid) {
            try {
                var objId = mongoose.Types.ObjectId(aggregatefunctionid);
                Functions.findOne({ "_id": objId }, function (err, existingaggregatefunction) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing aggregate function detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingaggregatefunction);
                });
            }
            catch (ex) {
                deferred.reject({
                    message: 'An error occurred while fetcing aggregate function detail, aggregate function id is not valid',
                    error: ex
                });
            }
        }
        else {
            var error = {
                message: 'An error occurred - aggregatefunctionid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetByCode: function (aggregatefunctioncode) {
        var deferred = Q.defer();
        if (aggregatefunctioncode) {
            Functions.findOne({ "code": { $regex: new RegExp("^" + aggregatefunctioncode.toLowerCase() + "$", "i") } },
                function (err, existingaggregatefunction) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing aggregatefunction detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingaggregatefunction);
                });
        }
        else {
            var error = {
                message: 'An error occurred - aggregatefunctionname not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetByName: function (aggregatefunctionname) {
        var deferred = Q.defer();
        if (aggregatefunctionname) {
            Functions.findOne({ "name": { $regex: new RegExp("^" + aggregatefunctionname.toLowerCase() + "$", "i") } },
                function (err, existingaggregatefunction) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing aggregatefunction detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingaggregatefunction);
                });
        }
        else {
            var error = {
                message: 'An error occurred - aggregatefunctionname not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetAll: function () {
        var deferred = Q.defer();
        Functions.find({}, function (err, existingaggregatefunctions) {
            if (err) {
                deferred.reject({
                    message: 'An error occurred while fetcing aggregatefunctions',
                    error: err
                });
            }
            deferred.resolve(existingaggregatefunctions);
        });
        return deferred.promise;
    },

    Delete: function (aggregatefunctionid) {
        var deferred = Q.defer();
        if (aggregatefunctionid) {
            var objId = mongoose.Types.ObjectId(aggregatefunctionid);
            Functions.findOne({ "_id": objId },
                function (err, existingaggregatefunction) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetching aggregatefunction detail',
                            error: err
                        });
                    }

                    if (!existingaggregatefunction) {
                        deferred.reject({
                            message: 'aggregatefunction not found',
                            error: {
                                "aggregatefunctionid": aggregatefunctionid,
                                "err": "aggregatefunction not found"
                            }
                        });
                    }
                    else {

                        //remove Functions here then callback
                        Functions.remove({ "_id": objId }, function (remerr) {
                            if (remerr) {
                                deferred.reject({
                                    message: 'An error occurred while removing aggregate function',
                                    error: err
                                });
                            }
                            else {
                                deferred.resolve(existingaggregatefunction);
                            }
                        });
                    }
                });
        }
        else {
            var error = {
                message: 'An error occurred - aggregatefunctionid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }
}
