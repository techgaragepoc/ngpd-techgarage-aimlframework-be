const mongoose = require('mongoose');
const { Synonyms } = require('../../models/intentfulfillment/');
var Q = require('q');

module.exports = {

    Add: function (synonyms) {
        var deferred = Q.defer();
        if (synonyms && synonyms.refid) {

            this.GetByRefId(synonyms.refid).then(
                existingsynonyms => {
                    //if not found
                    if (!existingsynonyms) {

                        var newsynonyms = new Synonyms({
                            refid: synonyms.refid,
                            synonyms: synonyms.synonyms,
                            createdby: synonyms.createdby.toLowerCase(),
                        });

                        newsynonyms.save(function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while adding a new synonyms",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                    }
                    else {  //else throw error - already exists
                        deferred.reject({
                            message: "Synonym RefId already exists!!",
                            error: existingsynonyms
                        });
                    }
                },
                fnderr => {
                    deferred.reject({
                        message: "An Error has occured while checking existance before adding synonyms",
                        error: fnderr
                    });
                });
        }
        else {
            var error = {
                message: 'An error occurred - synonyms not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Update: function (synonyms) {
        var deferred = Q.defer();
        if (synonyms) {
            var objId = mongoose.Types.ObjectId(synonyms._id);
            Synonyms.findOne({ "_id": objId }, function (err, existingsynonyms) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a synonyms detail',
                        error: err
                    });
                }

                if (!existingsynonyms) {
                    deferred.reject({
                        message: 'synonyms not found',
                        error: { "_id": synonyms._id, "err": "synonyms not found" }
                    });
                }
                else {

                    Synonyms.update({ "_id": objId },
                        {
                            $set: {
                                refid: synonyms.refid,
                                synonyms: synonyms.synonyms,
                                lastmodifiedby: synonyms.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating synonyms details",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - synonyms id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Get: function (synonymsid) {
        var deferred = Q.defer();
        if (synonymsid) {
            try {
                var objId = mongoose.Types.ObjectId(synonymsid);
                Synonyms.findOne({ "_id": objId }, function (err, existingsynonyms) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing synonyms detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingsynonyms);
                });
            }
            catch (ex) {
                deferred.reject({
                    message: 'An error occurred while fetcing synonyms detail, synonymsid is not valid',
                    error: ex
                });
            }
        }
        else {
            var error = {
                message: 'An error occurred - synonymsid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetByRefId: function (synonymsrefid) {
        var deferred = Q.defer();

        if (synonymsrefid) {
            Synonyms.findOne({ "name": { $regex: new RegExp("^" + synonymsrefid.toLowerCase() + "$", "i") } },
                function (err, existingsynonyms) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing synonyms detail',
                            error: err
                        });
                    }

                    deferred.resolve(existingsynonyms);
                });
        }
        else {
            var error = {
                message: 'An error occurred - synonymsrefid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetAll: function () {
        var deferred = Q.defer();
        Synonyms.find({}, function (err, existingsynonymss) {
            if (err) {
                deferred.reject({
                    message: 'An error occurred while fetcing synonymss',
                    error: err
                });
            }
            deferred.resolve(existingsynonymss);
        });
        return deferred.promise;
    },

    Delete: function (synonymsid) {
        var deferred = Q.defer();
        if (synonymsid) {
            var objId = mongoose.Types.ObjectId(synonymsid);
            Synonyms.findOne({ "_id": objId },
                function (err, existingsynonyms) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetching synonyms detail',
                            error: err
                        });
                    }

                    if (!existingsynonyms) {
                        deferred.reject({
                            message: 'synonyms not found',
                            error: {
                                "synonymsid": synonymsid,
                                "err": "synonyms not found"
                            }
                        });
                    }
                    else {

                        //remove Synonyms here then callback
                        Synonyms.remove({ "_id": objId }, function (remerr) {
                            if (remerr) {
                                deferred.reject({
                                    message: 'An error occurred while removing synonyms',
                                    error: err
                                });
                            }
                            else {
                                deferred.resolve(existingsynonyms);
                            }
                        });
                    }
                });
        }
        else {
            var error = {
                message: 'An error occurred - synonymsid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }
}
