const mongoose = require('mongoose');
const { IntentFulfillmentModelGraph } = require('../../models/intentfulfillment/');
var Q = require('q');

module.exports = {

    Create: function (intentfulfillmentmodelgraph) {
        var deferred = Q.defer();

        if (intentfulfillmentmodelgraph && intentfulfillmentmodelgraph.name) {

            this.GetByName(intentfulfillmentmodelgraph.name).then(existingintentfulfillmentmodelgraph => {
                //console.log('existingintentfulfillmentmodelgraph =>', existingintentfulfillmentmodelgraph);
                //if not found
                if (existingintentfulfillmentmodelgraph == null || existingintentfulfillmentmodelgraph == 'undefined') {

                    var newintentfulfillmentmodelgraph = new IntentFulfillmentModelGraph({
                        name: intentfulfillmentmodelgraph.name,
                        pos: intentfulfillmentmodelgraph.pos,
                        graphjson: intentfulfillmentmodelgraph.graphjson,
                        nodeproperties: intentfulfillmentmodelgraph.nodeproperties,
                        createdby: intentfulfillmentmodelgraph.createdby.toLowerCase(),
                    });

                    newintentfulfillmentmodelgraph.save(function (err, result) {
                        if (err) {
                            deferred.reject({
                                message: "An Error has occured while creating a new intentfulfillment model",
                                error: err
                            });

                        } else {
                            deferred.resolve(result);
                        }
                    });
                }
                else {  //else throw error - already exists
                    deferred.reject({
                        message: "IntentFulfillment Model Graph already exists!!",
                        error: existingintentfulfillmentmodelgraph
                    });
                }

            },
                fnderr => {
                    deferred.reject({
                        message: "An Error has occured while checking existance before creating intentfulfillment model",
                        error: fnderr
                    });
                }); // GetByName

        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillment model not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Update: function (intentfulfillmentmodelgraph) {
        var deferred = Q.defer();
        if (intentfulfillmentmodelgraph) {
            var objId = mongoose.Types.ObjectId(intentfulfillmentmodelgraph._id);
            IntentFulfillmentModelGraph.findOne({ "_id": objId }, function (err, existingintentfulfillmentmodelgraph) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a intentfulfillment model detail',
                        error: err
                    });
                }

                if (!existingintentfulfillmentmodelgraph) {
                    deferred.reject({
                        message: 'intentfulfillment model not found',
                        error: { intentfulfillmentmodelgraphid: intentfulfillmentmodelgraph._id, "err": "intentfulfillment model not found" }
                    });
                }
                else {

                    IntentFulfillmentModelGraph.update({ "_id": objId },
                        {
                            $set: {
                                name: intentfulfillmentmodelgraph.name,
                                pos: intentfulfillmentmodelgraph.pos,
                                graphjson: intentfulfillmentmodelgraph.graphjson,
                                nodeproperties: intentfulfillmentmodelgraph.nodeproperties,
                                lastmodifiedby: intentfulfillmentmodelgraph.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating intentfulfillment model details",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillment model id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Get: function (intentfulfillmentmodelgraphid) {
        var deferred = Q.defer();
        if (intentfulfillmentmodelgraphid && typeof intentfulfillmentmodelgraphid != 'undefined') {
            try {
                var objId = mongoose.Types.ObjectId(intentfulfillmentmodelgraphid);

                IntentFulfillmentModelGraph.findOne({ "_id": objId }, function (err, existingintentfulfillmentmodelgraph) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing intentfulfillment model detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingintentfulfillmentmodelgraph);
                });
            }
            catch (ex) {
                deferred.reject({
                    message: 'An error occurred while fetcing intentfulfillment model detail, graphid is invalid',
                    error: ex
                });
            }
        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillmentmodelgraphid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetByName: function (intentfulfillmentmodelgraphname) {
        var deferred = Q.defer();
        if (intentfulfillmentmodelgraphname) {
            IntentFulfillmentModelGraph.findOne({ "name": { $regex: new RegExp("^" + intentfulfillmentmodelgraphname.toLowerCase() + "$", "i") } },
                function (err, existingintentfulfillmentmodelgraph) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing intentfulfillment model detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingintentfulfillmentmodelgraph);
                });
        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillment model name not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetAll: function () {
        var deferred = Q.defer();
        IntentFulfillmentModelGraph.find({}, function (err, existingintentfulfillmentmodelgraphs) {
            if (err) {
                deferred.reject({
                    message: 'An error occurred while fetcing intentfulfillment models',
                    error: err
                });
            }
            deferred.resolve(existingintentfulfillmentmodelgraphs);
        });
        return deferred.promise;
    },

    Delete: function (intentfulfillmentmodelgraphid) {
        var deferred = Q.defer();
        if (intentfulfillmentmodelgraphid) {
            var objId = mongoose.Types.ObjectId(intentfulfillmentmodelgraphid);
            IntentFulfillmentModelGraph.findOne({ "_id": objId },
                function (err, existingintentfulfillmentmodelgraph) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetching intentfulfillment model detail',
                            error: err
                        });
                    }

                    if (!existingintentfulfillmentmodelgraph) {
                        deferred.reject({
                            message: 'intentfulfillment model not found',
                            error: {
                                "_id": intentfulfillmentmodelgraphid,
                                "err": "intentfulfillment model not found"
                            }
                        });
                    }
                    else {

                        //remove IntentFulfillmentModelGraph here then callback
                        IntentFulfillmentModelGraph.remove({ "_id": objId }, function (remerr) {
                            if (remerr) {
                                deferred.reject({
                                    message: 'An error occurred while removing intentfulfillment model type',
                                    error: err
                                });
                            }
                            else {
                                deferred.resolve(existingintentfulfillmentmodelgraph);
                            }
                        });
                    }
                });
        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillment model id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }
}
