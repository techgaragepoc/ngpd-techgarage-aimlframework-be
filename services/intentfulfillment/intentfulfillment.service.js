const mongoose = require('mongoose');
const { IntentFulfillmentModel } = require('../../models/intentfulfillment/');
const TransformGraphJSONHandler = require('../../lib/transformgraphmodel.handler');

const Q = require('q');

module.exports = {

    Create: function (intentfulfillmentmodel) {
        var deferred = Q.defer();
        if (intentfulfillmentmodel && intentfulfillmentmodel.name) {
            this.GetByName(intentfulfillmentmodel.name).then(
                existingintentfulfillmentmodel => {
                    //if not found
                    if (!existingintentfulfillmentmodel) {

                        var newintentfulfillmentmodel = new IntentFulfillmentModel({
                            modelgraphid: intentfulfillmentmodel.modelgraphid,
                            name: intentfulfillmentmodel.name,
                            pos: intentfulfillmentmodel.pos,
                            details: intentfulfillmentmodel.details,
                            nodes: intentfulfillmentmodel.nodes,
                            nodeproperties: intentfulfillmentmodel.nodeproperties,
                            createdby: intentfulfillmentmodel.createdby.toLowerCase(),
                        });

                        newintentfulfillmentmodel.save(function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while creating a new intentfulfillment model",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                    }
                    else {  //else throw error - already exists
                        deferred.reject({
                            message: "Intentfulfillment Model already exists!!",
                            error: existingintentfulfillmentmodel
                        });
                    }
                },
                fnderr => {
                    deferred.reject({
                        message: "An Error has occured while checking existance before creating intentfulfillment model",
                        error: fnderr
                    });
                });
        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillment model not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Update: function (intentfulfillmentmodel) {
        var deferred = Q.defer();
        if (intentfulfillmentmodel) {
            // var objId = mongoose.Types.ObjectId(intentfulfillmentmodel.modelgraphid);
            IntentFulfillmentModel.findOne({ "modelgraphid": intentfulfillmentmodel.modelgraphid }, function (err, existingintentfulfillmentmodel) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a intentfulfillment model detail',
                        error: err
                    });
                }

                if (!existingintentfulfillmentmodel) {
                    deferred.reject({
                        message: 'intentfulfillment model not found',
                        error: { "modelgraphid": intentfulfillmentmodel.modelgraphid, "err": "intentfulfillment model not found" }
                    });
                }
                else {

                    IntentFulfillmentModel.update({ "modelgraphid": intentfulfillmentmodel.modelgraphid },
                        {
                            $set: {
                                modelgraphid: intentfulfillmentmodel.modelgraphid,
                                name: intentfulfillmentmodel.name,
                                pos: intentfulfillmentmodel.pos,
                                details: intentfulfillmentmodel.details,
                                nodes: intentfulfillmentmodel.nodes,
                                // nodeproperties: intentfulfillmentmodel.nodeproperties,
                                lastmodifiedby: intentfulfillmentmodel.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating intentfulfillment model details",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillment model id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    UpdateNodeProperty: function (intentfulfillmentmodel) {
        var deferred = Q.defer();
        if (intentfulfillmentmodel) {
            // var objId = mongoose.Types.ObjectId(intentfulfillmentmodel.modelgraphid);
            IntentFulfillmentModel.findOne({ "modelgraphid": intentfulfillmentmodel.modelgraphid }, function (err, existingintentfulfillmentmodel) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a intentfulfillment model detail',
                        error: err
                    });
                }

                if (!existingintentfulfillmentmodel) {
                    deferred.reject({
                        message: 'intentfulfillment model not found',
                        error: { "modelgraphid": intentfulfillmentmodel.modelgraphid, "err": "intentfulfillment model not found" }
                    });
                }
                else {

                    IntentFulfillmentModel.update({ "modelgraphid": intentfulfillmentmodel.modelgraphid },
                        {
                            $set: {
                                nodeproperties: intentfulfillmentmodel.nodeproperties,
                                lastmodifiedby: intentfulfillmentmodel.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating intentfulfillment model details",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillment model id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Get: function (intentfulfillmentmodelid) {
        var deferred = Q.defer();
        if (intentfulfillmentmodelid) {
            try {
                var objId = mongoose.Types.ObjectId(intentfulfillmentmodelid);
                IntentFulfillmentModel.findOne({ "_id": objId }, function (err, existingintentfulfillmentmodel) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing intentfulfillment model detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingintentfulfillmentmodel);
                });
            }
            catch (ex) {
                deferred.reject({
                    message: 'An error occurred while fetcing intentfulfillment model detail, model id is invalid',
                    error: ex
                });
            }
        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillmentmodelid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;

    },

    GetByGraphId: function (intentfulfillmentgraphmodelid) {
        var deferred = Q.defer();
        if (intentfulfillmentgraphmodelid) {
            //var objId = mongoose.Types.ObjectId(intentfulfillmentgraphmodelid);
            IntentFulfillmentModel.findOne({ "modelgraphid": intentfulfillmentgraphmodelid },
                function (err, existingintentfulfillmentmodel) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing intentfulfillment model detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingintentfulfillmentmodel);
                });
        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillmentmodelid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetByName: function (intentfulfillmentmodelname) {
        var deferred = Q.defer();
        if (intentfulfillmentmodelname) {
            IntentFulfillmentModel.findOne({ "name": { $regex: new RegExp("^" + intentfulfillmentmodelname.toLowerCase() + "$", "i") } },
                function (err, existingintentfulfillmentmodel) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing intentfulfillment model detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingintentfulfillmentmodel);
                });
        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillment model name not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetAll: function () {
        console.log('getall=>');
        var deferred = Q.defer();
        IntentFulfillmentModel.find({}, function (err, existingintentfulfillmentmodels) {
            if (err) {
                deferred.reject({
                    message: 'An error occurred while fetcing intentfulfillment models',
                    error: err
                });
            }
            deferred.resolve(existingintentfulfillmentmodels);
        });
        return deferred.promise;
    },

    Delete: function (intentfulfillmentmodelid) {
        var deferred = Q.defer();
        if (intentfulfillmentmodelid) {
            var objId = mongoose.Types.ObjectId(intentfulfillmentmodelid);
            IntentFulfillmentModel.findOne({ "_id": objId },
                function (err, existingintentfulfillmentmodel) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetching intentfulfillment model detail',
                            error: err
                        });
                    }

                    if (!existingintentfulfillmentmodel) {
                        deferred.reject({
                            message: 'intentfulfillment model not found',
                            error: {
                                "_id": intentfulfillmentmodelid,
                                "err": "intentfulfillment model not found"
                            }
                        });
                    }
                    else {

                        //remove IntentFulfillmentModel here then callback
                        IntentFulfillmentModel.remove({ "_id": objId }, function (remerr) {
                            if (remerr) {
                                deferred.reject({
                                    message: 'An error occurred while removing intentfulfillment model type',
                                    error: err
                                });
                            }
                            else {
                                deferred.resolve(existingintentfulfillmentmodel);
                            }
                        });

                    }
                });
        }
        else {
            var error = {
                message: 'An error occurred - intentfulfillment model id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    TransformedJSON: function (graphdata) {
        var deferred = Q.defer();

        TransformGraphJSONHandler.GetIntentffModel(graphdata).then(
            resultjson => { deferred.resolve(resultjson); },
            err => {
                console.log('Error occured while transforming intentff model graph json...');
                deferred.reject({ error: err, message: 'Error occured while transforming intentff model graph json...' });
            }
        );

        return deferred.promise;
    }
}
