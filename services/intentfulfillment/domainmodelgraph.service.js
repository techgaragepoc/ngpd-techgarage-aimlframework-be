const mongoose = require('mongoose');
const { DomainModelGraph } = require('../../models/intentfulfillment/');
var Q = require('q');

module.exports = {

    Create: function (domainmodelgraph) {
        var deferred = Q.defer();
        var self = this;
        if (domainmodelgraph && domainmodelgraph.name) {

            this.GetByName(domainmodelgraph.name).then(existingdomainmodelgraph => {
                //if not found
                if (!existingdomainmodelgraph) {

                    var newdomainmodelgraph = new DomainModelGraph({
                        name: domainmodelgraph.name,
                        iconpath: domainmodelgraph.iconpath,
                        graphjson: domainmodelgraph.graphjson,
                        nodeproperties: domainmodelgraph.nodeproperties,
                        createdby: domainmodelgraph.createdby.toLowerCase(),
                    });

                    newdomainmodelgraph.save(function (err, result) {
                        if (err) {
                            deferred.reject({
                                message: "An Error has occured while creating a new domain model graph",
                                error: err
                            });

                        } else {

                            //to update basekey with modelid
                            result["lastmodifiedby"] = result["createdby"];
                            var updatedmodel = self.UpdateBaseModelRef(result);
                            self.Update(updatedmodel).then(updresult => {
                                deferred.resolve(updatedmodel);
                            },
                                upderr => {
                                    deferred.reject({
                                        message: "An Error has occured while updating basemodeid in a new domain model graph",
                                        error: err
                                    });
                                })


                        }
                    });
                }
                else {  //else throw error - already exists
                    deferred.reject({
                        message: "Domain Model already exists!!",
                        error: existingdomainmodelgraph
                    });
                }
            },
                fnderr => {
                    deferred.reject({
                        message: "An Error has occured while checking existance before creating domainmodelgraph",
                        error: fnderr
                    });
                });
        }
        else {
            var error = {
                message: 'An error occurred - domain model graph not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Update: function (domainmodelgraph) {
        var deferred = Q.defer();
        var self = this;

        if (domainmodelgraph) {
            var objId = mongoose.Types.ObjectId(domainmodelgraph._id);
            DomainModelGraph.findOne({ "_id": objId }, function (err, existingdomainmodelgraph) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a domain model graph detail',
                        error: err
                    });
                }

                if (!existingdomainmodelgraph) {
                    deferred.reject({
                        message: 'domain model graph not found',
                        error: { domainmodelgraphid: domainmodelgraph._id, "err": "domain model graph not found" }
                    });
                }
                else {
                    //to update basekey with modelid
                    var updatedmodel = self.UpdateBaseModelRef(domainmodelgraph);

                    DomainModelGraph.update({ "_id": objId },
                        {
                            $set: {
                                name: updatedmodel.name,
                                iconpath: updatedmodel.iconpath,
                                graphjson: updatedmodel.graphjson,
                                nodeproperties: updatedmodel.nodeproperties,
                                lastmodifiedby: updatedmodel.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating domain model graph details",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - domain model graph id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },


    Get: function (domainmodelgraphid) {
        var deferred = Q.defer();
        if (domainmodelgraphid) {
            try {  
            var objId = mongoose.Types.ObjectId(domainmodelgraphid);
            DomainModelGraph.findOne({ "_id": objId }, function (err, existingdomainmodelgraph) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while fetcing domain model graph detail',
                        error: err
                    });
                }
                deferred.resolve(existingdomainmodelgraph);
            });
        }
        catch(ex) {
            deferred.reject({
                message: 'An error occurred while fetcing domain model graph detail, domainmodel graph id is not valid',
                error: err
            });
        }
        }
        else {
            var error = {
                message: 'An error occurred - domainmodelgraphid not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetByName: function (domainmodelgraphname) {
        var deferred = Q.defer();
        if (domainmodelgraphname) {
            DomainModelGraph.findOne({ "name": { $regex: new RegExp("^" + domainmodelgraphname.toLowerCase() + "$", "i") } },
                function (err, existingdomainmodelgraph) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing domain model graph detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingdomainmodelgraph);
                });
        }
        else {
            var error = {
                message: 'An error occurred - domain model graph name not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetAll: function () {
        var deferred = Q.defer();
        DomainModelGraph.find({}, function (err, existingdomainmodelgraphs) {
            if (err) {
                deferred.reject({
                    message: 'An error occurred while fetcing domain model graphs',
                    error: err
                });
            }
            deferred.resolve(existingdomainmodelgraphs);
        });
        return deferred.promise;
    },

    Delete: function (domainmodelgraphid) {
        var deferred = Q.defer();
        if (domainmodelgraphid) {
            var objId = mongoose.Types.ObjectId(domainmodelgraphid);
            DomainModelGraph.findOne({ "_id": objId },
                function (err, existingdomainmodelgraph) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetching domain model graph detail',
                            error: err
                        });
                    }

                    if (!existingdomainmodelgraph) {
                        deferred.reject({
                            message: 'domain model graph not found',
                            error: {
                                "_id": domainmodelgraphid,
                                "err": "domain model graph not found"
                            }
                        });
                    }
                    else {

                        //remove DomainModelGraph here then callback
                        DomainModelGraph.remove({ "_id": objId }, function (remerr) {
                            if (remerr) {
                                deferred.reject({
                                    message: 'An error occurred while removing domain model graph type',
                                    error: err
                                });
                            }
                            else {
                                deferred.resolve(existingdomainmodelgraph);
                            }
                        });

                    }
                });
        }
        else {
            var error = {
                message: 'An error occurred - domain model graph id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    UpdateBaseModelRef: function (model) {
        var modelid = (model["_id"]).toString();
        if (model != null && model != 'undefined' &&
            model.graphjson != null && model.graphjson != 'undefined' &&
            model.graphjson.node != null && model.graphjson.node != 'undefined' && model.graphjson.node.length > 0) {

            for (var i = 0; i < model.graphjson.node.length; i++) {
                model.graphjson.node[i]["model"] = modelid;
            }
        }
        return model;
    }
}
