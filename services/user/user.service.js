var config = require('../../config');
var logger = require('../../lib/logger');
var db = require('../../models/connection');
var User = require('../../models/user/user');
var utility = require('../../lib/utility');

var _ = require('lodash');
var jwt = require('jsonwebtoken');
var Q = require('q');

var service = {};

service.authenticate = authenticate;
service.getAll = getAll;
service.getById = getById;
service.getByUserName = getByUserName;
service.create = create;
service.update = update;
service.delete = _delete;
service.changePassword = changePassword;
service.resetPassword = resetPassword;

module.exports = service;

function authenticate(username, password) {
    var deferred = Q.defer();

    // console.log(username + ':' + password);
    // find the user
    User.findOne(
        {
            'username': username
        },
        function (err, user) {
            var response = {};

            if (err) {
                deferred.reject({ 'success': false, 'message': err.name + ': ' + err.message });
                // console.log(err);
            }
            if (!user) {
                // console.log('service user not found');
                response = { 'success': false, 'message': 'Authentication failed. User not found.' };
            }
            else if (user) {
                if (utility.compareHash(password, user.hash)) {
                    response = {
                        success: true,
                        message: 'Enjoy your token!',
                        result: user,
                        token: jwt.sign({ sub: user._id }, config.token.secret, { expiresIn: config.token.expiryinmins * 60 })
                    };

                    // console.log(JSON.stringify(response));
                }
                else {
                    // authentication failed
                    response = {
                        success: false,
                        message: 'Authentication failed. Credentials do not match.'
                    };
                }
            }

            deferred.resolve(response);
        });

    return deferred.promise;
};

function getAll() {
    var deferred = Q.defer();

    User.find(function (err, users) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        // return users (without hashed passwords)
        users = _.map(users, function (user) {
            return _.omit(user.toObject(), 'hash');
        });

        deferred.resolve(users);
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    User.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user) {
            // return user (without hashed password)
            deferred.resolve(_.omit(user, 'hash'));
        } else {
            // user not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getByUserName(_userName) {
    var deferred = Q.defer();

    User.findOne(
        { username: _userName },
        function (err, user) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (user) {
                // return user (without hashed password)
                deferred.resolve(_.omit(user, 'hash'));
            } else {
                // user not found
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function create(userParam) {
    var deferred = Q.defer();

    // validation
    User.findOne(
        {
            username: userParam.username
        },
        function (err, user) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (user) {
                // username already exists
                deferred.reject('Username "' + userParam.username + '" is already taken');
            }
            else {
                createUser();
            }
        });

    function createUser() {
        // set user object to userParam without the cleartext password
        var user = _.omit(userParam, 'password');

        // add hashed password to user object
        // user.hash = bcrypt.hashSync(userParam.password, 10);
        user.hash = utility.createHash(utility.decryptData(userParam.password));

        var data = new User(user);
        data.forcePwdChange = false;

        // console.log(JSON.stringify(data));

        data.save(
            function (err) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function update(_id, userParam) {
    var deferred = Q.defer();

    // validation
    User.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user.username !== userParam.username) {
            // username has changed so check if the new username is already taken
            User.findOne(
                {
                    username: userParam.username
                },
                function (err, user) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (user) {
                        // username already exists
                        deferred.reject('Username "' + userParam.username + '" is already taken')
                    }
                    else {
                        updateUser();
                    }
                });
        }
        else {
            updateUser();
        }
    });

    function updateUser() {
        // fields to update
        var set = {
            firstName: userParam.firstName,
            lastName: userParam.lastName,
            username: userParam.username,
            admin: userParam.admin,
            modified: new Date()
        };

        // update password if it was entered
        if (userParam.password) {
            // set.hash = bcrypt.hashSync(userParam.password, 10);
            set.hash = utility.createHash(userParam.password);
        }

        //User.update()
        User.updateOne(
            { _id: _id },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    User.deleteOne(
        //{ _id: mongo.helper.toObjectID(_id) },
        { _id: _id },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

function changePassword(_id, userParam) {
    var deferred = Q.defer();

    // validation
    User.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user.hash !== utility.createHash(userParam.currpwd)) {
            // current password does not match
            deferred.reject('Current Password does not match for the user');
        }
        else {
            // fields to update
            var set = {};

            // update password if it was entered
            if (userParam.newpwd) {
                set.forcePwdChange = false;
                set.hash = utility.createHash(userParam.newpwd);
                set.modified = new Date()
            }

            //User.update()
            User.updateOne(
                { _id: _id },
                { $set: set },
                function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    deferred.resolve();
                });
        }
    });

    return deferred.promise;
}

function resetPassword(userParam) {
    var deferred = Q.defer();

    // validation
    User.findOne(
        {
            username: userParam.username
        },
        function (err, user) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (config.mail.allowsend === false) {
                deferred.reject('Email functionality is not enabled. Please contact application support.');
            }
            else if (!user) {
                // username already exists
                deferred.reject('Username <' + userParam.username + '> not found');
            }
            else if (user.email !== userParam.email) {
                deferred.reject('Email does not match in our records');
            }
            // Send mail only if allowed and connection to mail server is available
            else if (config.mail.allowsend && utility.verifyMailServerConnection()) {
                generateAndEmailPassword(user);
            }
            else {
                deferred.reject('Unable to send email. Please retry after some time or else contact application support.');
            }
        })

    function generateAndEmailPassword(user) {
        var newPwd = utility.generatePassword();

        // console.log(newPwd);
        // fields to update
        var set = {};

        // update password if it was entered
        if (newPwd) {
            set.forcePwdChange = true;
            set.hash = utility.createHash(newPwd);
            set.modified = new Date()
        }

        //User.update()
        User.updateOne(
            { _id: user._id },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                try {
                    utility.sendMail(user.email, "AI / ML App - Reset Password", newPwd, null);
                    deferred.resolve();
                }
                catch (ex) {
                    console.log('gen-email-password error:' + ex);
                    deferred.reject(ex.name + ': ' + ex.message);
                }
            });
    }

    return deferred.promise;
}
