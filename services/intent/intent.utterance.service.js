var Q = require('q');
var request = require('request');
const Rx = require("rxjs");
var utility = require('../../lib/utility');
var IntentUtterance = require('../../models/intent/IntentUtterance');
var Intent = require('../../models/intent/intent');
var NlpLanguageModel = require('../../models/nlp/nlplanguagemodel');
const config = require('../../config');

module.exports = {
    addIntentUtterance: function (intentParam) {
        var deferred = Q.defer();
        // console.log('intentParam', intentParam);
        Intent.findOne(
            {
                ModelName: intentParam.ModelName,
                CategoryName: intentParam.CategoryName
            },
            function (error, intent) {
                if (error) {
                    deferred.reject({ success: false, message: err.message });
                }
                else if (intent !== null) {
                    IntentUtterance.find(
                        {
                            UtteranceText: trimSpaces(intentParam.UtteranceText, false),
                            Category: intent._id
                        },
                        function (err, existingutterance) {
                            if (err) {
                                deferred.reject({ success: false, message: 'Error finding matching utterance' });
                            }
                            else if (existingutterance.length > 0) {
                                deferred.reject({ success: false, message: 'Utterance already exists' });
                            }
                            else {
                                intentParam.Category = intent._id;
                                intentParam.UtteranceText = trimSpaces(intentParam.UtteranceText, false);
                                intentParam.modified = new Date();

                                IntentUtterance.create(intentParam,
                                    function (err) {
                                        if (err) {
                                            deferred.reject({ success: false, message: err.name + ': ' + err.message });
                                        }
                                        else {
                                            // Update the NLP model to record successful training
                                            NlpLanguageModel.updateOne(
                                                { name: intentParam.ModelName },
                                                { $set: { istrained: false } },
                                                // { $set: { istrained: true } },
                                                function (err, doc) {
                                                    if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });

                                                    deferred.resolve({ success: true, data: 'Utterance created successfully' });
                                                });
                                        }
                                    });
                            }
                        });
                }
                else {
                    deferred.resolve({ success: true, data: 'No matching Intent found.' });
                }
            });

        return deferred.promise;
    },
    getIntentUtterances: function (categoryName, modelName) {
        var deferred = Q.defer();
        // console.log(modelName, categoryName);

        Intent.findOne(
            {
                ModelName: decodeURIComponent(modelName),
                CategoryName: decodeURIComponent(categoryName)
            },
            function (error, intent) {
                if (error) {
                    deferred.reject({ success: false, message: err.message });
                }
                else if (intent !== null) {
                    // console.log('intent', intent._id);
                    IntentUtterance
                        .find
                        (
                        { Category: intent._id, UtteranceText: { '$ne': config.nlp.defaults.utterance } },
                        null,
                        { sort: { UtteranceText: 1 } }
                        )
                        .populate({ path: 'Category', select: { 'CategoryName': 1, 'ModelName': 1 } })
                        .exec(function (err, data) {
                            if (err) {
                                deferred.reject({ success: false, message: err.message });
                            }
                            else {
                                deferred.resolve({ success: true, data: data });
                            }
                        });
                }
            });

        return deferred.promise;
    },
    getIntentUtteranceById: function (_id) {
        var deferred = Q.defer();
        // console.log(modelName, categoryName);

        IntentUtterance.findById(utility.AsObjectId(_id))
            .populate({ path: 'Category', select: { 'CategoryName': 1, 'ModelName': 1 } })
            .exec(function (err, data) {
                if (err) {
                    deferred.reject({ success: false, message: err.message });
                }
                else {
                    deferred.resolve({ success: true, data: data });
                }
            });

        return deferred.promise;
    },
    deleteIntentUtterance: function (_id, _modelName) {
        var deferred = Q.defer();

        IntentUtterance.findById(_id, function (err, utterance) {
            if (err) { deferred.reject({ success: false, message: 'Matching utterance not found' }); }

            if (utterance != null) {
                // For all the related entities, they will be part of a common category and utterance text
                IntentUtterance.deleteMany(
                    {
                        Category: utterance.Category,
                        UtteranceText: utterance.UtteranceText
                    },
                    function (error) {
                        if (error) { deferred.reject({ success: false, message: 'Error deleting utterance' }); }

                        // Make the model dirty for training
                        NlpLanguageModel.updateOne(
                            { name: _modelName },
                            { $set: { istrained: false } },
                            // { $set: { istrained: true } },
                            function (err, doc) {
                                if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });

                                deferred.resolve({ success: true, data: 'Utterance and associated entities deleted successfully' });
                                // deferred.resolve({ success: true, data: utterances });
                            });
                    });
            }
            else {
                deferred.resolve({ success: true, data: 'Matching utterance not found' });
            }
        });

        return deferred.promise;
    },
    getIntents: function (inputIntent, modelName) {
        var deferred = Q.defer();
        /* return http.get('http://usdfw11as667v:5003/api/getPOS/testmodel/'+ inputIntent).promise(
            data=>data
        ) */
        request(config.externalendpoints.nlp + '/getPOS/' + modelName + '/' + inputIntent, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                deferred.resolve(body); // Show the HTML for the Google homepage.
            }
            else if (error) {
                deferred.reject({ success: false, message: 'Error fetching intents' + error.message });
            }
        });

        return deferred.promise;
    },
    getNlpResponse: (userName, modelName, inputIntent) => {
        const url = config.externalendpoints.nlp + '/getPOS/' + userName + '/' + modelName + '/' + inputIntent;
        //console.log('url', url);
        return Rx.Observable.create((obs) => {
            request(url, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    obs.next(body);
                    obs.complete();
                }
                else {
                    obs.error(new Error('Error fetching intents:' + error.message));
                }
            });
        });
    },
    getIntentsByUser: function (userName, modelName, inputIntent) {
        var deferred = Q.defer();
        let modelUserName = userName;

        getModel(modelName).then(model => {
            // overwrite username with created user name to access model in NLP layer
            if (model.sysdefined === true || model.shared === true) {
                modelUserName = model.updatedBy.username;
            }

            let url = config.externalendpoints.nlp + '/getPOSByUser?username=' + modelUserName;
            url += '&modelname=' + modelName;
            url += '&userquery=' + inputIntent;

            request(url, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    deferred.resolve(body);
                }
                else if (error) {
                    deferred.reject({ success: false, message: 'Error fetching intents' + error.message });
                }

                // console.log(response, body);
            });
        })
            .catch(error => deferred.reject(error));

        return deferred.promise;
    },
    getTree: function (inputIntent, modelName) {
        var deferred = Q.defer();
        // console.log('Inside service', inputInten, modelName);
        /* return http.get('http://usdfw11as667v:5003/api/getPOS/testmodel/'+ inputIntent).promise(
            data=>data
        ) */
        request(config.externalendpoints.nlp + '/gettree/' + modelName + '/' + inputIntent, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                deferred.resolve(body); // Show the HTML for the Google homepage.
            }
            else if (error) {
                deferred.reject({ success: false, message: 'Error fetching tree ' + error.message });
            }
        });
        return deferred.promise;
    },
    getTreeByUser: function (userName, modelName, inputIntent) {
        var deferred = Q.defer();
        let modelUserName = userName;

        getModel(modelName).then(model => {
            // overwrite username with created user name to access model in NLP layer
            if (model.sysdefined === true || model.shared === true) {
                modelUserName = model.updatedBy.username;
            }

            let url = config.externalendpoints.nlp + '/getTreeByUser?username=' + userName
            url += '&modelname=' + modelName;
            url += '&userquery=' + inputIntent;

            request(url, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    deferred.resolve(body); // Show the HTML for the Google homepage.
                }
                else if (error) {
                    deferred.reject({ success: false, message: 'Error fetching tree ' + error.message });
                }
            });
        })
            .catch(error => deferred.reject(error));

        return deferred.promise;
    },
    // createEntity: function (modelName, entityData) {
    //     var deferred = Q.defer();

    //     Intent.findOne(
    //         { ModelName: modelName, CategoryName: entityData.CategoryName },
    //         function (error, intent) {
    //             if (error) {
    //                 deferred.reject({ success: false, message: error.message });
    //             }
    //             else if (intent !== null) {
    //                 IntentUtterance.findOne({ 'Category': intent._id, 'Entity.name': entityData.EntityName },
    //                     function (err, entities) {
    //                         // Entities do not exist for this utterance
    //                         if (entities == null || entities.length === 0) {
    //                             var data = new IntentUtterance(entityData);

    //                             data.Category = intent._id;
    //                             data.UtteranceText = entityData.Utterance;
    //                             data.Entity.name = entityData.EntityName;
    //                             data.Entity.startIndex = entityData.startIndex;
    //                             data.Entity.endIndex = entityData.endIndex;

    //                             data.save(
    //                                 function (err) {
    //                                     if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });

    //                                     NlpLanguageModel.updateOne(
    //                                         { name: modelName },
    //                                         { $set: { istrained: false } },
    //                                         // { $set: { istrained: true } },
    //                                         function (err, doc) {
    //                                             if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });

    //                                             deferred.resolve({ success: true, data: 'Entity mapped successfully.' });
    //                                         });
    //                                 });

    //                             // var addModelString = '';
    //                             // addModelString += '[(\'' + entityData.Utterance + '\',{\'entities\':[(' + entityData.startIndex + ',' + entityData.endIndex + ',\'' + entityData.EntityName + '\')]})]';
    //                             // console.log('addModelString', addModelString);

    //                             // var urlToPost = config.externalendpoints.nlp + '/addentity/' + modelName;
    //                             // console.log(urlToPost);

    //                             // var options = {
    //                             //     method: 'POST',
    //                             //     url: urlToPost,
    //                             //     headers:
    //                             //     {
    //                             //         'Cache-Control': 'no-cache',
    //                             //         'Content-Type': 'text/plain'
    //                             //     },
    //                             //     body: addModelString
    //                             // };

    //                             // request(options, function (error, response, body) {
    //                             //     if (error) throw new Error(error);
    //                             //     deferred.resolve(response);
    //                             //     console.log(body);
    //                             // });
    //                         }
    //                         else {
    //                             deferred.reject({ success: false, message: 'Entity with the same name already exists.' });
    //                         }
    //                     });
    //             }
    //             else {
    //                 deferred.reject({ success: false, message: 'No matching Category found.' });
    //             }
    //         });

    //     return deferred.promise;
    // },
    createEntity: function (modelName, entityData) {
        var deferred = Q.defer();

        Intent.findOne(
            { ModelName: modelName, CategoryName: entityData.CategoryName },
            function (error, intent) {
                if (error) {
                    deferred.reject({ success: false, message: error.message });
                }
                else if (intent !== null) {
                    var data = new IntentUtterance(entityData);

                    data.Category = intent._id;
                    data.UtteranceText = entityData.Utterance;
                    data.Entity.name = entityData.EntityName;
                    data.Entity.startIndex = entityData.startIndex;
                    data.Entity.endIndex = entityData.endIndex;

                    data.save(
                        function (err) {
                            if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });

                            NlpLanguageModel.updateOne(
                                { name: modelName },
                                { $set: { istrained: false } },
                                // { $set: { istrained: true } },
                                function (err, doc) {
                                    if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });

                                    deferred.resolve({ success: true, data: 'Entity mapped successfully.' });
                                });
                        });
                }
                else {
                    deferred.resolve({ success: false, message: 'No matching intent found!!' });
                }
            });

        return deferred.promise;
    },
    trainModel: function (trainModelString, modelName) {
        var deferred = Q.defer();
        // console.log('Inside service', modelName, trainModelString);

        var urlToPost = config.externalendpoints.nlp + '/traincategory/' + modelName;
        // console.log(urlToPost);

        var options = {
            method: 'POST',
            url: urlToPost,
            headers:
            {
                'Cache-Control': 'no-cache',
                'Content-Type': 'text/plain'
            },
            body: trainModelString
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            deferred.resolve(response);
            // console.log(body);
        });
        return deferred.promise;
    },
    trainEntireModel: function (modelName, userName) {
        let result = { versionId: config.externalendpoints.nlpVersionId };
        // console.log('trainEntireModel', modelName);
        let modelUserName = userName;

        var deferred = Q.defer();

        // Fetch model info
        getModel(modelName).then(model => {
            result.modelName = model.name;
            result.desc = model.description;
            result.culture = model.cultureCode.code;

            // overwrite username with created user name to access model in NLP layer
            if (model.sysdefined === true || model.shared === true) {
                modelUserName = model.updatedBy.username;
            }
        })
            // .catch(error => { console.log(error); deferred.reject(error); })
            .catch(error => { deferred.reject(error) })

        // Fetch intents and utterances associated with a model
        getUtterances(modelName)
            .then(utterances => {
                // console.log('utterances', utterances);
                result.utterances = [];
                let groupedData = [];

                let d3 = require('d3-collection');
                groupedData = (d3.nest()
                    .key(function (d) { return d.Category.CategoryName })
                    .key(function (d) { return d.UtteranceText })
                    .entries(utterances));

                // console.log('groupedData', JSON.stringify(groupedData));
                if (groupedData !== null && groupedData.length > 0) {

                    groupedData.forEach((value, index, data) => {
                        // get an array of utteranes for each category and then merge all as one groingle array
                        let utterances = prepareUtterances(value.key, value.values);

                        utterances.forEach((v, i, a) => {
                            result.utterances.push(v);
                        });
                    })
                }

                var urlToPost = config.externalendpoints.nlp + '/trainspacymodel';
                var trainingPayload = { model: result, user: modelUserName };
                // console.log('result', trainingPayload);
                // console.log('urlToPost', urlToPost, ''' + JSON.stringify(result).replace(''', '\'') + ''');

                var options = {
                    method: 'POST',
                    url: urlToPost,
                    // json: { model: result, user: userName },
                    json: trainingPayload,
                    headers:
                    {
                        'Cache-Control': 'no-cache',
                        'Content-Type': 'application/json'
                    }
                };

                request(options, function (error, response, body) {
                    if (error) deferred.reject({ success: false, message: error.message });

                    // console.log('response', response.body);

                    if (response) {
                        switch (response.statusCode) {
                            case (500): deferred.reject({ success: false, message: response.body.error }); break;
                            case (200): {
                                // Update the NLP model to record successful training
                                NlpLanguageModel.updateOne(
                                    { name: modelName },
                                    {
                                        $set: {
                                            istrained: true,
                                            publishdate: new Date(),
                                            // trainedPayload: JSON.stringify({ model: result, user: userName })
                                            trainedPayload: JSON.stringify(trainingPayload)
                                        }
                                    },
                                    // { $set: { istrained: true } },
                                    function (err, doc) {
                                        if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });

                                        deferred.resolve({
                                            success: response.body ? response.body.success : false,
                                            data: response.body ?
                                                { message: response.body.message, utterances: JSON.parse(response.body.utterances) }
                                                :
                                                { message: 'Error getting response', utterances: [] }
                                        });
                                    });

                                break;
                            }
                        }
                    }
                    else {
                        deferred.reject({ success: false, message: 'Model training failed!!!' });
                    }

                });
                // deferred.resolve({ success: true, data: result });
            })
            .catch(error => { deferred.reject(error) });

        return deferred.promise;
    },
    getUtterancesByModel: function (modelName) {
        var deferred = Q.defer();
        // console.log(modelName);

        Intent.find(
            {
                ModelName: modelName,
            },
            function (error, intents) {
                if (error) {
                    deferred.reject({ success: false, message: err.message });
                }
                else if (intents !== null && intents.length > 0) {
                    const dataFilter = intents.map(item => { return item._id });
                    // console.log(intents, dataFilter);

                    IntentUtterance
                        .find({ 'Category': { '$in': dataFilter }, UtteranceText: { '$ne': config.nlp.defaults.utterance } },
                        null,
                        {
                            sort: { modified: -1, CategoryName: 1, UtteranceText: 1, Entity: 1 }
                        })
                        .populate({ path: 'Category', select: { 'CategoryName': 1, 'ModelName': 1 } })
                        .exec(function (err, data) {
                            if (err) {
                                deferred.reject({ success: false, message: err.message });
                            }
                            else {
                                deferred.resolve({ success: true, data: data });
                            }
                        });
                }
                else {
                    deferred.resolve({ success: true, data: [] });
                }
            });

        return deferred.promise;
    },
    updateUtteranceScores: function (modelName, utterancesWithScores) {
        var deferred = Q.defer();

        if (typeof utterancesWithScores == "undefined" || utterancesWithScores == null ||
            utterancesWithScores.length == null || utterancesWithScores.length == 0) {

            deferred.reject({ success: false, message: 'No scores information provided' });
        }

        this.getUtterancesByModel(modelName).then(utterances => {
            // console.log('utterances', utterances.data);

            utterances.data.forEach(utt => {
                // console.log('utt', utt);

                // Get returned POS and scores for each utterance and update
                let results = utterancesWithScores
                    .filter(x => { return x.utterance === trimSpaces(utt.UtteranceText, true) })
                    .map(x => { return { scores: x.scores, pos: x.pos, posg: x.posg, entities: x.entities } });

                if (results && results.length > 0) {
                    // console.log('result', JSON.stringify(results[0].pos), JSON.stringify(results[0].scores));

                    // Update the utterance record with intent scores
                    IntentUtterance.updateOne(
                        { _id: utt._id },
                        {
                            $set: {
                                TrainingScores: utility.isJson(results[0].scores) ? JSON.stringify(results[0].scores) : null,
                                POS: utility.isJson(results[0].pos) ? JSON.stringify(results[0].pos) : null,
                                POSTree: utility.isJson(results[0].posg) ? JSON.stringify(results[0].posg) : null,
                                TrainedEntities: utility.isJson(results[0].entities) ? JSON.stringify(results[0].entities) : null
                            }
                        },
                        // { $set: { istrained: true } },
                        function (err, doc) {
                            if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });
                        });
                    // console.log('scores', JSON.stringify(scores[0]), utt);
                }
            });

            deferred.resolve({ success: true, data: 'Scores updated successfully' });
        })
            .catch(error => deferred.reject(error));

        return deferred.promise;
    },
    updateUtterance: function (modelName, uttInfo) {
        var deferred = Q.defer();

        IntentUtterance.findById(uttInfo._id, function (uttFindErr, utterance) {
            if (uttFindErr) { deferred.reject({ success: false, message: 'Matching utterance not found' }); }

            if (utterance != null) {
                const isTextChange = false || (utterance.UtteranceText !== trimSpaces(uttInfo.text, false));
                const isCategoryChange = false || (utterance.Category.toString() !== uttInfo.catId);

                // console.log(isTextChange, isCategoryChange);//, utterance, uttInfo);

                if (isTextChange) {
                    // Delete the utterance and all its associated entities
                    IntentUtterance.deleteMany(
                        { UtteranceText: utterance.UtteranceText, Category: utterance.Category },
                        (uttDelErr) => {
                            if (uttDelErr) deferred.reject({ success: false, message: uttDelErr.name + ': ' + uttDelErr.message });

                            // Create a new utterance 
                            IntentUtterance.create(
                                { Category: uttInfo.catId, UtteranceText: trimSpaces(uttInfo.text, false), modified: new Date() },
                                function (uttCreateErr) {
                                    if (uttCreateErr) {
                                        deferred.reject({ success: false, message: uttCreateErr.name + ': ' + uttCreateErr.message });
                                    }
                                    else {
                                        setLangModelDirty(modelName)
                                            .then(data => deferred.resolve({ success: true, data: 'Utterance updated successfully' }))
                                            .catch(error => deferred.reject(error));
                                    }
                                });
                        });
                }
                else if (isCategoryChange) {
                    IntentUtterance.updateOne(
                        { _id: uttInfo._id },
                        { $set: { Category: uttInfo.catId, modified: new Date() } },
                        // { $set: { istrained: true } },
                        function (uttUpdErr, doc) {
                            if (uttUpdErr) deferred.reject({ success: false, message: uttUpdErr.name + ': ' + uttUpdErr.message });

                            setLangModelDirty(modelName)
                                .then(data => deferred.resolve({ success: true, data: 'Utterance updated successfully' }))
                                .catch(error => deferred.reject(error));
                        });
                }
            }
            else {
                deferred.resolve({ success: false, data: 'Utterance not found' });
            }
        });

        return deferred.promise;
    }
}

function setLangModelDirty(modelName) {
    var deferred = Q.defer();

    NlpLanguageModel.updateOne(
        { name: modelName },
        { $set: { istrained: false } },

        function (err, doc) {
            if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });

            deferred.resolve();
        });

    return deferred.promise;
}

function getModel(modelName) {
    var deferred = Q.defer();

    NlpLanguageModel.findOne({ 'name': modelName })
        .populate({ path: 'cultureCode', select: { 'code': 1, 'name': 1 } })
        .populate({ path: 'updatedBy', select: 'username' })
        .exec(function (error, model) {
            if (error) deferred.reject({ success: false, message: error.message });

            // console.log('model', model, modelName);
            if (!model) {
                deferred.reject({ success: false, message: 'Model not found!!' });
                // throw new Error({ success: false, error: 'Model not found!!' });
            }

            deferred.resolve(model);
        });

    return deferred.promise;
}

function getUtterances(modelName) {
    var deferred = Q.defer();

    Intent.find({ 'ModelName': modelName },
        null,
        {
            sort: { CategoryName: 1 }
        },
        function (error, intents) {
            if (error) deferred.reject({ success: false, message: error.message });

            return intents;
        })
        // .sort('CategoryName')
        .then(function (intents) {
            // const dataFilter = intents.map(item => { return item.CategoryName });
            const dataFilter = intents.map(item => { return item._id });

            IntentUtterance
                .find({ 'Category': { '$in': dataFilter } },
                null,
                {
                    sort: { CategoryName: 1, UtteranceText: 1, Entity: 1 }
                })
                .populate({ path: 'Category', select: { 'ModelName': 1, 'CategoryName': 1 } })
                .exec(function (error, utterances) {
                    if (error) deferred.reject({ success: false, message: error.message });

                    // console.log('utt:', utterances);
                    // return utterances; 
                    deferred.resolve(utterances);
                })
            // .then(data => {
            //     // console.log(data);
            //     deferred.resolve(data);
            // })
        })
        // .then((data) => {
        //     console.log(data);
        //     return data;
        // })
        .catch(error => { deferred.reject(error) });

    return deferred.promise;
}

function prepareUtterances(key, values) {
    let entities = [];
    let utterances = [];
    let utterance = {};

    if (values && values.length > 0) {
        for (let index = 0; index < values.length; index++) {
            let value = values[index];

            // console.log('value', value, index, values.length);

            if (value.values && value.values.length > 0) {
                // console.log('value.values.length', value.values.length);

                for (var items = 0; items < value.values.length; items++) {
                    let v = value.values[items];

                    if (v.UtteranceText) {
                        utterance.text = trimSpaces(v.UtteranceText, true);
                    }

                    if (v && v.Entity && typeof v.Entity.name !== 'undefined') {
                        // console.log(typeof v.Entity.name);
                        entities.push(v.Entity);
                    }
                }

                utterance.category = key;
                utterance.entities = entities.map(x => { return x });
                // console.log('uttenrance', utterance, items);

                utterances.push(utterance);
                utterance = {};
                entities = [];
            }
        }
    }

    return utterances;
}

function trimSpaces(utterance, makeLowerCase) {
    if (utterance == null || utterance.length === 0) {
        return null;
    }

    var parts = utterance.split(" ");
    var finalParts = parts.map(x => { return x.trim() }).filter(x => { return x !== "" });

    if (makeLowerCase == true) {
        return finalParts.map(x => { return x.toLowerCase() }).join(" ");
    }

    return finalParts.join(" ");
}