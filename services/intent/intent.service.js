var config = require('../../config');
var Intent = require('../../models/intent/intent');
var IntentUtterance = require('../../models/intent/IntentUtterance');
var NlpLanguageModel = require('../../models/nlp/nlplanguagemodel');

var Q = require('q');

module.exports = {
    // find the user
    getAll: function () {
        var deferred = Q.defer();

        Intent.find({}, function (err, data) {

            if (err) { console.log('intent find error', err); }
            else {
                deferred.resolve(data);
            }
        });

        return deferred.promise;
    },
    getByModel: function (modelName) {
        var deferred = Q.defer();
        /*  Intent.aggregate(
             [
                 {
                     $lookup:
                         {
                             from: 'intentutterances',
                             localField: 'CategoryName',
                             foreignField: 'CategoryName',
                             as: 'intentutterances'
                         }
                 },
                 {
                     $group:
                         {
                             _id: '$intentutterances',
                             'numOfStudent': { $sum: 1 },
 
                         }
                 }
             ], function (err, data) {
 
                 if (err) { deferred.reject(err.name + ': ' + err.message) }
                 else {
                     deferred.resolve(data);
                 }
             }); */

        let result = [];
        let finalResult = [];

        const aggregatorOpts = [{
            $unwind: "$Category"
        },
        // Skip default utterance
        { "$match": { $and: [{ 'Entity.name': null || undefined }, { 'UtteranceText': { '$ne': config.nlp.defaults.utterance } }] } },
        {
            $group: {
                _id: "$Category",
                count: { $sum: 1 }
            }
        }];

        IntentUtterance.aggregate(aggregatorOpts).exec((error, data) => {
            if (data !== null) {
                result = data;
                // console.log('data', data, modelName);

                Intent.find(
                    { 'ModelName': modelName },
                    null,
                    {
                        sort: { CategoryName: 1 }
                    },
                    function (err, intents) {
                        if (err) {
                            deferred.reject({ success: false, message: err.name + ': ' + err.message });
                        }
                        else if (intents != null) {
                            // console.log('intents', intents);

                            intents.forEach(intent => {
                                var count = result.filter(x => { return x._id.toString() === intent._id.toString() })
                                    .map(y => { return y.count });

                                finalResult.push({
                                    _id: intent._id,
                                    CategoryName: intent.CategoryName,
                                    UtterancesCount: (count && count.length > 0) ? count[0] : 0
                                });
                            });

                            deferred.resolve(finalResult);
                        }
                    });
            }
        });

        return deferred.promise;
    },
    createIntent: function (intentParam) {
        var deferred = Q.defer();

        Intent.find(
            { CategoryName: intentParam.CategoryName, ModelName: intentParam.ModelName },
            function (err, existingintent) {
                if (err) {
                    deferred.reject({ success: false, message: err.name + ': ' + err.message });
                }
                else if (existingintent != null && existingintent.length > 0) {
                    deferred.reject({ success: false, message: 'Intent already exists' });
                }
                else {
                    Intent.create(intentParam,
                        function (err) {
                            if (err) deferred.reject(err.name + ': ' + err.message);

                            NlpLanguageModel.updateOne(
                                { name: intentParam.ModelName },
                                { $set: { istrained: false } },
                                // { $set: { istrained: true } },
                                function (err, doc) {
                                    if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });

                                    deferred.resolve({ success: true, data: 'Intent created successfully.' });
                                });
                        });
                }
            }
        );

        return deferred.promise;
    },
    // deleteIntent: function (_id) {
    //     var deferred = Q.defer();

    //     Intent.findById(_id, function (err, intent) {
    //         if (err) { deferred.reject({ success: false, message: 'Error finding Intent' }); }

    //         if (intent != null) {
    //             IntentUtterance.deleteMany({ CategoryName: intent.CategoryName }, function (error) {
    //                 if (err) { deferred.reject({ success: false, message: 'Error deleting utterance(s)' }); }

    //                 Intent.deleteOne({ _id: _id }, function (errorIntent) {
    //                     if (err) { deferred.reject({ success: false, message: 'Error deleting Intent' }); }

    //                     deferred.resolve({ success: true, data: 'Intent and associated utterance(s) deleted successfully' });
    //                 });
    //             });
    //         }
    //         else {
    //             deferred.reject({ success: false, data: 'No matching Intent found' });
    //         }
    //     });

    //     return deferred.promise;
    // }
    deleteIntent: function (_id) {
        var deferred = Q.defer();

        IntentUtterance.deleteMany({ Category: _id }, function (error) {
            // IntentUtterance.find({ Category: _id }, function (error, results) {
            if (error) { deferred.reject({ success: false, message: 'Error deleting utterance(s)' }); }

            Intent.findByIdAndRemove({ _id: _id }, function (errorIntent, deletedIntent) {
                if (errorIntent) { deferred.reject({ success: false, message: 'Error deleting Intent' }); }

                NlpLanguageModel.updateOne(
                    { name: deletedIntent.ModelName },
                    { $set: { istrained: false } },
                    // { $set: { istrained: true } },
                    function (err, doc) {
                        if (err) deferred.reject({ success: false, message: err.name + ': ' + err.message });

                        deferred.resolve({ success: true, data: 'Intent and associated utterance(s) deleted successfully' });
                    });
            });

            // deferred.resolve({ success: true, data: results });
        });

        return deferred.promise;
    }
}
