const mongoose = require('mongoose');
const { ClientApplication } = require('../../models/client/');
var Q = require('q');

module.exports = {

    Add: function (clientapplication) {
        var deferred = Q.defer();
        if (clientapplication && clientapplication.applicationname) {

            this.GetByApplicationName(clientapplication.applicationname).then(
                existingClientApplication => {


                    //if not found
                    if (!existingClientApplication) {

                        var newclientapplication = new ClientApplication({
                            applicationname: clientapplication.applicationname,
                            authenticationtoken: clientapplication.authenticationtoken,
                            projects: clientapplication.projects,
                            status: clientapplication.status,
                            createdby: clientapplication.createdby.toLowerCase(),
                        });

                        newclientapplication.save(function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while adding a new client application",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                    }
                    else {  //else throw error - already exists
                        deferred.reject({
                            message: "Client Application of same name already exists!!",
                            error: existingClientApplication
                        });
                    }
                },
                fnderr => {
                    deferred.reject({
                        message: "An Error has occured while checking existance before adding client application",
                        error: fnderr
                    });
                });
        }
        else {
            var error = {
                message: 'An error occurred -  client application not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Update: function (clientapplication) {
        var deferred = Q.defer();
        if (clientapplication) {
            var objId = mongoose.Types.ObjectId(clientapplication._id);
            ClientApplication.findOne({ "_id": objId }, function (err, existingClientApplication) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a client application detail',
                        error: err
                    });
                }

                if (!existingClientApplication) {
                    deferred.reject({
                        message: 'client application not found',
                        error: { "_id": clientapplication._id, "err": "client application not found" }
                    });
                }
                else {

                    ClientApplication.update({ "_id": objId },
                        {
                            $set: {
                                applicationname: clientapplication.applicationname,
                                authenticationtoken: clientapplication.authenticationtoken,
                                projects: clientapplication.projects,
                                status: clientapplication.status,
                                lastmodifiedby: clientapplication.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating client application details",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - client application not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Get: function (clientapplicationid) {
        var deferred = Q.defer();
        if (clientapplicationid) {
            try {
                var objId = mongoose.Types.ObjectId(clientapplicationid);
                ClientApplication.findOne({ "_id": objId }, function (err, existingClientApplication) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing client application detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingClientApplication);
                });
            }
            catch (ex) {
                deferred.reject({
                    message: 'An error occurred while fetcing client application detail as application id is not valid',
                    error: ex
                });
            }
        }
        else {
            var error = {
                message: 'An error occurred - client application id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Authenticate(applicationid, authtoken, projectid) {
        var deferred = Q.defer();
        
        if (applicationid && authtoken) {
            ClientApplication.findOne({ "_id": applicationid },
                function (err, existingClientApplication) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing client application detail',
                            error: err
                        });
                    }

                    if (existingClientApplication && existingClientApplication !== 'undefined') {
                        if (existingClientApplication.authenticationtoken == authtoken && 
                             existingClientApplication.projects != null && existingClientApplication.projects !== 'undefined' && 
                             existingClientApplication.projects.length > 0 && 
                             existingClientApplication.projects.indexOf(projectid) > -1) {
                            deferred.resolve(true);
                        }
                        else {
                            deferred.resolve(false);
                        }
                    }
                    else {
                        deferred.resolve(false);
                    }

                });
        }
        else {
            var error = {
                message: 'An error occurred - client application name not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },


    GetByApplicationName: function (applicationname) {
        var deferred = Q.defer();
        if (applicationname) {
            ClientApplication.findOne({ "applicationname": { $regex: new RegExp("^" + applicationname.toLowerCase() + "$", "i") } },
                function (err, existingClientApplication) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing client application detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingClientApplication);
                });
        }
        else {
            var error = {
                message: 'An error occurred - client application name not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetAll: function () {
        var deferred = Q.defer();
        ClientApplication.find({}, function (err, existingClientApplication) {
            if (err) {
                deferred.reject({
                    message: 'An error occurred while fetcing client applications',
                    error: err
                });
            }
            deferred.resolve(existingClientApplication);
        });
        return deferred.promise;
    },

    Delete: function (clientapplicationid) {
        var deferred = Q.defer();
        if (clientapplicationid) {
            var objId = mongoose.Types.ObjectId(clientapplicationid);
            ClientApplication.findOne({ "_id": objId },
                function (err, existingClientApplication) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetching client application detail',
                            error: err
                        });
                    }

                    if (!existingClientApplication) {
                        deferred.reject({
                            message: 'client application not found',
                            error: {
                                "applicationid": clientapplicationid,
                                "err": "client application not found"
                            }
                        });
                    }
                    else {

                        //remove ClientApplication here then callback
                        ClientApplication.remove({ "_id": objId }, function (remerr) {
                            if (remerr) {
                                deferred.reject({
                                    message: 'An error occurred while removing client application',
                                    error: err
                                });
                            }
                            else {
                                deferred.resolve(existingClientApplication);
                            }
                        });
                    }
                });
        }
        else {
            var error = {
                message: 'An error occurred - client application id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }
}
