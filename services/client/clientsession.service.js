const mongoose = require('mongoose');
const { ClientSession } = require('../../models/client/');
const Q = require('q');
const Utility = require('../../lib/utility');

module.exports = {

    Initiate: function (clientsession) {
        var deferred = Q.defer();
        if (clientsession && clientsession.applicationid) {

            var newclientsession = new ClientSession({
                applicationid: clientsession.applicationid,
                projectid: clientsession.projectid,
                usercontext: clientsession.usercontext,
                chathistory: clientsession.chathistory,
                createdby: clientsession.createdby.toLowerCase(),
            });

            newclientsession.save(function (err, result) {
                if (err) {
                    deferred.reject({
                        message: "An Error has occured while initiating a new session for client application",
                        error: err
                    });

                } else {
                    deferred.resolve(result);
                }
            });
        }
        else {
            var error = {
                message: 'An Error has occured while initiating a new session for client application -  application id not defined',
                error: null
            };
            deferred.reject(error);
        }

        return deferred.promise;
    },

    Update: function (clientsession) {
        var deferred = Q.defer();

        if (clientsession && clientsession._id) {
            var objId = mongoose.Types.ObjectId(clientsession._id);

            ClientSession.findOne({ "_id": objId }, function (err, existingClientSession) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating a client application session detail',
                        error: err
                    });
                }

                if (!existingClientSession) {
                    deferred.reject({
                        message: 'client application not found',
                        error: { "_id": clientsession._id, "err": "client application not found" }
                    });
                }
                else {

                    ClientSession.update({ "_id": objId },
                        {
                            $set: {
                                applicationid: clientsession.applicationid,
                                projectid: clientsession.projectid,
                                usercontext: clientsession.usercontext,
                                chathistory: clientsession.chathistory,
                                lastmodifiedby: clientsession.lastmodifiedby.toLowerCase(),
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating client application session details",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - client application session id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    UpdateChatHistory: function(clientsessionid, message, response) {
        var deferred = Q.defer();

        var chat = {
            "timestamp": new Date(),
            "user": message,
            "bot": response
        }

        if (clientsessionid) {
            ClientSession.findOne({ "_id": clientsessionid }, function (err, existingClientSession) {
                if (err) {
                    deferred.reject({
                        message: 'An error occurred while updating chat history for client application session',
                        error: err
                    });
                }

                if (!existingClientSession) {
                    deferred.reject({
                        message: 'client application session not found',
                        error: { "sessionid": clientsessionid, "err": "client application session not found" }
                    });
                }
                else {

                    var sessionChatHistory = Utility.IsArray(existingClientSession.chathistory)?existingClientSession.chathistory:[];
                    sessionChatHistory.push(chat);

                    ClientSession.update({ "_id": clientsessionid },
                        {
                            $set: {
                                chathistory: sessionChatHistory,
                                lastmodifiedon: Date.now(),
                            }
                        },
                        function (err, result) {
                            if (err) {
                                deferred.reject({
                                    message: "An Error has occured while updating chat history fo client application session",
                                    error: err
                                });

                            } else {
                                deferred.resolve(result);
                            }
                        });
                }
            });
        }
        else {
            var error = {
                message: 'An error occurred - client session id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Get: function (clientsessionid) {
        var deferred = Q.defer();
        if (clientsessionid) {
            try {
               // var objId = mongoose.Types.ObjectId(clientsessionid);
                ClientSession.findOne({ "_id": clientsessionid }, function (err, existingClientSession) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing client application session detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingClientSession);
                });
            }
            catch (ex) {
                deferred.reject({
                    message: 'An error occurred while fetcing client application detail as application session id is not valid',
                    error: ex
                });
            }
        }
        else {
            var error = {
                message: 'An error occurred - client application session id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    Validate(applicationid, projectid, userContext) {
        var deferred = Q.defer();
        if (applicationid && projectid && userContext && userContext.userid) {

            ClientSession.find({
                "applicationid": applicationid,
                "projectid": projectid, "createdby": userContext.userid
            })
                .sort({ "lastmodifiedon": -1 })
                .limit(1)
                .exec(
                    function (err, existingClientSessions) {
                        if (err) {
                            deferred.reject({
                                message: 'An error occurred while fetcing client application session detail',
                                error: err
                            });
                        }

                        //session will be valid for whole day
                        if (existingClientSessions && existingClientSessions.length > 0) {
                            var existingClientSession = existingClientSessions[0];
                            var currentday = getDateAsNumber(null);

                            if (existingClientSession && existingClientSession !== 'undefined'
                                && existingClientSession.lastmodifiedon != null && existingClientSession.lastmodifiedon !== 'undefined') {

                                var lastsessionday = getDateAsNumber(existingClientSession.lastmodifiedon);
                                console.log('session diff...', currentday, lastsessionday)
                                if (currentday == lastsessionday) {
                                    deferred.resolve(existingClientSession);
                                }
                                else {
                                    deferred.resolve(null);
                                }
                            }
                            else {
                                deferred.resolve(null);
                            }
                        } else {
                            deferred.resolve(null);
                        }
                    });
        }
        else {
            deferred.resolve(null);
        }
        return deferred.promise;
    },


    GetAllByApplicationId: function (applicationid) {
        var deferred = Q.defer();
        if (applicationid) {
            ClientSession.find({ "applicationid": applicationid },
                function (err, existingClientSessions) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetcing client application sessions detail',
                            error: err
                        });
                    }
                    deferred.resolve(existingClientSessions);
                });
        }
        else {
            var error = {
                message: 'An error occurred - client application session not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    },

    GetAll: function () {
        var deferred = Q.defer();
        ClientSession.find({}, function (err, existingClientSession) {
            if (err) {
                deferred.reject({
                    message: 'An error occurred while fetcing sessions for all client applications',
                    error: err
                });
            }
            deferred.resolve(existingClientSession);
        });
        return deferred.promise;
    },

    Delete: function (clientsessionid) {
        var deferred = Q.defer();
        if (clientsessionid) {
            var objId = mongoose.Types.ObjectId(clientsessionid);
            ClientSession.findOne({ "_id": objId },
                function (err, existingClientSession) {
                    if (err) {
                        deferred.reject({
                            message: 'An error occurred while fetching client application session detail',
                            error: err
                        });
                    }

                    if (!existingClientSession) {
                        deferred.reject({
                            message: 'client application session not found',
                            error: {
                                "sessionid": clientsessionid,
                                "err": "client application session not found"
                            }
                        });
                    }
                    else {

                        //remove ClientSession here then callback
                        ClientSession.remove({ "_id": objId }, function (remerr) {
                            if (remerr) {
                                deferred.reject({
                                    message: 'An error occurred while removing client application session',
                                    error: err
                                });
                            }
                            else {
                                deferred.resolve(existingClientSession);
                            }
                        });
                    }
                });
        }
        else {
            var error = {
                message: 'An error occurred - client application session id not defined',
                error: null
            };
            deferred.reject(error);
        }
        return deferred.promise;
    }
}

function getDateAsNumber(mngdate) {
    var dt = (mngdate != null) ? new Date(mngdate) : new Date();
    var num = leftpad(dt.getFullYear().toString(), 4) + leftpad(dt.getMonth().toString(), 2) + leftpad(dt.getDate().toString(), 2);
    return eval(num);
}

function leftpad(num, padlen) {
    var padzeros = '00000000000';
    var temp = padzeros + num;
    return temp.substr((padzeros.length + num.length) - padlen);
}
