var config = require('../../config');
var logger = require('../../lib/logger');
var db = require('../../models/connection');
var Models = require('../../models/model/model');
var { DomainModelGraph } = require('../../models/intentfulfillment');
var utility = require('../../lib/utility');

var _ = require('lodash');
var jwt = require('jsonwebtoken');
var Q = require('q');

var service = {};

service.create = create;
service.getAll = getAll;
service.getById = getById;
service.update = update;
service.delete = _delete;

module.exports = service;

function getAll() {
    var deferred = Q.defer();

    DomainModelGraph.find({}, function (err, data) {

        if (err) {
            console.log('Error occure while fetching domain models...', err)
            deferred.resolve([]);
        }
        else {
            //this transformation needs to remove
            /*
            var results = [];
            if (data != null && data != 'undefined') {
                for (var i = 0; i < data.length; i++) {
                    var result = data[i];
                    
                    results.push({
                        _id: result._id,
                        name: result.name,
                        body: result.graphjson,
                        graphjson: result.graphjson,
                        nodeproperties: result.nodeproperties
                    });
                }
                console.log(results);
                deferred.resolve(results);
            }
            */
           deferred.resolve(data);
        }
    });

    /*
    predefinedModels = [
        {
            _id: 0, name: 'Client', body: {
                node: [
                    { model: 901, basekey: 1, nodetype: "entity", key: 1, text: "Geo", color: "lightblue" },
                    { model: 901, basekey: 2, nodetype: "entity", key: 2, text: "Client", color: "lightblue" }
                ],
                link: [
                    { from: "1", to: "2" }
                ]
            }
        },
        {
            _id: 1, name: 'Health', body: {
                node: [
                    { model: 101, basekey: 1, nodetype: "entity", key: 1, text: "Plans", color: "lightblue" },
                    { model: 101, basekey: 2, nodetype: "entity", key: 2, text: "Health", color: "lightblue" },
                    { model: 101, basekey: 3, nodetype: "entity", key: 3, text: "Policy", color: "lightblue" },
                    { model: 101, basekey: 4, nodetype: "entity", key: 4, text: "Coverage", color: "lightblue" },
                    { model: 101, basekey: 5, nodetype: "entity", key: 5, text: "Claims", color: "lightblue" }
                ], link:
                    [
                        { from: "1", to: "2" },
                        { from: "1", to: "3" },
                        { from: "2", to: "3" },
                        { from: "3", to: "4" },
                        { from: "3", to: "5" }
                    ]
            }
        },
        {
            _id: 2, name: 'Organisation', body: {
                node: [
                    { model: 201, basekey: 1, key: 3, text: "Organisation", color: "lightblue" },
                    { model: 201, basekey: 2, key: 4, text: "Employee", color: "lightblue" },
                    { model: 201, basekey: 3, key: 5, text: "Position", color: "lightblue" },
                    { model: 201, basekey: 4, key: 6, text: "Employment", color: "lightblue" }
                ], link:
                    [
                        { from: "3", to: "4" },
                        { from: "3", to: "5" },
                        { from: "4", to: "6" },
                        { from: "5", to: "6" }
                    ]
            }
        },
        {
            _id: 3, name: 'Wealth', body: {
                node: [
                    { model: 301, basekey: 1, key: 13, text: "Fund Manager", color: "lightblue" },
                    { model: 301, basekey: 2, key: 14, text: "Strategies", color: "lightblue" },
                    { model: 301, basekey: 3, key: 15, text: "Vehicles", color: "lightblue" },
                ],
                link: [
                    { from: "13", to: "14" },
                    { from: "14", to: "15" }
                ]
            }
        }
    ];
    deferred.resolve(predefinedModels);
    */


    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    Models.findById(_id, function (err, model) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (model) {
            // return model
            deferred.resolve(model);
        } else {
            // model not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function create(modelParam) {
    var deferred = Q.defer();

    // validation
    model.findOne(
        {
            name: modelParam.modelname
        },
        function (err, model) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (model) {
                // model with the name already exists
                deferred.reject('modelname "' + modelParam.modelname + '" is already exists');
            }
            else {
                if (JSON.parse(modelParam.body) == null) {
                    deferred.reject('Invalid model body JSON');
                }
                createModel();
            }
        });

    function createModel() {
        var data = new Models(model);
        // console.log(JSON.stringify(data));

        data.save(
            function (err) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function update(_id, modelParam) {
    var deferred = Q.defer();

    // validation
    model.findById(_id, function (err, model) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (model.name !== modelParam.name) {
            // modelname has changed so check if the new modelname is already taken
            model.findOne(
                {
                    name: modelParam.name
                },
                function (err, model) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (model) {
                        // modelname already exists
                        deferred.reject('model name "' + modelParam.name + '" is already taken')
                    }
                    else {
                        updateModel();
                    }
                });
        }
        else {
            updateModel();
        }
    });

    function updateModel() {
        // fields to update
        var set = {
            body: modelParam.body,
            modified: new Date()
        };

        //model.update()
        model.updateOne(
            { _id: _id },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    model.deleteOne(
        //{ _id: mongo.helper.toObjectID(_id) },
        { _id: _id },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
