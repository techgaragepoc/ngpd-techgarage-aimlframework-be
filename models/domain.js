var mongoose = require("mongoose");
var domainMasterSchema = mongoose.Schema;

var domainMasterModel = new domainMasterSchema({
    DomainName: { type: String },
    DomainDesc: { type: String },
    UpdatedBy: { type: String },
    Updatedts: { type: Number } // Date time stamp when event was updated/ created
});

module.exports = mongoose.model('DomainMaster', domainMasterModel);