var mongoose = require("mongoose");
var modelsSchema = mongoose.Schema;

var mlModel = new modelsSchema({
    name: { type: String }, // Unique code assigned to applications
    domain: {
        type: modelsSchema.ObjectId,
        ref: "Category"
    },
    body: {
        type: String,
        get: function (data) {
            try {
                return JSON.parse(data);
            } catch (e) {
                return data;
            }
        },
        set: function (data) {
            return data;
        }
    }, // JSON object
    modified: { type: Date, default: Date.now }, // Date time stamp
    updatedBy: String,
    sysdefined: { type: Boolean, default: false }
});

module.exports = mongoose.model('Model', mlModel);
