const mongoose = require('mongoose');

// create the schema
var NodeDatasetMapping = new mongoose.Schema({
    domainmodelgraphid: {
        type: String,
        index: true
    },
    nodeid: {
        type: String,
    },
    datasetid: {
        type: String,
    },
    attributemapping:{
        type: mongoose.Schema.Types.Mixed,
    },
    default: {
        type: Boolean,
        default: false
    },
    createdby: {
        type: String
    },
    createdon:{
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon: {
        type: Date,
        default: Date.now
    }
})

// set up a mongoose model and pass it using module.exports
module.exports =  mongoose.model('nodedatasetmapping', NodeDatasetMapping);