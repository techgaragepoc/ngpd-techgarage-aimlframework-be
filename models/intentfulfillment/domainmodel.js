const mongoose = require('mongoose');

// create the schema
var DomainModel = new mongoose.Schema({
    modelgraphid: {
        type: String,
        index: true
    },
    name: {
        type: String,
    },
    nodes:{
        type: mongoose.Schema.Types.Mixed,
    },
    nodeproperties:{
        type: mongoose.Schema.Types.Mixed,
    },
    createdby: {
        type: String
    },
    createdon:{
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon: {
        type: Date,
        default: Date.now
    }
})

// set up a mongoose model and pass it using module.exports
module.exports =  mongoose.model('domainmodel', DomainModel);