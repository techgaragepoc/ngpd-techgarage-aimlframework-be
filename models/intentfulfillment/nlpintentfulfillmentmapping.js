const mongoose = require('mongoose');

// create the schema
var NLPIntentFulfillmentMapping = new mongoose.Schema({
    nlpmodelid:{
        type: String,
        index: true
    },
    nlpintentid:{
        type: String,
        index: true
    },
    intentfulfillmentmodelgraphid: {
        type: String,
        index: true
    },
    implementationstatus: {
        type: String,
        default: '0'
    },
    createdby: {
        type: String
    },
    createdon:{
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon: {
        type: Date,
        default: Date.now
    }
})

// set up a mongoose model and pass it using module.exports
module.exports =  mongoose.model('NlpIntentFulfillmentMapping', NLPIntentFulfillmentMapping);