const mongoose = require('mongoose');

// create the schema
var IntentFulfillmentModelGraph = new mongoose.Schema({
    name: {
        type: String,
        index: true
    },
    pos:{
        type: mongoose.Schema.Types.Mixed,
    },
    graphjson:{
        type: mongoose.Schema.Types.Mixed,
    },
    nodeproperties:{
        type: mongoose.Schema.Types.Mixed,
    },
    createdby: {
        type: String
    },
    createdon:{
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon: {
        type: Date,
        default: Date.now
    }
})

// set up a mongoose model and pass it using module.exports
module.exports =  mongoose.model('intentfulfillmentmodelgraph', IntentFulfillmentModelGraph);