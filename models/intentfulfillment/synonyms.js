const mongoose = require('mongoose');

// create the schema
var Synonyms = new mongoose.Schema({
    refid:{
        type: String,
        index: true
    },
    synonyms: {
        type: [String]
    },
    createdby: {
        type: String
    },
    createdon:{
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon: {
        type: Date,
        default: Date.now
    }
})

// set up a mongoose model and pass it using module.exports
module.exports =  mongoose.model('synonyms', Synonyms);