const mongoose = require('mongoose');

// create the schema
var IntentFulfillmentModel = new mongoose.Schema({
    modelgraphid: {
        type: String,
        index: true
    },
    pos:{
        type: mongoose.Schema.Types.Mixed,
    },
    name: {
        type: String,
    },
    details:{
        type: mongoose.Schema.Types.Mixed,
    },
    nodes:{
        type: mongoose.Schema.Types.Mixed,
    },
    nodeproperties:{
        type: mongoose.Schema.Types.Mixed,
    },
    createdby: {
        type: String
    },
    createdon:{
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon: {
        type: Date,
        default: Date.now
    }
})

// set up a mongoose model and pass it using module.exports
module.exports =  mongoose.model('intentfulfillmentmodel', IntentFulfillmentModel);