const mongoose = require('mongoose');

// create the schema
var Functions = new mongoose.Schema({
    code:{
        type: String,
        index: true
    },
    name:{
        type: String,
        index: true
    },
    synonyms: {
        type: [String]
    },
    implementation: {
        type: String
    },
    createdby: {
        type: String
    },
    createdon:{
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon: {
        type: Date,
        default: Date.now
    }
})

// set up a mongoose model and pass it using module.exports
module.exports =  mongoose.model('functions', Functions);