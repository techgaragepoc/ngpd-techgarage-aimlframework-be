const mongoose = require('mongoose');

// create the schema
var IntentffModelExecutionResult = new mongoose.Schema({
    sessionid:{
        type: String,
        index: true
    },
    level: {
        type: Number,
        default: 0
    },
    message:{
        type: mongoose.Schema.Types.Mixed
    },
    initiatedon: {
        type: Date,
        default: Date.now
    },
    lastmodifiedon: {
        type: Date,
        default: Date.now
    }
})

// set up a mongoose model and pass it using module.exports
module.exports =  mongoose.model('intentffmodelexecutionresult', IntentffModelExecutionResult);