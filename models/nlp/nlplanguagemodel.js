// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var NlpLanguageModelSchema = mongoose.Schema({
    name: String,
    cultureCode: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Culture'
    },
    description: String,
    endpointsHitCount: Number,
    sysdefined: { type: Boolean, default: false },
    shared: { type: Boolean, default: false },
    istrained: { type: Boolean, default: false },
    trainedPayload: String,
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    publishdate: { type: Date, },// default: Date.now },
    modified: { type: Date, default: Date.now }
})

// module.exports.NlpLanguageModel = mongoose.model('NlpLanguageModel', NlpLanguageModelSchema);

var NlpLanguageModel = mongoose.model('NlpLanguageModel', NlpLanguageModelSchema);
module.exports = NlpLanguageModel;
