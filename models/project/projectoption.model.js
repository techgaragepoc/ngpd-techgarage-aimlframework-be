// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var projOptSchema = mongoose.Schema;

var optionModel = new projOptSchema({
    // _id: projOptSchema.Types.ObjectId,
    name: { type: String, required: true },
    datatype: { type: String, required: true }, // boolean, integer, string    
    multichoice: { type: Boolean, required: true },
    modified: { type: Date, default: Date.now }
});

module.exports = mongoose.model('ProjectOption', optionModel);
