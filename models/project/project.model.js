// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var projSchema = mongoose.Schema;

var projModel = projSchema({
    name: { type: String, required: true },
    description: { type: String, required: true },

    category: {
        type: projSchema.ObjectId,
        // required: true,
        ref: "Category"
    },

    options: {
        type: [{
            id:
            {
                type: projSchema.Types.ObjectId,
                ref: 'ProjectOption'
            },
            values: { type: [String], required: true } // Single or multiple values (separated by comma)
        }]
    },

    updatedBy: {
        type: projSchema.Types.ObjectId,
        // required: true,
        ref: 'User'
    },

    modified: { type: Date, default: Date.now }
});

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Project', projModel);
