var bcrypt = require('bcryptjs');

// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var userSchema = mongoose.Schema;

var userModel = new userSchema({
    //_id: userSchema.Types.ObjectId,
    username: { type: String, required: true },
    hash: { type: String, required: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    email: { type: String, required: true },
    admin: { type: Boolean, default: false },
    updatedBy: String,
    modified: { type: Date, default: Date.now },
    forcePwdChange: { type: Boolean, default: false }
})

userModel
    .virtual('fullname')
    .get(function () {
        return this.firstname + (this.lastname ? ' ' + this.lastname : '');
    });
/*
// generating a hash
userModel.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};
*/

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('User', userModel);