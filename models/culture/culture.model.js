// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var cultureSchema = mongoose.Schema;

var cultureModel = cultureSchema({
    // _id: catSchema.Types.ObjectId,
    name: { type: String, required: true },
    code: { type: String, required: true },
    updatedBy: {
        type: cultureSchema.Types.ObjectId,
        ref: 'User'
    },
    modified: { type: Date, default: Date.now }
})

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Culture', cultureModel);