// require mongoose
const mongoose = require('mongoose');

// create the schema
var ConnectorTypeSchema = new mongoose.Schema({
    connectortypeid:{
        type: String,
        index: true
    },
    name: {
        type: String
    },
    category: {
        type: String
    },
    subcategory: {
        type: String
    },
    version: {
        type: String
    },
    provider: {
        type: String
    },
    sysdefined: { 
        type: Boolean, 
        default: false 
    },
    createdby: {
        type: String
    },
    createdon:{
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon:{
        type: Date,
        default: Date.now
    },
})

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('ConnectorType', ConnectorTypeSchema);