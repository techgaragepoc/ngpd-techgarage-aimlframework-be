// require mongoose
const mongoose = require('mongoose');

// create the schema
var DataSetSchema = new mongoose.Schema({
    datasetid: {
        type: String,
        index: true
    },
    name: {
        type: String
    },
    connectorid: {
        type: String
    },
    source: {
        type: String,
    },
    ispublic: {
        type: Boolean,
        default: false
    },
    sysdefined: {
        type: Boolean,
        default: false
    },
    createdby: {
        type: String
    },
    createdon: {
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon: {
        type: Date,
        default: Date.now
    },
})

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('DataSet', DataSetSchema);