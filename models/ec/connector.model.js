// require mongoose
const mongoose = require('mongoose');

// create the schema
var ConnectorSchema = new mongoose.Schema({
    connectorid:{
        type: String,
        index: true
    },
    name: {
        type: String
    },
    connectortypeid: {
        type: String
    },
    connectiondetail: {
        type: String
    },
    sysdefined: { 
        type: Boolean, 
        default: false 
    },
    createdby: {
        type: String
    },
    createdon:{
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon:{
        type: Date,
        default: Date.now
    },
})

// set up a mongoose model and pass it using module.exports
module.exports =  mongoose.model('Connector', ConnectorSchema);