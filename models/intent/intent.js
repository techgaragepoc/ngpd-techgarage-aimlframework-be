// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var intentSchema = new Schema({
    CategoryName: {
        type: String
    },
    ModelName: {
        type: String
    },
})

module.exports = mongoose.model('Intent', intentSchema);
