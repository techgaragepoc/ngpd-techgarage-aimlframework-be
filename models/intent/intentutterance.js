// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');

var IntentUtteranceSchema = mongoose.Schema;

var intentUtteranceModel = new IntentUtteranceSchema({
    Category: {
        type: IntentUtteranceSchema.ObjectId,
        // required: true,
        ref: "Intent"
    },
    UtteranceText: {
        type: String
    },
    Entity: {
        name: { type: String },
        startIndex: { type: Number },
        endIndex: { type: Number }
    },
    TrainingScores: String,
    POS: String,
    POSTree: String,
    TrainedEntities: String,
    modified: { type: Date, default: Date.now }
})

module.exports = mongoose.model('IntentUtterance', intentUtteranceModel);
