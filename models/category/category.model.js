// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var catSchema = mongoose.Schema;

var catModel = catSchema({
    // _id: catSchema.Types.ObjectId,
    name: { type: String, required: true },
    updatedBy: {
        type: catSchema.Types.ObjectId,
        ref: 'User'
    },
    modified: { type: Date, default: Date.now }
})

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Category', catModel);