const mongoose = require('mongoose');

// create the schema
var ClientSession = new mongoose.Schema({
    applicationid: {
        type: String,
        index: true
    },
    projectid: {
        type: String,
        index: true
    },
    usercontext: {
        type: mongoose.Schema.Types.Mixed,
    },
    chathistory: {
        type: mongoose.Schema.Types.Mixed,
    },
    createdby: {
        type: String,
        index: true
    },
    createdon:{
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon: {
        type: Date,
        index: true,
        default: Date.now
    }
})

// set up a mongoose model and pass it using module.exports
module.exports =  mongoose.model('clientsession', ClientSession);