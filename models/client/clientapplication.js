const mongoose = require('mongoose');

// create the schema
var ClientApplication = new mongoose.Schema({
   
    applicationname: {
        type: String,
        index: true
    },
    authenticationtoken: {
        type: String,
    },
    projects: {
        type: [String],
    },
    status: {
        type: String,
        default: 'active'
    },
    createdby: {
        type: String
    },
    createdon:{
        type: Date,
        default: Date.now
    },
    lastmodifiedby: {
        type: String
    },
    lastmodifiedon: {
        type: Date,
        default: Date.now
    }
})

// set up a mongoose model and pass it using module.exports
module.exports =  mongoose.model('clientapplication', ClientApplication);